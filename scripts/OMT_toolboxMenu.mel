///////////////////////////////////////////////////////////////////////////////////
///										///
///				Open Maya Tool header				///
///										///
///  FILENAME:	OMT_toolboxMenu.mel						///
///  AUTHOR: 	Jakob Welner (jakob@welner.dk)					///
///										///
///  UDTATE LOG:								///
///	18th og april: 1.10 by Jakob Welner					///
///	- Made it load all native tools as default				///
///										///
///	16th og april: 1.01 by Jakob Welner					///
///	- Bugfixing and some features added I cant remember			///
///										///
///  DEPENDENCIES:								///
///   	None									///
///										///
///  PURPOSE & USE:								///
///	Creation procedure for the OMToolbox menu				///
///										///
/// /////////////////////////////////////////////////////////////////////////// ///
///										///
///	Open Maya Toolbox: Opensource Alias Maya toolbox			///
///	Copyright (C) 2005 OMToolbox community					///
///										///
///	This library is free software; you can redistribute it and/or modify it ///
///	under the terms of the GNU Lesser General Public License as published 	///
///	by the Free Software Foundation; either version 2.1 of the License, or 	///
///	(at your option) any later version.					///
///										///
///	This library is distributed in the hope that it will be useful, but 	///
///	WITHOUT ANY WARRANTY; without even the implied warranty of 		///
///	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 		///
///	GNU Lesser General Public License for more details.			///
///										///
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
///				SCRIPT START					///
///////////////////////////////////////////////////////////////////////////////////



proc string getLabel(string $fileName)
{
	switch( $fileName ) {
		case "OMT_to_splitLoop":		return "Split Loop"; break;
		case "OMT_to_spinEdge":			return "Spin Edge"; break;
		case "OMT_to_extrudeComponent":		return "Extrude Component"; break;
		case "OMT_to_selectLoop":		return "Select Loop"; break;
		case "OMT_to_selectOutline":		return "Select Outline"; break;
		case "OMT_to_selectRing":		return "Select Ring"; break;
		case "OMT_to_splitAroundSelection":	return "Split Around Selected"; break; 
		case "OMT_to_connectComponents":	return "Connect Components"; break;
		case "OMT_to_selectionDragger":		return "Selection Dragger"; break;
		default:				return $fileName; break;
	}	
}





///////////////////////////////////////////////////////////////////////////////////
///	Function:	Opens Open Maya Toolboxs homepage in the default browser///
///////////////////////////////////////////////////////////////////////////////////

global proc OMT_homepageLink()
{
	string $win = "OM_homeWin";
    	if (`window -exists $win`) 
		{ deleteUI -window $win; }

	window -title "Open Maya Toolbox Home" 
		-width 1024
		-height 786
		$win;
		
	columnLayout;
	webBrowser 
		-width 1024
		-height 786
		-url "http://sourceforge.net/projects/omtoolbox/"
		btHomeBrowser;
	showWindow $win;
	
}

///////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////
///					TOOL					///
///	Function:	Opens the help doc in browser				///
///////////////////////////////////////////////////////////////////////////////////

global proc OMT_help()
{
	
	string $omtbPath = `whatIs OMT_toolboxMenu`;

	//string $buffer[];
	//subtract magic number of 24: "Mel procedure found in: "
	int $numCharacters = `size($omtbPath)`;

	$omtbPath = `endString $omtbPath ($numCharacters-24)`;

	//subtract magic number of 27: "scripts/OMT_toolboxMenu.mel"
	$numCharacters = `size($omtbPath)`;

	$omtbPath = `substring $omtbPath 1 ($numCharacters-27)`;

	//add new docs path
	$omtbPath = $omtbPath + "docs/index.html";
	$omtbPath = "file://" + $omtbPath;

	//print "\npath "; print $omtbPath;

	//build window
	string $win = "OM_toolboxHelpWindow";

    	if (`window -exists $win`) 
		{ deleteUI -window $win; }

	window -title "OMToolbox Help" 
		-width 1280
		-height 800
		$win;
	columnLayout;
	webBrowser -width 1280
		-height 800
		-url $omtbPath
		omtbHelpBrowser;
	showWindow $win;


}

///////////////////////////////////////////////////////////////////////////////////

















////////////////////////////////////
// get OMtoolbox FileList names without ".mel"
////////////////////////////////////
proc string[] listFiles(string $fileSpec)
{

    // Get the absolute path to the OMToolbox directory
	string $omtPath = `whatIs OMT_toolboxMenu`;

	//string $buffer[];
	//subtract magic number of 24: "Mel procedure found in: "
	int $numCharacters = `size($omtPath)`;

	$omtPath = `endString $omtPath ($numCharacters-24)`;

	//subtract magic number of 19: "OMT_toolboxMenu.mel"
	$numCharacters = `size($omtPath)`;

	$omtPath = `substring $omtPath 1 ($numCharacters-19)`;
	

    // Fetch a list of all OMT toolfiles
	string $toolFileList[] = `getFileList -folder $omtPath -filespec $fileSpec`;
	
	
    // Edit names to remove ".mel" at the end
	string 	$toolName[];
	int	$charCount;
	int	$i;
	
	for ($i = 0 ; $i < size($toolFileList) ; $i++)
	{
	    $charCount = `size($toolFileList[$i])`;
	    $toolName[$i] = `substring $toolFileList[$i] 1 ($charCount-4)`;
	}
	
	return $toolName;
}




////////////////////////////////////
// get OMtoolbox FileList names without ".mel"
////////////////////////////////////
proc string[] isolateIDs(string $toolFileList[])
{

    // Edit names to remove "OMT_to_" in the front of each name
	string 	$toolName[];
	int	$charCount;
	int	$i;
	
	for ($i = 0 ; $i < size($toolFileList) ; $i++)
	{
	    $charCount = `size($toolFileList[$i])`;
	    $toolName[$i] = `endString $toolFileList[$i] ($charCount - 7)`;
	}
	
	return $toolName;
}







///////////////////////////////////////
// Returns 1 if the optionVar value is = "Load", else it returns 0
///////////////////////////////////////

proc int getLoadValue(string $optionVarName)
{
	int	$return = 0;
	
	if ( `optionVar -q $optionVarName` == "Load") $return = 1;

	return $return;	
}








proc makeMenuItem(string $sectionName, string $OMToolList[], string $customToolList[], string $os)
{
	
	string 	$toolIDList[] = isolateIDs($OMToolList);
	
	int	$itemCount = 0;
	int	$i;

	// Native Tools
	for ($i = 0; $i < size($toolIDList); $i++)
	{
	    if ((`optionVar -q ("OMTLoad_" + $toolIDList[$i])`) == "Load" && (`optionVar -q ("OMTSection_" + $toolIDList[$i])`) == $sectionName)		
	    {	
	    	
	    	
	    	$itemCount++;
	    	
	    	menuItem -l `getLabel($OMToolList[$i])`
	    	-echoCommand true
	    	-c $OMToolList[$i]
	    	-i ($toolIDList[$i] + ".bmp")
	    	($toolIDList[$i] + "Item");
	    	
	    	eval("source " + $OMToolList[$i]);
	    	if (`exists ($OMToolList[$i] + "OptWin")`)
	    	{
			menuItem -optionBox true
			-label (getLabel($OMToolList[$i]) + " Option Box") 
			-command ($OMToolList[$i] + "OptWin") 
			($toolIDList[$i] + "OptWinItem");
		}
	    }
	}
	

	// Custom Tools
	for ($i = 0; $i < size($customToolList); $i++)
	{
	    if ((`optionVar -q ("OMTLoad_" + $customToolList[$i])`) == "Load" && (`optionVar -q ("OMTSection_" + $customToolList[$i])`) == $sectionName)		
	    {	
	    	
	    	$itemCount++;
	    	
	    	menuItem -l $customToolList[$i]
	    	-echoCommand true
	    	-c $customToolList[$i]
	    	-i ($customToolList[$i] + ".bmp")
	    	($customToolList[$i] + "Item");
	    }
	}


	
	if ($itemCount == 0)
	{
	    menuItem -l ($sectionName + ": Nothing here")
		-ann ("No tool set for this section")
		-en 0
		("no" + $sectionName + "Item");		
	}

	
	menuItem -divider true;
	
}








global proc OMT_setDefaults()
{
	// Modelling tools
	optionVar -sv OMTSection_splitLoop "Modelling";
	optionVar -sv OMTSection_splitAroundSelection "Modelling";
	optionVar -sv OMTSection_spinEdge "Modelling";
	optionVar -sv OMTSection_connectComponents "Modelling";
	optionVar -sv OMTSection_extrudeComponent "Modelling";
	optionVar -sv OMTSection_selectionDragger "Modelling";
	
	// General tools
	optionVar -sv OMTSection_selectLoop "General";
	optionVar -sv OMTSection_selectOutline "General";
	optionVar -sv OMTSection_selectRing "General";
	
	OMT_toolManageInterface;	
}









///////////////////////////////////////////////////////////////////////
// Menu Setting window
///////////////////////////////////////////////////////////////////////

global proc OMT_toolManageInterface()
{

   string $OMToolIDList[] = isolateIDs(`listFiles("OMT_to_*.mel")`);

   if (`window -exists toolManageInterfaceWin`)
      deleteUI toolManageInterfaceWin;

   window 
   	-width 322
   	-height 400 
   	-title "OMToolbox settings" 
   	-sizeable true 
   	toolManageInterfaceWin;

      string $mainForm = `formLayout`;
        string $mainTab = `tabLayout -innerMarginWidth 5 -innerMarginHeight 5`;
	  
	  string $ContentCol = `columnLayout -width 322 -height 400`;
	    tabLayout -e -tabLabel $ContentCol "Tool managing" $mainTab;

	    frameLayout 
	   	-width 322
	   	-l "OMTools" 
	   	-collapse 0 
	   	-collapsable 1
	   	-borderStyle "etchedIn";
	   	
	        string $OMTools = `columnLayout`;

		  rowLayout -numberOfColumns 3 -columnWidth 1 140;
			text -label "Tool name";
			text -label "Menu Item";
			text -label "Sections";
		  setParent $OMTools;	
                
		  for ($tool in $OMToolIDList)
		  {
		  
		     string 	$optVarLoadName = ("OMTLoad_" + $tool);
		     if (`optionVar -q $optVarLoadName` == 0) optionVar -sv $optVarLoadName "Load";		     
		     
		     string 	$optVarSectionName = ("OMTSection_" + $tool);
		     if (`optionVar -q $optVarSectionName` == 0) optionVar -sv $optVarSectionName "General";

		     rowLayout -numberOfColumns 3  -columnWidth 1 140 -columnAttach 1 "left" 5;
			text -label `getLabel(("OMT_to_" + $tool))`;
 
			checkBox
			-label "Auto load"
                        -value `getLoadValue($optVarLoadName)`
                        -onc ("optionVar -sv " + $optVarLoadName + " \"Load\"")
                        -ofc ("optionVar -sv " + $optVarLoadName + " \"Unload\"");
 
 			optionMenuGrp -changeCommand ("optionVar -sv " + $optVarSectionName + " `optionMenuGrp -q -v optionMenu_" + $tool + "`") ("optionMenu_" + $tool);	
 			  menuItem -label "Animation";
 			  menuItem -label "Modelling";
			  menuItem -label "Dynamics";
			  menuItem -label "Rendering";
 			  menuItem -label "General";

			  string $optVarSectionVal = `optionVar -q $optVarSectionName`;
			  optionMenuGrp -e -v $optVarSectionVal ("optionMenu_" + $tool); // Set the default here.

	        setParent $OMTools; 
		  }
              
	  setParent $ContentCol;
           
          
                

   string $melFileNameList[] = `listFiles("*.mel")`;
   string $customToolList[];
   
   for ($tool in $melFileNameList)
   {
        if (!`gmatch $tool "OMT_*"` && !`gmatch $tool "userSetup"`) $customToolList[size($customToolList)] = $tool;
   }
                
	   frameLayout 
	   	-width 322
	   	-l "CustomTools" 
	   	-collapse 1 
	   	-collapsable 1
	   	-borderStyle "etchedIn";

	      string $CustomTools = `columnLayout`;

		  rowLayout -numberOfColumns 3 -columnWidth 1 140;
			text -label "Tool name";
			text -label "Menu Item";
			text -label "Sections";
		  setParent $CustomTools;	
                
		  for ($tool in $customToolList)
		  {
		     string 	$optVarLoadName = ("OMTLoad_" + $tool);
		     if (`optionVar -q $optVarLoadName` == 0) optionVar -sv $optVarLoadName "UnLoad";
		     
		     string 	$optVarSectionName = ("OMTSection_" + $tool);
		     if (`optionVar -q $optVarSectionName` == 0) optionVar -sv $optVarSectionName "General";

	  
		     rowLayout -numberOfColumns 3 -columnWidth 1 140 -columnAttach 1 "left" 5;
			text -label $tool;
 
			checkBox
			-label "Auto load"
                        -value `getLoadValue($optVarLoadName)`
                        -onc ("optionVar -sv " + $optVarLoadName + " \"Load\"")
                        -ofc ("optionVar -sv " + $optVarLoadName + " \"Unload\"");
 
 			optionMenuGrp -changeCommand ("optionVar -sv " + $optVarSectionName + " `optionMenuGrp -q -v optionMenu_" + $tool + "`") ("optionMenu_" + $tool);	
 			  menuItem -label "Animation";
 			  menuItem -label "Modelling";
			  menuItem -label "Dynamics";
			  menuItem -label "Rendering";
 			  menuItem -label "General";

			  string $optVarSectionVal = `optionVar -q $optVarSectionName`;
			  optionMenuGrp -e -v $optVarSectionVal ("optionMenu_" + $tool); // Set the default here.

	      setParent $CustomTools; 
                  }
              
	  setParent $ContentCol;
              

	rowLayout -numberOfColumns 2 -columnAlign 1 "center" -columnAlign 2 "center" -columnAttach 1 "left" 0 -columnAttach 2 "left" 5;
 
           string $makeMenuButton =
            `button
               -label "Make Menu"
               -width 100
               -command OMT_toolboxMenu`;

           string $setDefaultButton =
            `button
               -label "Set Defaults"
               -width 100
               -command OMT_setDefaults`;

	  setParent $ContentCol;
        setParent $mainTab;
      setParent $mainForm;
               
   showWindow toolManageInterfaceWin;
   
}









///////////////////////////////
// BUTTOM TOOLS//
///////////////////////////////
proc OM_toolboxMenuButtom(string $os)
{

	menuItem -l "Help"
	    -ann "Help"
	    -echoCommand true
	    -c "OMT_help"
	    OM_helpItem;
		
	menuItem -l "Project Home"
	    -ann "Link to OMToolbox @ sourceforge"
	    -echoCommand true
	    -c "OMT_homepageLink"
	    OM_homepageLinkItem;
	    
	menuItem -l "Settings"
	    -ann "Configure OMToolbox"
	    -echoCommand true
	    -c "OMT_toolManageInterface"
	    OMT_toolManageInterfaceItem;	 

}	








proc OM_toolboxModeMenuStyle(string $os, string $mayaMode, string $toolNameList[], string $customToolList[])
{

	switch( $mayaMode ) {
		case "Animation":	makeMenuItem "Animation" $toolNameList $customToolList $os; break;
		case "Modeling":	makeMenuItem "Modelling" $toolNameList $customToolList $os; break;
		case "Rendering":	makeMenuItem "Rendering" $toolNameList $customToolList $os; break;
		case "Dynamics":	makeMenuItem "Dynamics" $toolNameList $customToolList $os; break;
		case "Cloth":		makeMenuItem "Cloth" $toolNameList $customToolList $os; break;
		case "Live":		makeMenuItem "Live" $toolNameList $customToolList $os; break;
		default:		makeMenuItem "Modelling" $toolNameList $customToolList $os; break;
	}
	
}







global proc OMT_toolboxMenu()
{

	global string $gMainWindow;
	global string $gMenuModeButton;
	string $mayaMode = `setMenuMode`;
	string $os = `about -operatingSystem`;

	if(`menu -exists OM_toolboxMenu`)
		deleteUI -menu OM_toolboxMenu;

	setParent $gMainWindow;

	menu -label "OMToolbox"
		-parent $gMainWindow
		-tearOff 1
		-allowOptionBoxes true
		-familyImage "default.bmp"
		OM_toolboxMenu;

	string $toolNameList[] = `listFiles("OMT_to_*.mel")`;
   	string $melFileNameList[] = `listFiles("*.mel")`;
   	
   	string $customToolList[];
   
   	for ($tool in $melFileNameList)
   	{
            if (!`gmatch $tool "OMT_*"` && !`gmatch $tool "userSetup"`) $customToolList[size($customToolList)] = $tool;
   	}

   	$toolNameList = `sort($toolNameList)`;
	$customToolList = `sort($customToolList)`;

	makeMenuItem("General", $toolNameList, $customToolList, $os);
	OM_toolboxModeMenuStyle $os $mayaMode $toolNameList $customToolList;
	//menuItem -divider true;
	OM_toolboxMenuButtom $os;

	hotBox -updateMenus;
	
}
