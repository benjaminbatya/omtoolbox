/**
Format - Conversion library for preforming Maya class -> MString conversions
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _FORMAT_H_
#define _FORMAT_H_

// Format.h - Formating function for MVector, MPoint, MMatrix, ...

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>

class Format
{
public:
    static MString toString(const MVector& vec);
    static MString toString(const double* mat);
    static MString toString(const float* mat);
    static MString toString(const MMatrix& mat);
    static MString toString(const float** mat);
	static MString toString(const MPoint& pos);
};


#endif
