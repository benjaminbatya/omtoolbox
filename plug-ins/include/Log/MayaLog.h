/**
MayaLog.h - Useful assertion, trace, and Check (assert with a message) macros for Maya
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _DPSIMAYALOG_H_
#define _DPSIMAYALOG_H_

#include <maya/MGlobal.h>
#include <maya/MIOStream.h>

#define MayaMessagePrefix() (MString(__FILE__) + " (" + __LINE__ + "): ")

#define MError(msg) \
    MGlobal::displayError(MayaMessagePrefix() + msg) // \
    // cerr << (MayaMessagePrefix() + msg) << endl;                          

#define MWarn(msg) \
    MGlobal::displayWarning(MayaMessagePrefix() + msg) // \
	// cout << (MayaMessagePrefix() + msg) << endl;                          

#define MInform(msg) \
    MGlobal::displayInfo(MString() + msg) // Don't display the prefix here it's too long
    // ; cout << (MString() + msg) << endl;

#define MValidateStatPtr(ptr) \
    MStatus __temp__; \
	if((ptr) == NULL) { (ptr) = &__temp__; }

#ifdef _DEBUG

#define MTrace(msg) MInform(msg)

#define MCheck(cond, msg) if(!(cond)) { MError(msg); return(MS::kFailure); }
#define MCheckNoReturn(cond, msg) if(!(cond)) { MError(msg); return; }
#define MCheckReturnObj(cond, msg, obj) if(!(cond)) { MError(msg); return(obj); }
#define MCheckStat(stat, msg) MCheck((stat) == MS::kSuccess, msg)
#define MCheckStatNoReturn(stat, msg) MCheckNoReturn((stat) == MS::kSuccess, msg)
#define MCheckStatReturnObj(stat, msg, obj) MCheckReturnObj((stat) == MS::kSuccess, msg, obj)

#define MAssert(cond) MCheck(cond, "Assert Failed")
#define MAssertNoReturn(cond) MCheckNoReturn(cond, "Assert Failed")
#define MAssertReturnObj(cond, obj) MCheckReturnObj(cond, "Assert Failed", obj)
#define MAssertStat(stat) MCheckStat(stat, "Assert Failed: Status = " + (stat).statusCode() + ", " + (stat).errorString())
#define MAssertStatNoReturn(stat) MCheckStatNoReturn(stat, "Assert Failed: Status = " + (stat).statusCode() + ", " + (stat).errorString())
#define MAssertStatReturnObj(stat, obj) MCheckStatReturnObj(stat, "Assert Failed: Status = " + (stat).statusCode() + ", " + (stat).errorString(), obj)

#else // _DEBUG

#define MTrace(msg)
	
#define MCheck(cond, msg)
#define MCheckNoReturn(cond, msg)
#define MCheckReturnObj(cond, msg, obj)
#define MCheckStat(stat, msg)
#define MCheckStatNoReturn(stat, msg)
#define MCheckStatReturnObj(stat, msg, obj)

#define MAssert(cond)
#define MAssertNoReturn(cond)
#define MAssertReturnObj(cond, obj)
#define MAssertStat(stat)
#define MAssertStatNoReturn(stat)
#define MAssertStatReturnObj(stat, obj)

#endif // _DEBUG


#endif // _DPSIMAYALOG_H_
