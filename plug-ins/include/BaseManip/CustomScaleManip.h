/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Declarations for the custom scale base manip.

#ifndef _CUSTOMSCALEMANIP_H_
#define _CUSTOMSCALEMANIP_H_

#include "CustomManip3D.h"
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MVector.h>
#include <maya/MManipData.h>
#include <maya/MMatrix.h>

class CustomEvent;

class CustomScaleManip : public CustomManip3D
{
protected:
	CustomScaleManip(CustomManipContainer* parent, MStatus* stat = NULL);

public:
    static CustomScaleManip* create(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);
    virtual void draw(M3dView& view);
    virtual void select(M3dView& view);
    // Update functions
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);
    
    // Connect to the scale plug k3Double.
    MStatus connectToScalePlug(MPlug& plug);
    // Connect to the scale pivot plug
    MStatus connectToScalePivotPlug(MPlug& plug);

    // The slot index getters
    // Getter for the scale pivot slot
    unsigned scalePivotIndex() { return(this->scalePivotSlot); }
    // Getter for the current scale slot
    unsigned scaleIndex() { return(this->scaleSlot); }

    // Component indices
    enum 
    {
	kXAxis = 0, // Component for scaling along the x axis
	kYAxis, // Component for scaling along the y axis
	kZAxis, // Component for scaling along the z axis
	kGlobal, // Component for scaling on all three axises
	kAll
    };
    
protected:    
    // The scale pivot data
    MManipData scalePivotData;
    // The current scale data
    MManipData scaleData;

    // Slots
    unsigned scalePivotSlot;
    unsigned scaleSlot;

private:

    // GLList for quickly rendering the circles and arcball
    static GLuint renderList;
    static void setupDrawingData();
    static const ManipColor manipColors[CustomScaleManip::kAll];

    // The axes
    MVector axes[3];
    // The previous scaling amount
    double prevDistance;
    // The current scaling amount
    double currScale;
    // Indicates which axes are being scaled during a mouse drag operation
    MVector scaleVec;

    // Gets the scaling amount for the current view.
    double getScale(M3dView& view, MVector& center, MStatus* stat = NULL);    
    double getHandleSize();
};

#endif
