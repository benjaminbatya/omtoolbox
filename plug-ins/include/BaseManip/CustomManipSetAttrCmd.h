/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _CUSTOMMANIPSETATTRCMD_H_
#define _CUSTOMMANIPSETATTRCMD_H_

/****************************************************
 * CustomManipSetAttrCmd.h - declarations of the command to be used when an attribute is set. 
 * Usually this is done when  
 ****************************************************/
 
#include <maya/MPxCommand.h>

class CustomManipSetAttrCmd : public MPxCommand
{
public:
    // static creation methods
    static void *creator() { return new CustomManipSetAttrCmd; }   

    MStatus doIt(const MArgList &);
    virtual MStatus undoIt();
    virtual MStatus redoIt();
    virtual bool isUndoable() const { return true; }

    static const MString commandName;

protected:
    MString undoArgs;
    MString redoArgs;
};

#endif
