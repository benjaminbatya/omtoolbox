/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Declarations for the viewport distamce manip....

#ifndef _CUSTOMROTATEMANIP_H_
#define _CUSTOMROTATEMANIP_H_

#include "CustomManip3D.h"
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MVector.h>
#include <maya/MManipData.h>
#include <maya/MMatrix.h>

class CustomEvent;

class CustomRotateManip : public CustomManip3D
{
protected:
	CustomRotateManip(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);

public:
    static CustomRotateManip* create(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);
    virtual void draw(M3dView& view);
    virtual void select(M3dView& view);
    // Update functions
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);
    
    // Connect to the rotation plug k3Double in radians.
    MStatus connectToRotationPlug(MPlug& plug);
    // Connects to the rotation pivot.
    MStatus connectToRotationPivotPlug(MPlug& plug);

    // The slot index getters
    // Getter for the current rotation slot
    unsigned rotationIndex() { return(this->rotationSlot); }
    // Getter for the current rotation center slot
    unsigned rotationPivotIndex() { return(this->rotationPivotSlot); }

    // Component indices
    enum ManipPlane
    {
	kXYPlane = 0,
	kXZPlane,
	kYZPlane,
	kViewPlane,
	kArcball,
	kAll
    };
    
protected:    
    // The current center data
    MManipData rotationPivotData;
    // The current rotation data
    MManipData rotationData;

    // Slots
    unsigned rotationPivotSlot;
    unsigned rotationSlot;

private:

    // GLList for quickly rendering the circles and arcball
    static GLuint renderList;
    static const MVector normals[3];
    static void setupDrawingData();
    static const ManipColor manipColors[CustomRotateManip::kAll];

    // The initial angle of the drag
    // MVector startVec;
    // The previous angle of the drag
    MVector currVec;

    MStatus calcHalfSphericalCoords(const CustomEvent& event, MVector& retVal);
    // Center plane to get scaling values...
    double getScale(M3dView& view, MVector& center, MStatus* stat = NULL);
    
};

#endif
