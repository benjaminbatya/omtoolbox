/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Custom manipulator container

#ifndef _CUSTOMMANIPCONTAINER_H_ 
#define _CUSTOMMANIPCONTAINER_H_ 

#include <maya/MPxManipContainer.h>
#include <maya/MMessage.h>
#include <maya/MDoubleArray.h>
#include <maya/MStringArray.h>

// Include the vector class
#include <vector>

// We offset the slots so we cannot conflict with Maya's slot ordering.
// Means we can override addManipToPlugCCB and addPlugToManipCCB without problems.
#define SLOT_OFFSET 0x80000000

class CustomEvent;
// Forward declare CustomManip3D
class CustomManip3D;
// Forward declare the CustomManipContainer and MManipData
class CustomManipContainer;
class MManipData;
class MMatrix;

// Typedef the vector of Manips
typedef std::vector<CustomManip3D*> ManipVector;

// Declare the class needed to store the ManipSlots
class ManipSlot
{
public:
    ManipSlot(MManipData* dataPtr);
    // POINTER to the manipulator's data
    MManipData* dataPtr;
    // The function to use to convert the value of the manipulator data
    plugToManipConversionCallback convertCB;
    // Plug slot if the plug was directly attached
    unsigned associatedPlugSlot;
};
typedef std::vector<ManipSlot*> ManipSlotVector;

// Class needed to store the PlugSlots.
class PlugSlot
{
public:
    PlugSlot(MObject& node, MObject& attr);
    // The MObject of the connected plug and attribute
    MObject node;
    MObject attribute;
    // The function to use to convert the value of the plug data
    manipToPlugConversionCallback convertCB;
    // Manip slot data if the manip was attached directly.
    unsigned associatedManipSlot;
};
typedef std::vector<PlugSlot*> PlugSlotVector;

class CustomManipContainer : public MPxManipContainer
{
public:
    CustomManipContainer();
    virtual ~CustomManipContainer();
    
    virtual MStatus createChildren();
	// Subclasses should not override this method
	// NOTE: Remove the virtual then
    virtual MStatus connectToDependNode(const MObject&);
    
    // Pure virtual functions which the children of this class must implement
    virtual MStatus customConnectToDependNode(const MObject& depNode) = 0;
    
    // New method for adding custom base manipulators
    void addManip(CustomManip3D*);
    
    // Used for registering the manips to the Container
    unsigned registerManip(MManipData* manipPtr);
    // Used for registering the plugs to the container
    unsigned registerPlug(MObject& node, MObject& attribute);
    	    
    // Subclasses of CustomManipContainer are supposed to override drawManip instead!!
	// NOTE: Make this non-virtual to not be overrided
	virtual void draw(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status);
	// they should be overriding drawManip instead of draw...
    virtual void drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status) = 0;

    // Registers the callbacks for the manipulator's input slots
    // Ignore plugSlot unless you are writing a Custom Base Manipulator, otherwise it is used to indicate which plug to get a value from
    void addPlugToManipConversionCallback(unsigned slot, plugToManipConversionCallback callback, unsigned plugSlot = (unsigned)-1);
    // Ignore manipSlot unless you are writing a Custom Base Manipulator, otherwise it is used to indicate which manip to get a value from
    unsigned addManipToPlugConversionCallback(MPlug& plug, manipToPlugConversionCallback callback, unsigned manipSlot = (unsigned)-1);
    // Registers the plug's slot in this ManipContainer without having to use addManipToPlugCB
    unsigned getPlugSlot(MPlug& plug);

    // Getters for the manipulators in their slots.
    MStatus getConvertorManipValue(unsigned manipIndex, unsigned& value);
    MStatus getConvertorManipValue(unsigned manipIndex, double& value);
    MStatus getConvertorManipValue(unsigned manipIndex, MVector& value);
	MStatus getConvertorManipValue(unsigned manipIndex, MPoint& value);
    MStatus getConvertorManipValue(unsigned manipIndex, MDoubleArray& array);
    MStatus getConvertorManipValue(unsigned manipIndex, MMatrix& matrix);
    // Add more later...
    
    // Getters for the plugs in the slots...
	MStatus getConvertorPlugValue(unsigned plugIndex, int& value);
    MStatus getConvertorPlugValue(unsigned plugIndex, double& value);
    MStatus getConvertorPlugValue(unsigned plugIndex, MVector& value);
    MStatus getConvertorPlugValue(unsigned plugIndex, MDoubleArray& array);
    MStatus getConvertorPlugValue(unsigned plugIndex, MMatrix& matrix);
    // Add more later...
    
    // Generic conversion callbacks so it's easy to hookup customManips to plugs.
    MManipData doublePToMConversionCallback(unsigned index);
    MManipData doubleMToPConversionCallback(unsigned index);
    MManipData k3DoublePToMConversionCallback(unsigned index);
    MManipData k3DoubleMToPConversionCallback(unsigned index);
    MManipData doubleArrayPToMConversionCallback(unsigned index);
    MManipData doubleArrayMToPConversionCallback(unsigned index);
    MManipData matrixPToMConversionCallback(unsigned index);
    MManipData matrixMToPConversionCallback(unsigned index);
    // Add others here...

	// The ViewChangedEvent
	static const MString ViewChangedEvent;

protected:
    // Method for updating the manipulators
    void updateManips();
    // Method for updating the plugs
    void updatePlugs();
    
	// Runs the selection for each base manip
	virtual CustomManip3D* select(const CustomEvent& e);

    virtual void onMotion(const CustomEvent& e);
    virtual void onPress(const CustomEvent& e);
    virtual void onRelease(const CustomEvent& e);
    
	// Setup a friend function
	friend void processEvent(const CustomEvent&);

    // Vector to track the base manipulators
    ManipVector baseManips;

    // The slot vectors for the manipulators
    ManipSlotVector manipSlots;
    PlugSlotVector plugSlots;
    
    // The currently selected base manipulator
    CustomManip3D* selectedManip; 
    // The plug array settings for the duration of the base manipulator manipulation.
    // Determines whether the plug settings should be saved or not.
    MStringArray plugSettings;

    // Saves the current plug settings into the plugSettings array
    MStatus getPlugSetCmds(MStringArray& strArray);
    // Sets the plug undo strings into the undo queue
    MStatus setPlugUndos();
	
private:
    // A callback for setting the release state of the manipulator
    MCallbackId releaseCBID;
    friend void releaseCB(void*);
    void onReleaseCB();
    
    CustomManip3D* parseSelectHits(GLint, GLuint*);
    
    // This manip is only here to prevent Maya from crashing since it must have at least one baseManip
    // inside of a ManipContainer in order to not crash.
    MDagPath dummy;

public:
	static bool isActiveManip();
private:
	static void setActiveManip(CustomManipContainer*);
	static CustomManipContainer* getActiveManip();
	static CustomManipContainer* _activeManip;
};

#endif
