/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _CUSTOMMANIP3D_H_
#define _CUSTOMMANIP3D_H_

#include <maya/MFnManip3D.h>
#include <maya/M3dView.h>

// Forward declare CustomManipContainer
class CustomManipContainer;
class CustomEvent;
class MFloatMatrix;

struct ManipColor
{
	const char* name;
	bool active;
};

class CustomManip3D : public MFnManip3D
{
protected:
    CustomManip3D(CustomManipContainer* parent, unsigned numComponents, MStatus* stat = NULL);

public:
    virtual ~CustomManip3D();

public:
	// Creator method. THIS MUST BE OVERRIDED
	// Used to indicate that CustomManipContainer OWNs the manip, not the child class.
    static CustomManip3D* create(CustomManipContainer* parent, MStatus* stat = NULL);
    // Make the draw function pure virtual
    virtual void draw(M3dView& view) = 0;
    // Make the select function pure virtual
    virtual void select(M3dView& view) = 0;
    
    // Active component setter/getter
    enum {
		kNoComponentSelected = -1
	};
	void setComponent(unsigned component);
    unsigned getComponent(void);
    bool isActive();

    // Component enabled getter/setters
    void setComponentEnabled(unsigned component, bool state);
    bool isComponentEnabled(unsigned component);

    // event handler functions
    // Used when the container receives mouse events and has to update the manipulator
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);

protected:
	// Returns the parent	
	CustomManipContainer* getParent() const { return(this->parent); }

    // Helper function for getting the index value of a named color.
    int queryDisplayColor(MString name, bool active);
    // Helper function for getting the RGB colors for an index.
    MColor queryColorIndex(int index);
    // Helper function for getting the current projection matrix, useful inside of select commands.
    MStatus getProjectionMatrix(M3dView& view, float mat[][4]);
    // helper function to calculate the scale factor for an object which needs to stay constant size.
    // (Or shrink by constant size, if it's a object in screen space and needs to stay constant size.)
    double getForeshortenScale(M3dView& view, MVector& center, MStatus* = NULL) const;

    // Helper function to calculate the closest distance between a ray and a line.
    MStatus calcClosestPtToAxis(const MVector& rayStart, const MVector& rayDirection, 
				const MVector& pos, const MVector& axis, 
				MVector& retVal);
    // Helper function to calculate the intersection of a ray and a plane.
    MStatus calcPlaneLineIntersection(const MVector& rayStart, const MVector& rayDirection, 
				      const MVector& pos, const MVector& normal, 
				      MVector& retVal);
private:
	// Parent should be only accessible by subclasses.
    CustomManipContainer* parent;

	// Indicates which component is active or kNoComponentSelected if none
	// are active
    unsigned component;

    // the number of components in the manip
    const unsigned numComponents;
    // The enabled flags for each of the components
    bool* componentEnabled;
};

#endif
