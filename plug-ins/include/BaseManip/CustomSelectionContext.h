/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/***********************************
 * CustomSelectionContext.h - Used for Selecting while using the custom base manips and CustomManipContainer.
************************************/

#ifndef _CUSTOMSELECTIONCONTEXT_H_
#define _CUSTOMSELECTIONCONTEXT_H_

#include <maya/MPxSelectionContext.h>
#include <maya/M3dView.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>

#define CUSTOM_CONTEXT_HELP_STRING "Click with left button or drag with middle button to select"

class CustomSelectionContext : public MPxSelectionContext
{
public:
	CustomSelectionContext();
	virtual void    toolOnSetup( MEvent & event );
	virtual void    toolOffCleanup();
	
	// Subclasses are supposed to override doCustomPress/Drag/Hold/Release instead of these...
	virtual MStatus doPress( MEvent & event );
	virtual MStatus doHold(MEvent& event);
	virtual MStatus doDrag( MEvent & event );
	virtual MStatus doRelease( MEvent & event );

	// Override these instead of the regular doPress/Hold/Drag/Release events
	// so event filtering works properly
	virtual MStatus doCustomPress( MEvent & event ) = 0;
	virtual MStatus doCustomHold(MEvent& event) = 0;
	virtual MStatus doCustomDrag( MEvent & event ) = 0;
	virtual MStatus doCustomRelease( MEvent & event ) = 0;

	virtual MStatus doEnterRegion( MEvent & event );

	bool isActiveContext();

private:
    
    // Flag for if this context is active or not.
    bool active;
};

#endif
