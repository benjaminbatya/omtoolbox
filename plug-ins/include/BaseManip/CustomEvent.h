/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Subclasses the MEvent class
// Used for onPress/Drag/Release event handlers

#ifndef _CUSTOMEVENT_H_
#define _CUSTOMEVENT_H_

#include <maya/MTypes.h>

class CustomEvent
{
public:    
    enum MouseButton
    {
	kNoButton     = 0,
	kLeftButton   = 1L<<0,
	kMiddleButton = 1L<<1,
	kRightButton  = 1L<<2
    };
    enum KeyModifier
    {
	kNoModifier = 0,
	kShiftKey   = 1L<<0,
	kCtrlKey    = 1L<<1,
	kAltKey     = 1L<<2 
    };
    enum EventType
    {
	kInvalidEventType = 0,
	kButtonPressed,
	kButtonReleased,
	kMouseMoved
    };

	CustomEvent(const CustomEvent&);
    CustomEvent(int x=0, int y=0, MouseButton b=kNoButton, KeyModifier k=kNoModifier, EventType m=kInvalidEventType);
    
    // Override the mouseButton functions...
    inline MouseButton getButton(void) const { return(this->button); }
    void setButton(const MouseButton);
    inline KeyModifier getModifier(void) const { return(this->modifier); }
    void setModifier(const KeyModifier);
    inline EventType getEventType(void) const { return(this->type); }
    void setEventType(const EventType);
    void setPosition(const int, const int);

	// Indexer method
    inline int& operator[](const int idx);
	
	// Copy method
    CustomEvent& operator=(const CustomEvent& that);

    int x, y;

protected:
    MouseButton button;
    KeyModifier modifier;
    EventType type;
};

#endif
