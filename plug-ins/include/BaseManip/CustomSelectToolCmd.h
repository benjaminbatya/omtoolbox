/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _CUSTOMSELECTTOOLCMD_H_
#define _CUSTOMSELECTTOOLCMD_H_

/****************************************
 * CustomSelectToolCmd.h - used for saving selection commands by the CustomSelectionContext.
 ****************************************/
  
#include <maya/MPxCommand.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MMessage.h>

class CustomSelectToolCmd : public MPxCommand
{
public:
    // CustomSelectToolCmd();
    
    static void* creator() { return(new CustomSelectToolCmd); }
    // Command string for the tool
    static MString cmdString;

    virtual MStatus doIt(const MArgList&);
    virtual MStatus undoIt();
    virtual MStatus redoIt();
    virtual bool isUndoable() const { return(true); }

protected:
    // Undo/redo fields
    MSelectionList preList;
    MSelectionList postList;
};

#endif

