/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// CustomManipContainer.cpp - base class for Manip containers which use Custom
// base manipulators
#include "CustomManipContainer.h"
#include "CustomManip3D.h"
#include "CustomEvent.h"
#include "CustomManipSetAttrCmd.h"

#include <maya/MGlobal.h>
#include <maya/MFnStateManip.h>
#include <maya/MManipData.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixData.h>
#include <maya/MEventMessage.h>
#include <maya/MUserEventMessage.h>
#include <maya/MCommandResult.h>

#if defined (LINUX)
	#include <Xm/Xm.h> 
	#include <GL/glx.h>
	#include <GL/GLwDrawA.h>
#elif defined (WIN32)
	#include <windows.h>
#endif

#include <GL/glu.h>
#include <Log/MayaLog.h>

#include <string>
#include <algorithm>

ManipSlot::ManipSlot(MManipData* data)
{
	// Initialize both pointers to NULL
	this->dataPtr = data;
	this->convertCB = NULL;
	this->associatedPlugSlot = (unsigned)-1;
}

PlugSlot::PlugSlot(MObject& node, MObject& attr)
{
	// Initialize both pointers to NULL
	this->node = node;
	this->attribute = attr;
	this->convertCB = NULL;
	this->associatedManipSlot = (unsigned)-1;
}

// A vector of observing ManipContainers who will receive callbacks 
// whenever the inputCB is called
typedef std::vector<CustomManipContainer*> ManipContainerVector;
static ManipContainerVector manipContainers;

#if defined (LINUX)
 // Prototype for the Xt callback function
 void inputCB(Widget w, XtPointer closure, XEvent* event, Boolean* continue_to_dispatch);    
 // Currently used widget.
 // NOTE: THIS IS UNTESTED!!
 static Widget inputHook = NULL;
#elif defined (WIN32)
 // Prototype for the Win32 callback function
 LRESULT CALLBACK inputCB(int nCode, WPARAM wParam, LPARAM lParam);
 // ID of the hook function so it can be removed properly 
 // when there is no more manipContainers using it
 static HHOOK inputHook = NULL; 
#endif

const MString CustomManipContainer::ViewChangedEvent("ViewChanged");

CustomManipContainer::CustomManipContainer() : MPxManipContainer()
{
	// MTrace("CustomManipContainer::CustomManipContainer: called.");
	this->baseManips.clear();
	this->manipSlots.clear();
	this->plugSlots.clear();

	// Setup the selected manipulator
	this->selectedManip = NULL; 
	// Setup the releaseCB id
	this->releaseCBID = MCallbackId(-1);

	// If nothing exists in the vector, setup the hook function
	if(manipContainers.empty())
    {
		MAssertNoReturn(inputHook == NULL);
#if defined (LINUX)
		M3dView view = M3dView::active3dView();
		inputHook = XtWindowToWidget(view.display(), view.window()); 
		// cerr << "CustomManipContainer::CustomManipContainer: adding input callback to widget " << widget << endl; 
		// MGlobal::displayInfo(MString("CustomManipContainer::draw; got widget ") + XtName(widget));
		// Add the event handler
		// NOTE Insert event handler has to be used, otherwise there seems to be crashes...
		XtInsertEventHandler(inputHook, ButtonPressMask|ButtonReleaseMask|PointerMotionMask, False, inputCB, NULL, XtListHead);
#elif defined (WIN32)
		// MTrace("CustomManipContainer::customAddManip: Registering mouse input CB.");
		inputHook = SetWindowsHookEx(WH_MOUSE, inputCB, GetModuleHandle(NULL), GetCurrentThreadId());
		// MTrace("CustomManipContainer::customAddManip: Input Hook = " + unsigned(inputHook));
#endif
		MAssertNoReturn(inputHook != NULL);
	}

	// Check if this is already in the list of manipContainers
	ManipContainerVector::iterator p1 = std::find(manipContainers.begin(), manipContainers.end(), this);
	if(p1 == manipContainers.end())
   	{
		// MTrace("CustomManipContainer::customAddManip: Adding container " + unsigned(this) + " to manipContainers vector.");
		manipContainers.push_back(this);
   	} 
}

CustomManipContainer::~CustomManipContainer()
{
	// MTrace("CustomManipContainer::~CustomManipContainer: called.");

	// Delete all of the registered ManipSlots
	for(ManipSlotVector::iterator manipIter = this->manipSlots.begin();
	   manipIter != this->manipSlots.end(); 
	   manipIter++)
	{
		delete *manipIter;
	}
	// Delete all of the registered PlugSlots
	for(PlugSlotVector::iterator plugIter = this->plugSlots.begin();
	   plugIter != this->plugSlots.end(); 
	   plugIter++)
	{
		delete *plugIter;
	}

	// Empty out the baseManips vector
	for(ManipVector::iterator iter = this->baseManips.begin();
	   iter != this->baseManips.end(); 
	   iter++)
	{
		// Delete each of the BaseManipInfos
		delete *iter;
	}
	
	// Search the manipContainers vector. If we find this in it,
	// remove it from the vector
	ManipContainerVector::iterator p1 = std::find(manipContainers.begin(), manipContainers.end(), this);
	if(p1 != manipContainers.end())
	{
		// MTrace("this = " + unsigned(this) + ", *p1 = " + unsigned(*p1));
		// Remove p1 from manipContainers
		manipContainers.erase(p1);
	}

	if(manipContainers.empty())
	{ 
		MAssertNoReturn(inputHook != NULL);
#if defined (LINUX)
		// cerr << "CustomManipContainer::~CustomManipContainer: Removing the input callback from widget " << this->widget << endl; 
		// MGlobal::displayInfo(MString("CustomManipContainer::~CustomManipContainer: Removing the input callback from widget ") +
		// 		     XtName(widget)); 
		// See http://www.ac3.edu.au/SGI_Developer/books/XLib_WinSys/sgi_html/ch09.html
		XtRemoveEventHandler(inputHook, ButtonPressMask|ButtonReleaseMask|PointerMotionMask, False, inputCB, NULL);
		// XtRemoveCallback(this->widget, GLwNinputCallback, inputCB, this);  
#elif defined (WIN32)
		// MTrace("CustomManipContainer::~CustomManipContainer: Removing the input callback from widget " + unsigned(inputHook)); 
		BOOL val = UnhookWindowsHookEx(inputHook);
		// MTrace("CustomManipContainer::~CustomManipContainer: Failure flag:" + val);
		MAssertNoReturn(val != FALSE);
#endif  
		inputHook = NULL;
	}
}

// We need to save the previous modelview matrix so we can restore it
// inside the select function
static GLdouble prevModelMatrix[16];

void CustomManipContainer::draw(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{
	MStatus stat; 

	if(style == M3dView::kFlatShaded || style == M3dView::kGouraudShaded)
	{
		// Look at the depth buffer enable flag 
		GLint depthEnabled = GL_TRUE;  
		glGetIntegerv(GL_DEPTH_TEST, &depthEnabled);  
		if(!depthEnabled)
		{
			// Return if the depth testing is disabled, we don't want to draw strange things...
			return;         
		}
		// MTrace(MString("Depth buffer is enabled = ") + depthEnabled);
	}

	// MTrace(MString("CustomManipContainer::draw: called. style = ") + style);
	MPxManipContainer::draw(view, path, style, status);

	// Draw the manipulator draw function
	this->drawManip(view, path, style, status);

	// Update all of the manipSlots by calling them
	this->updateManips();

	// Start drawing
	stat = view.beginGL(); MAssertStatNoReturn(stat);
	{  
		// Save the current and transform bits...
		glPushAttrib(GL_CURRENT_BIT | GL_TRANSFORM_BIT | GL_ENABLE_BIT);
		{ 
			// Individual base manips can enable lighting if they want
			glDisable(GL_LIGHTING);
			// Setup culling...
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);   
			// Clear the depth buffer so our manips overlay the objects.
			glClear(GL_DEPTH_BUFFER_BIT);
			// Enable the depth test   
			glEnable(GL_DEPTH_TEST);

			// Draw each of the base manipulators in the vector
			for(ManipVector::iterator iter = this->baseManips.begin(); iter != this->baseManips.end(); iter++)
			{
				(*iter)->draw(view);
			}

			// Save the current modelMatrix
			glGetDoublev(GL_MODELVIEW_MATRIX, prevModelMatrix);

			/*
			// Draw the selection items
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			{ 
				glLoadIdentity();

				// This is for displaying the selection buffer...   
				for(ManipVector::iterator iter = this->baseManips.begin();
					 iter != this->baseManips.end();
					 iter++) 
				{
					glMatrixMode(GL_PROJECTION);
					glPushMatrix();
					{    
						(*iter)->select(view);
					}
					glMatrixMode(GL_PROJECTION);
					glPopMatrix();      
				} 
			}
			glMatrixMode(GL_PROJECTION);
			glPopMatrix();
			*/
		}
		glPopAttrib();
	}
	stat = view.endGL(); MAssertStatNoReturn(stat); 
}

// Setup our Selection buffer size
#define BUFSIZE 512

/**
 * Determines if any manipulator was hit by the mouseX/Y coordinates.
 * @param	mouseX		mouse X coordinate
 * @param	mouseY		mouse Y coordinate
 * @return	the manipulator that was hit or NULL if none were hit.
 */
CustomManip3D* CustomManipContainer::select(const CustomEvent& e)
{
	MStatus stat;

	// MTrace(MString("CUSTOMManipContainer::select: MOUSE POS = (") + e.x + ", " + e.y + ")");

	M3dView view = M3dView::active3dView();

	GLuint selectBuf[BUFSIZE];
	GLint hits;
	// Get the viewport size
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);   

	view.beginSelect(selectBuf, BUFSIZE);
	{
		// Initialize the selection names
		glInitNames();
		glPushName(0);
		{
			// Save the current and transform bits...
			glPushAttrib(GL_CURRENT_BIT | GL_TRANSFORM_BIT);
			{    
				glMatrixMode(GL_MODELVIEW);
				glPushMatrix();
				{   
					/*
					GLdouble mat[16];
					// Read out current ModelView matrix
					glGetDoublev(GL_MODELVIEW_MATRIX, mat);
					MString str = "CustomManipContainer::select: modelview matrix = \n";
					for(i=0; i<16; i++)
					{
						str += MString(", ") + mat[i];
					}
					MTrace(str);
					*/

					// Load the previous matrix so the modelview matrix is correct.
					glLoadMatrixd(prevModelMatrix);
				
					// Setup the projection matrix
					glMatrixMode(GL_PROJECTION);
					glPushMatrix();
					{    
						glLoadIdentity();
						// Get the current selection size from MEL prefs
						MCommandResult res;
						stat = MGlobal::executeCommand("selectPref -q -clickBoxSize", res); MAssertStatReturnObj(stat, NULL);
						int selSize;
						stat = res.getResult(selSize); MAssertStatReturnObj(stat, NULL);    

						// Setup the pickmatrix     
						// MGlobal::displayInfo(MString("CustomManipContainer::select: mouse pos = (") + e.x + ", " + e.y + ")");
						gluPickMatrix((GLdouble)e.x, (GLdouble)e.y, selSize, selSize, viewport); 

						// Draw each of the base manipulators in the vector
						unsigned i=0;
						for(ManipVector::iterator iter = this->baseManips.begin(); iter != this->baseManips.end(); iter++, i++)
						{
							glMatrixMode(GL_PROJECTION);
							glPushMatrix();
							{     
								// Set the first name to the manip that is being rendered
								glLoadName(i);
								// Always push the component's name
								glPushName(0);
								{   
									(*iter)->select(view);
								}
								glPopName();
							}
							glMatrixMode(GL_PROJECTION);
							glPopMatrix();
						}
					}
					glMatrixMode(GL_PROJECTION);
					glPopMatrix();
				}
				glMatrixMode(GL_MODELVIEW);
				glPopMatrix();
			}
			glPopAttrib();
		}
		glPopName();
	}
	hits = view.endSelect();
	// Figure out which objects were hit or not...
	return(this->parseSelectHits(hits, selectBuf));
}

/**
 * Parses out the hit structure and gets the values
 * @param	hits	the number of hits gotten
 * @param	ptr	pointer to the hit datastructure
 * @return	the manipulator hit or NULL if none.
 */
CustomManip3D* CustomManipContainer::parseSelectHits(GLint hits, GLuint* ptr)
{
	// Examine the hit buffer here...
	// MTrace(MString("CustomManipContainer::select: Number of hits = ") + hits);

	CustomManip3D* manip = NULL;
	float minZ = 10e28f;

	for(int i=0; i<hits; i++)
	{
		// Find the first record in the stack with two names.
		// Get the number of names
		GLuint names = *ptr; ptr++;
		// MTrace(MString("CustomManipContainer::select: number of names = ") + names);  

		// Check if there are two names on the stack, if so use it
		if(names == 2)
		{
			// Get the  average z values
			float z = (float) *ptr/0x7fffffff; 
			ptr++;
			// Ignore the max z for now... we're only interested in the closest manipulator
			z += (float) *ptr/0x7fffffff; 
			ptr++;
			// Calculate the average z value.
			z /= 2;

			// MTrace(MString("CustomManipContainer::select: Hit manipulator #") + *ptr);
			// Get the selected manip
			int manipIdx = *ptr;   
			ptr++;
			// Get the manip's selected component   
			// MTrace(MString("CustomManipContainer::select: Selected manip component ") + *ptr);
			int comp = *ptr;
			ptr++;

			// Only use the current base manip if it is closer to the screen then the previous manip
			if(z < minZ)
			{
				manip = this->baseManips[manipIdx];
				manip->setComponent(comp);
				minZ = z;
			}

		} else
		{
			// MTrace("record doesn't have 2 names, skipping...");
			// skip names  and z values and continue searching
			ptr += names + 2;
		}
	}

	return(manip);
}

void CustomManipContainer::addManip(CustomManip3D* manip)
{
	// MGlobal::displayInfo("CustomManipContainer::addManipulator: called.");    
	// Add the manip to thebaseManips vector
	this->baseManips.push_back(manip);
}

MStatus CustomManipContainer::createChildren()
{
	MStatus stat;

	// Add a dummy real manip so maya doesn't crash
	this->dummy = this->addStateManip("dummyManip", "dM");
	// Set the functor
	MFnStateManip dumFn(dummy, &stat); MAssertStat(stat);
	// Make it invisible
	stat = dumFn.setVisible(false); MAssertStat(stat);

	return(stat);
}

MStatus CustomManipContainer::connectToDependNode(const MObject& depNode)
{
	MStatus stat;

	// Connect the depNode to the dummy manipulator...
	MFnStateManip stateManip(this->dummy, &stat); MAssertStat(stat);
	MFnDependencyNode nodeFn(depNode); MAssertStat(stat);
	MPlug plug = nodeFn.findPlug("nodeState", &stat); MAssertStat(stat);
	stat = stateManip.connectToStatePlug(plug); MAssertStat(stat);

	// Connect the manips to the dependency node and it's attributes
	stat = this->customConnectToDependNode(depNode); MAssertStat(stat);

	stat = this->finishAddingManips(); MAssertStat(stat);
	stat = MPxManipContainer::connectToDependNode(depNode); MAssertStat(stat);

	return(stat);
}

/**
 * Registers manipulators to the manipulator vector
 * @param	manipPtr	       	pointer to the manipData in the CustomManip3D
 * @return	the index value that the new manipIndex is at..
 */ 
unsigned CustomManipContainer::registerManip(MManipData* manipPtr)
{
	// Make a new ManipSlot instance
	ManipSlot* newSlot = new ManipSlot(manipPtr); MAssert(newSlot != NULL);
	
	// Record the current size of the vector
	unsigned index = this->manipSlots.size();
	
	// Add the new ManipSlot to the vector
	this->manipSlots.push_back(newSlot);
	
	// Offset the slot index
	MAssert(index < SLOT_OFFSET);
	index += SLOT_OFFSET;

	// Return the index
	return(index);
}

/**
 * Registers plugs to the plugSlots vector
 * @param	plug 	       	pointer to the MPlug which the manipulator can modify
 * @return	the index value of the new plugIndex
 */ 
unsigned CustomManipContainer::registerPlug(MObject& node, MObject& attr)
{
	// Make a new PlugSlot instance
	PlugSlot* newSlot = new PlugSlot(node, attr); MAssert(newSlot != NULL);
	
	// Record the current size of the vector
	unsigned index = this->plugSlots.size();
	
	// Add the new ManipSlot to the vector
	this->plugSlots.push_back(newSlot);
	
	// Offset the slot index
	// NOTE: Technically this isn't needed for the plugSlots
	// but for consistancy with the manipSlots, we do it.
	MAssert(index < SLOT_OFFSET);
	index += SLOT_OFFSET;
	
	// Return the index
	return(index);
}

void CustomManipContainer::updateManips()
{
	// Run thorugh each of the manipulator slots and
	// get it's callback function and MManipData ptr
	for(unsigned i=0; i<this->manipSlots.size(); i++)
	{
		ManipSlot* slot = (ManipSlot*)this->manipSlots[i];
		// Skip the noninitialized slots
		if(slot->convertCB == NULL)
		{
			continue;
		}

		// MGlobal::displayInfo(MString("CustomManipContainer::updateManips: updating manipulator slot ") + i); 
		
		// NOTE: Whenever we call a conversion function, we need to send the correct slot number
		MManipData newData = (this->*(slot->convertCB))(i+SLOT_OFFSET);

		// Make sure the MManipData types correspond...
		if(slot->dataPtr->isSimple())
		{   
			if(newData.isSimple())
			{
				// Copy the data over and ignore the type to be copied. it's up to the user to make sure the types are compatible.
				// MGlobal::displayInfo("CustomManipContainer::updateManips: copying simple data.");
				*(slot->dataPtr) = newData; 
			} // Ignore the newData. We can't copy it if it is not simple.
		} else
		{
			// Make sure newData isn't simple too...
			if(!newData.isSimple())
			{
				// Check that the object Functors are compatible.
				MObject target = slot->dataPtr->asMObject();
				MObject source = newData.asMObject();

				// MGlobal::displayInfo(MString("Source object = ") + source.apiTypeStr() + ", target object = " + target.apiTypeStr());
				// Check that the type of source is compatible with the target
				// if(target.hasFn(source.apiType()))
				// Make sure the source and target are identical
				if(target.apiType() == source.apiType())
				{
					// Types are compatible, copy the MObject
					// MGlobal::displayInfo("CustomManipContainer::updateManips: copying MObject data.");
					*(slot->dataPtr) = newData;
				}
			}
		}
	}
}

void CustomManipContainer::updatePlugs()
{
	MStatus stat;

	// Run all of the plug conversion callbacks here
	// MTrace("CustomManipContainer::updatePlugs: Updating all of the plug slots.");
	for(unsigned i=0; i<this->plugSlots.size(); i++)
	{
		PlugSlot* slot = (PlugSlot*)this->plugSlots[i];  
		// Skip the uninitialized slots
		if(slot->convertCB == NULL)
		{
			continue;
		}

		MPlug plug(slot->node, slot->attribute); 
		// MTrace(MString("CustomManipContainer::updatePlugs: Updating plug ") + plug.name());  
		// Get the data types the plug can work with
		// MTrace(MString("CustomManipContainer::updatePlugs: Plug data is of type: ") + slot->attribute.apiTypeStr());
				
		// NOTE: Whenever we call a conversion function, we need to send the correct slot number
		MManipData newData = (this->*(slot->convertCB))(i+SLOT_OFFSET);    

		if(newData.isSimple())
		{
			// MTrace("CustomManipContainer::updatePlugs: new Data is simple type.");
			// MTrace(MString("CustomManipContainer::updatePlugs: data = ") + newData.asDouble());
			stat = plug.setValue(newData.asDouble()); MAssertStatNoReturn(stat);
		} else
		{
			// MTrace(MString("CustomManipContainer::updatePlugs: new Data is MObject type: ") + newData.asMObject().apiTypeStr());
			MObject data = newData.asMObject();
			stat = plug.setValue(data); MAssertStatNoReturn(stat);
		}      
	}
}

void CustomManipContainer::addPlugToManipConversionCallback(unsigned slot, plugToManipConversionCallback callback, unsigned plugSlot)
{
	if(slot < SLOT_OFFSET)
	{
		MPxManipContainer::addPlugToManipConversionCallback(slot, callback);
	} else
	{ 
		// Undo the offset amount
		slot -= SLOT_OFFSET;
	
		MAssertNoReturn(slot < this->manipSlots.size());
		// Find the slot and over write the callback
		this->manipSlots[slot]->convertCB = callback;
		this->manipSlots[slot]->associatedPlugSlot = plugSlot;
	}
}

unsigned CustomManipContainer::addManipToPlugConversionCallback(MPlug& plug, manipToPlugConversionCallback callback, unsigned manipSlot)
{
	// Ignore the return value for the conversion callback because 
	// CustomManipContainer is saving the plug value anyway.
	MPxManipContainer::addManipToPlugConversionCallback(plug, callback);
	// Get the plug's slot
	unsigned slot = this->getPlugSlot(plug);
	// undo the offset amount
	slot -= SLOT_OFFSET;

	// Overwrite the manip->plug callback and the associated manipSlot
	this->plugSlots[slot]->convertCB = callback;
	this->plugSlots[slot]->associatedManipSlot = manipSlot;

	return(slot);
}

unsigned CustomManipContainer::getPlugSlot(MPlug& plug)
{
	MObject node = plug.node();
	MObject attr = plug.attribute();
	// Search for the plug in the plug slots list

	// If there, override it.
	unsigned slot = (unsigned)-1;
	for(signed i=0; i<this->plugSlots.size(); i++)
	{
		PlugSlot* plugSlot = (PlugSlot*)this->plugSlots[i];
		if(plugSlot->node == node && plugSlot->attribute == attr)
		{
			slot = i;
			break;
		}
	}

	if(slot == (unsigned)-1)
	{
		// If the slot is not found, register the plug.
		slot = this->registerPlug(node, attr);
	} else
	{
		// Add the offset for the plug slot
		MAssert(slot < SLOT_OFFSET);
		slot += SLOT_OFFSET;
	}
	return(slot);
}

// Conveniance getConvertor value functions
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, unsigned& value)
{
	MStatus stat;
	if(manipIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterManipValue(manipIndex, value); MAssertStat(stat);
	} else
	{
		// Undo the offset amount
		manipIndex -= SLOT_OFFSET;

		MAssert(manipIndex < this->manipSlots.size());
		ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
		value = slot->dataPtr->asUnsigned();
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, double& value)
{
	MStatus stat;
	if(manipIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterManipValue(manipIndex, value); MAssertStat(stat);
	} else
	{
		// Undo the offset amount
		manipIndex -= SLOT_OFFSET;
	
		MAssert(manipIndex < this->manipSlots.size());
		ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
		value = slot->dataPtr->asDouble();
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, MVector& value)
{
	MStatus stat;
	if(manipIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterManipValue(manipIndex, value); MAssertStat(stat);
	} else
	{
		// Undo the offset amount
		manipIndex -= SLOT_OFFSET;
	
		MAssert(manipIndex < this->manipSlots.size());
		ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
		MObject obj = slot->dataPtr->asMObject();
		MFnNumericData numerData(obj, &stat); MAssertStat(stat);
		stat = numerData.getData(value[0], value[1], value[2]); MAssertStat(stat);
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, MPoint& value)
{
	MStatus stat;
	if(manipIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterManipValue(manipIndex, value); MAssertStat(stat);
	} else
	{
		// Undo the offset amount
		manipIndex -= SLOT_OFFSET; 
		MAssert(manipIndex < this->manipSlots.size());
		ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
		MObject obj = slot->dataPtr->asMObject();
		value.cartesianize();
		MFnNumericData numerData(obj, &stat); MAssertStat(stat);
		stat = numerData.getData(value[0], value[1], value[2]); MAssertStat(stat);
	}
	return(MS::kSuccess); 
}
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, MDoubleArray& value)
{
	MAssert(manipIndex >= SLOT_OFFSET);
	// Undo the offset amount
	manipIndex -= SLOT_OFFSET;

	MAssert(manipIndex < this->manipSlots.size());
	ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
	MObject obj = slot->dataPtr->asMObject();
	MStatus stat;
	MFnDoubleArrayData arrayData(obj, &stat); MAssertStat(stat);
	stat = arrayData.copyTo(value); MAssertStat(stat);
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorManipValue(unsigned manipIndex, MMatrix& value)
{
	MStatus stat;
	if(manipIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterManipValue(manipIndex, value); MAssertStat(stat);
	} else
	{
		// Undo the offset amount
		manipIndex -= SLOT_OFFSET;

		MAssert(manipIndex < this->manipSlots.size());
		ManipSlot* slot = (ManipSlot*)this->manipSlots[manipIndex];
		MObject obj = slot->dataPtr->asMObject();
		MFnMatrixData matData(obj, &stat); MAssertStat(stat);
		value = matData.matrix(&stat); MAssertStat(stat);
	}
	return(MS::kSuccess);
}

MStatus CustomManipContainer::getConvertorPlugValue(unsigned plugIndex, int& value)
{
	MStatus stat;
	if(plugIndex < SLOT_OFFSET)
	{
		// Use the double convertorPlugValue so errors don't occur (leave it to Maya
		// to forget a getConvertorPlugValue(..., int&) signeture.
		double ret;
		stat = MPxManipContainer::getConverterPlugValue(plugIndex, ret); MAssertStat(stat);
		value = int(ret);
	} else
	{ 
		// Undo the offset amount
		plugIndex -= SLOT_OFFSET;

		MAssert(plugIndex < this->plugSlots.size());
		PlugSlot* slot = (PlugSlot*)this->plugSlots[plugIndex];
		MPlug plug(slot->node, slot->attribute);
		stat = plug.getValue(value); MAssertStat(stat);
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorPlugValue(unsigned plugIndex, double& value)
{
	MStatus stat;
	if(plugIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterPlugValue(plugIndex, value); MAssertStat(stat);
	} else
	{ 
		// Undo the offset amount
		plugIndex -= SLOT_OFFSET;

		MAssert(plugIndex < this->plugSlots.size());
		PlugSlot* slot = (PlugSlot*)this->plugSlots[plugIndex];
		MPlug plug(slot->node, slot->attribute);
		stat = plug.getValue(value); MAssertStat(stat);
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorPlugValue(unsigned plugIndex, MVector& value)
{
	MStatus stat;
	if(plugIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterPlugValue(plugIndex, value); MAssertStat(stat);
	} else
	{ 
		// Undo the offset amount
		plugIndex -= SLOT_OFFSET;
	
		MAssert(plugIndex < this->plugSlots.size());   
		PlugSlot* slot = (PlugSlot*)this->plugSlots[plugIndex];
		MPlug plug(slot->node, slot->attribute);
		MObject obj;
		stat = plug.getValue(obj); MAssertStat(stat);
		MFnNumericData numerData(obj, &stat); MAssertStat(stat);
		stat = numerData.getData(value[0], value[1], value[2]); MAssertStat(stat);
	}
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorPlugValue(unsigned plugIndex, MDoubleArray& value)
{
	MAssert(plugIndex >= SLOT_OFFSET);
	// Undo the offset amount
	plugIndex -= SLOT_OFFSET;

	MAssert(plugIndex < this->plugSlots.size());  
	PlugSlot* slot = (PlugSlot*)this->plugSlots[plugIndex];
	MPlug plug(slot->node, slot->attribute);
	MObject obj;
	MStatus stat = plug.getValue(obj); MAssertStat(stat);
	MFnDoubleArrayData arrayData(obj, &stat); MAssertStat(stat);
	stat = arrayData.copyTo(value); MAssertStat(stat);
	return(MS::kSuccess);
}
MStatus CustomManipContainer::getConvertorPlugValue(unsigned plugIndex, MMatrix& value)
{
	MStatus stat;
	if(plugIndex < SLOT_OFFSET)
	{
		stat = MPxManipContainer::getConverterPlugValue(plugIndex, value); MAssertStat(stat);
	} else
	{ 
		// Undo the offset amount
		plugIndex -= SLOT_OFFSET;

		MAssert(plugIndex < this->plugSlots.size());
		PlugSlot* slot = (PlugSlot*)this->plugSlots[plugIndex];
		MPlug plug(slot->node, slot->attribute);
		MObject obj;
		stat = plug.getValue(obj); MAssertStat(stat);
		MFnMatrixData matrixData(obj, &stat); MAssertStat(stat);
		value = matrixData.matrix(&stat); MAssertStat(stat);
	}
	return(MS::kSuccess);
}


static MString replace(const MString& currStr, char c, const char* r)
{
	std::string input = currStr.asChar(); 
	std::string output;

	for(std::string::const_iterator iter=input.begin(); iter!=input.end(); iter++)
	{
		if(*iter == c)
		{
			output += r;
		} else
		{
			output += *iter;
		}
	}

	MString final = output.c_str();

	// MTrace(MString("Input = ") + currStr + ", output = " + final);

	return(final);
}

/*****************************************
 * escapes the MString
 ****************************************/
static MString escape(const MString& str)
{
	MString temp = replace(str, '\\', "\\\\");
	temp = replace(temp, '\"', "\\\"");
	return(temp);
}

/** Saves the plug settings so undo can be automatically run when the manip is no longer 
 * dragged inside of <i>setPlugUndos</i>.
 * @return the current MStatus
 */
MStatus CustomManipContainer::getPlugSetCmds(MStringArray& strArray)
{
	MStatus stat = MS::kSuccess;

	// Clear the string array
	stat = strArray.clear(); MAssertStat(stat);

	// Save the plug settings for the different plug slots.
	for(unsigned i=0; i<this->plugSlots.size(); i++)
	{
		PlugSlot* slot = (PlugSlot*)this->plugSlots[i];  
		MPlug plug(slot->node, slot->attribute); 

		// Get the plug stringCmds before the modification
		MStringArray origCmds;
		stat = plug.getSetAttrCmds(origCmds, MPlug::kAll, true); MAssertStat(stat);

		// Put the plug name into it's proper place
		MString plugName = plug.name(&stat); MAssertStat(stat);  
		// MTrace(MString("CustomManipContainer::savePlugSettings, got plug: ") + plugName);
		for(unsigned j=0; j<origCmds.length(); j++)
		{
			// MTrace(MString("CustomManipContainer::savePlugSettings: before rebuild = ") + origCmds[j]);
			// NOTE: Move all of the string reformatting to setPlugUndos for a speed boost.

			// Remove the ; at the end
			unsigned idx = origCmds[j].rindex(';');
			origCmds[j] = origCmds[j].substring(0, idx-1);

			// MTrace(MString("CustomManipContainer::savePlugSettings, after removing ';', got = ") + origCmds[j]);
			// Break apart each set command     
			MStringArray arr;
			stat = origCmds[j].split(' ', arr); MAssertStat(stat);      

			// Pop off the setAttr part
			stat = arr.remove(0); MAssertStat(stat);

			// replace the .attribute with plugName
			// NOTE: Maybe we should prepend the node name to the ".attribute" bit.
			arr[0] = plugName;

			// Rebuild the string
			origCmds[j] = "";
			for(unsigned k=0; k<arr.length(); k++)
			{
				// MTrace(arr[k]);
				// Escape the string
				arr[k] = escape(arr[k]);
				// MTrace(arr[k]);
				origCmds[j] += MString(" \"") + arr[k] + "\"";              
			}

			// MTrace(MString("CustomManipContainer::savePlugSettings: after rebuild = ") + origCmds[j]);

			// Add origCmd to strArray
			stat = strArray.append(origCmds[j]); MAssertStat(stat);
		}


	}

	return(stat);
}

MStatus CustomManipContainer::setPlugUndos()
{
	MStatus stat;

	// Get the new plug set commands
	MStringArray newCmds;
	stat = this->getPlugSetCmds(newCmds); MAssertStat(stat);
	// Do a line-by-line comparsion of the changed plugs.
	MAssert(newCmds.length() == this->plugSettings.length());
	for(unsigned i=0; i<newCmds.length(); i++)
	{
		if(newCmds[i] != this->plugSettings[i])
		{
			// MTrace(MString("CustomManipContainer::setPlugUndos: Cmd changed: ") + newCmds[i]);

			// Take the strings
			MString redoStr = newCmds[i];
			MString undoStr = this->plugSettings[i];
			MString cmd = CustomManipSetAttrCmd::commandName + redoStr + undoStr;
			// MTrace(MString("CustomManipContainer::setPlugUndos: Executed command = ") + cmd);
			// Run the command
			stat = MGlobal::executeCommand(cmd, false, true); MAssertStat(stat);
		}
	}

	return(MS::kSuccess);
}

// Event handlers for the mouse input
void CustomManipContainer::onMotion(const CustomEvent& e)
{
	// MTrace("CustomManipContainer::onMouseMoved: called");
		
	// Do stuff, like update the selectedManipulator
	// MTrace("CustomManipContainer::onMouseMoved: updating selected manip");
	if(this->selectedManip != NULL)
	{ 
		this->selectedManip->onDrag(e);

		// Update the plugs for the selectedManip
		this->updatePlugs(); 

		// Force the view to refresh
		M3dView::active3dView().refresh(true, true);
	}
}

void CustomManipContainer::onPress(const CustomEvent& e)
{
	MStatus stat;

	// MTrace("CustomManipContainer::onMousePressed: button = " + e.getButton());
	// MTrace("CustomManipContainer::onMousePressed: left button = " + CustomEvent::kLeftMouse);
	// Return immediately if releaseCBID is not -1. Means that we're still waiting for a releaseCB
	if(this->releaseCBID != MCallbackId(-1))
	{
		return;
	}

	// Only activate on left mouse button for now
	if(e.getButton() == CustomEvent::kLeftButton)
	{
		// MGlobal::displayInfo(MString("CustomManipContainer::onPress: pos = (") + e.x + ", " + e.y + ")");
		this->selectedManip = this->select(e);
		// repeat...
		// this->selectedManip = this->select(e);
		if(this->selectedManip != NULL)
		{
			// MTrace("CustomManipContainer::onMousePressed: selected manip.");
			// Disable the selection changing while the manipulator is being dragged
			// NOTE: This is only needed if CustomManipContext is NOT used!!
			// stat = MGlobal::executeCommand("showManipCtx -edit -lockSelection true showManip3D"); MAssertStatNoReturn(stat);

			// Set global manipInUse to true
			CustomManipContainer::setActiveManip(this);

			// Begin updating the manipulator
			this->selectedManip->onPress(e);
			// Force the view to refresh
			stat = M3dView::active3dView().refresh(true, true); MAssertStatNoReturn(stat);

			stat = this->getPlugSetCmds(this->plugSettings); MAssertStatNoReturn(stat);   
		}
	}
}

// a callback function
void releaseCB(void* closure) { ((CustomManipContainer*)closure)->onReleaseCB(); }
void CustomManipContainer::onRelease(const CustomEvent& e)
{
	MStatus stat;

	// MTrace("CustomManipContainer::onMouseReleased: button = " + e.getButton());
	// Return immediately if releaseCBID is not -1. Means that we're still waiting for a releaseCB
	if(this->releaseCBID != MCallbackId(-1))
	{
		return;
	}

	// Only activate on left mouse button for now
	if(e.getButton() == CustomEvent::kLeftButton)
	{
		// MTrace("CustomManipContainer::onMouseReleased: releasing selected manip.");
		
		// Begin updating the manipulator
		if(this->selectedManip != NULL)
		{
			this->selectedManip->onRelease(e);  
			// Make no manip selected
			this->selectedManip = NULL;   

			// Force the view to refresh
			stat = M3dView::active3dView().refresh(true, true); MAssertStatNoReturn(stat);

			// Run a release callback to indicate that we are done dragging garranteed AFTER the context runs its onRelease code.
			this->releaseCBID = MEventMessage::addEventCallback("idleHigh", releaseCB, this, &stat); MAssertStatNoReturn(stat);
		}
	}
}
void CustomManipContainer::onReleaseCB()
{
	// MTrace("CustomManipContainer::onReleaseCB: called");
	MStatus stat;
	MAssertNoReturn(this->releaseCBID != MCallbackId(-1));
	stat = MMessage::removeCallback(this->releaseCBID);  MAssertStatNoReturn(stat);
	this->releaseCBID = MCallbackId(-1);
	// Set global manipInUse to false
	CustomManipContainer::setActiveManip(NULL);

	// Set the plug settings for the undo after the manip is released so 
	// the setAttr callback knows when that the customManip is released.
	stat = this->setPlugUndos(); MAssertStatNoReturn(stat);

	// Enable the selection changing
	// NOTE: This is only needed if CustomManipContext is NOT used!!
	// stat = MGlobal::executeCommand("showManipCtx -edit -lockSelection false showManip3D;"); MAssertStatNoReturn(stat);
}

// Platform independent method of sending each event to the observing ManipContainers
void processEvent(const CustomEvent& oldEvent)
{
	MStatus stat;

	if(oldEvent.getModifier() & CustomEvent::kAltKey) 
   	{
		if(oldEvent.getEventType() == CustomEvent::kMouseMoved)
		{  
			// Post a UserEventMessage here so we can receive that the view was changed.
			// MTrace("CustomManipContainer::processEvent: Alt key pressed and mouse moved.");
			stat = MUserEventMessage::postUserEvent(CustomManipContainer::ViewChangedEvent, NULL); MAssertStatNoReturn(stat);
		}
		// Always return on an alt modifier
		return;
	}

	// Build a new event
	CustomEvent event(oldEvent);
	M3dView view = M3dView::active3dView(&stat); MAssertStatNoReturn(stat);
	// view.beginGL must be called before any gl function is used!!
	stat = view.beginGL(); MAssertStatNoReturn(stat);
	{
		// flip the y value
		event.y = view.portHeight(&stat) - event.y; MAssertStatNoReturn(stat);
	}
	stat = view.endGL(); MAssertStatNoReturn(stat);

	// Check that the event is a button press and the
	switch(event.getEventType())
	{
	case(CustomEvent::kButtonPressed): 
		// Get the viewport size
		if(event.x < 0 || event.x >= view.portWidth() ||
		   event.y < 0 || event.y >= view.portHeight())
		{
			// MTrace(MString("Mouse out of bounds."));
			return;
		}
		{
			// Send the event to be processed to each of the manipContainers.
			for(ManipContainerVector::iterator iter = manipContainers.begin();
				iter != manipContainers.end();
				iter++)
			{
				// MTrace("CustomManipContainer::processEvent: button press");
				// Send the event
				(*iter)->onPress(event);

				// If the manipContainer is active, stop checking manips.
				if(CustomManipContainer::isActiveManip())
				{
					break;
				}
			}
		}
		break;
	case(CustomEvent::kMouseMoved):
		if(CustomManipContainer::isActiveManip())
		{  
			CustomManipContainer::getActiveManip()->onMotion(event);
		} // else call onMouseUpMotion event handler for all manip containers...
		break;
	case(CustomEvent::kButtonReleased):
		if(CustomManipContainer::isActiveManip())
		{  
			CustomManipContainer::getActiveManip()->onRelease(event);
		}
		break; 
	default:
		break; 
	}
}

// Static info for the activeManip...
CustomManipContainer* CustomManipContainer::_activeManip = NULL;
void CustomManipContainer::setActiveManip(CustomManipContainer* manip)
{
	CustomManipContainer::_activeManip = manip;
}
bool CustomManipContainer::isActiveManip()
{
	return(CustomManipContainer::_activeManip != NULL);
}
CustomManipContainer* CustomManipContainer::getActiveManip()
{
	return(CustomManipContainer::_activeManip);
}

/*
 * Platform dependent code follows
 */ 
#if defined (LINUX)
// Detected button type from experimentation
enum
{
	LEFT_BUTTON = 1,
	MIDDLE_BUTTON = 2,
	RIGHT_BUTTON = 3
};
CustomEvent::MouseButton convertButton(unsigned button)
{
	switch(button)
	{
	case LEFT_BUTTON: return(CustomEvent::kLeftButton);
	case MIDDLE_BUTTON: return(CustomEvent::kMiddleButton);
	case RIGHT_BUTTON: return(CustomEvent::kRightButton);
	default: return(CustomEvent::kNoButton);
	}
}

// Detected button state mask from experimentation...
enum
{
	SHIFT_MODIFIER = 1,
	CTRL_MODIFIER  = 4,
	ALT_MODIFIER   = 8,
};
CustomEvent::KeyModifier convertModifier(unsigned state)
{
	int mod = CustomEvent::kNoModifier;
	if(state & SHIFT_MODIFIER)
	{
		mod |= CustomEvent::kShiftKey;
	}
	if(state & CTRL_MODIFIER)
	{
		mod |= CustomEvent::kCtrlKey;
	}
	if(state & ALT_MODIFIER)
	{
		mod |= CustomEvent::kAltKey;
	}
	return((CustomEvent::KeyModifier)mod);
}

CustomEvent::EventType convertType(unsigned state)
{
	switch(state)
	{
	case(MotionNotify): return(CustomEvent::kMouseMoved); 
	case(ButtonPress): return(CustomEvent::kButtonPressed);
	case(ButtonRelease): return(CustomEvent::kButtonReleased);
	default: return(CustomEvent::kInvalidEventType);
	}
}

void inputCB(Widget w, XtPointer closure, XEvent* event, Boolean* continue_to_dispatch) 
{
	// MGlobal::displayInfo("inputCB: called...");
	// GLwDrawingAreaCallbackStruct* call_data = (GLwDrawingAreaCallbackStruct *) call;
	// XEvent* event = call_data->event;

	// *continue_to_dispatch = true;

	// MTrace(MString("Mouse was moved to (") + event->xbutton.x + ", " + event->xbutton.y + ")");
	// MTrace(MString("inputCB: ButtonMotion, button = ") + event->xbutton.button);  
	// MTrace(MString("inputCB: ButtonMotion, state = ") + event->xbutton.state);  
	CustomEvent e(event->xbutton.x, 
				  event->xbutton.y,
				  convertButton(event->xbutton.button), 
				  convertModifier(event->xbutton.state),
				  convertType(event->type));

	// Send the event to the platform independent event distributor
	processEvent(e); 
}

#elif defined(WIN32)

CustomEvent::KeyModifier getModifier()
{
	// Figure out which key modifiers are pressed.
	int modifier = CustomEvent::kNoModifier;
	if(GetAsyncKeyState(VK_SHIFT) & 0x8000)
	{
		// MGlobal::displayInfo("Shift key waswport size pressed.");
		modifier |= CustomEvent::kShiftKey;
	}
	if(GetAsyncKeyState(VK_CONTROL) & 0x8000)
	{
		// MGlobal::displayInfo("Ctrl key was pressed.");
		modifier |= CustomEvent::kCtrlKey;
	}
	if(GetAsyncKeyState(VK_MENU) & 0x8000)
	{
		// MGlobal::displayInfo("Alt key was pressed.");
		modifier |= CustomEvent::kAltKey;
	}
	return((CustomEvent::KeyModifier)modifier);
}

LRESULT CALLBACK inputCB(int nCode, WPARAM wParam, LPARAM lParam)
{
	// MGlobal::displayInfo(MString("inputCB called. nCode = ") + nCode); 

	// Only process the Message if nCode == HC_ACTION
	if(nCode == HC_ACTION)
	{
		// Process the message...
		// MGlobal::displayInfo("Processing message.");
		// MGlobal::displayInfo(MString("inputCB called. nCode = ") + nCode);
		// MGlobal::displayInfo(MString("activeContainer = ") + unsigned(activeContainer));   

		// Grab the event data
		MOUSEHOOKSTRUCT* event = (MOUSEHOOKSTRUCT*)lParam;
		// MGlobal::displayInfo(MString("Screen Mouse pos: (") + event->pt.x + ", " + event->pt.y + ")");
		// MGlobal::displayInfo(MString("Event HWND = ") + unsigned(event->hwnd));
		M3dView view = M3dView::active3dView();
		HWND currHWND = view.window();  
		// MGlobal::displayInfo(MString("View HWND = ") + unsigned(currHWND));

		// Ignore the returned event hwnd, it's not useful..
		POINT& pt = event->pt;
		if(ScreenToClient(currHWND, &pt) != 0)
		{
			// MGlobal::displayInfo(MString("Valid Mouse pos: (") + pt.x + ", " + pt.y + ")");

			// Initialize the event
			CustomEvent e(pt.x, pt.y);
			// Set the modifier
			e.setModifier(getModifier());

			// Mask for the previous button states. Used to track whether mouse buttons were pressed or released
			static SHORT prevButtonState = 0;

			// Get the mouse button states
			// MGlobal::displayInfo(MString("Left button state = ") + GetAsyncKeyState(VK_LBUTTON));
			// MGlobal::displayInfo(MString("Middle button state = ") + GetAsyncKeyState(VK_MBUTTON));
			// MGlobal::displayInfo(MString("Right button state = ") + GetAsyncKeyState(VK_RBUTTON));

			// Bit 0 is leftbutton, bit 1 is middle, bit 2 is right
			SHORT buttonState = 0;
			buttonState |= ((GetAsyncKeyState(VK_LBUTTON)>>15)&0x1)<<0;
			buttonState |= ((GetAsyncKeyState(VK_MBUTTON)>>15)&0x1)<<1;
			buttonState |= ((GetAsyncKeyState(VK_RBUTTON)>>15)&0x1)<<2;

			// Find out if a press of release happened.
			SHORT edgeMask = buttonState ^ prevButtonState;
			SHORT pressMask = buttonState & edgeMask;
			SHORT releaseMask = prevButtonState & edgeMask;
			// Save the previous button state.
			prevButtonState = buttonState;

			// Check if an edge event occurred. If not, then this is a motion event only
			if((edgeMask & 0x7) == 0)
			{
				// MGlobal::displayInfo("Mouse Motion only.");
				e.setEventType(CustomEvent::kMouseMoved);
			} else
			{
				// Check which button is pressed
				if(edgeMask & 1L<<0)
				{
					e.setButton(CustomEvent::kLeftButton);
				} else if(edgeMask & 1L<<1)
				{
					e.setButton(CustomEvent::kMiddleButton);
				} else if(edgeMask & 1L<<2)
				{
					e.setButton(CustomEvent::kRightButton);
				}

				// Check the event type
				if((pressMask & 0x7) != 0)
				{
					// MTrace("inputCB: got pressed button event.");
					e.setEventType(CustomEvent::kButtonPressed);
				} else if((releaseMask & 0x7) != 0)
				{
					e.setEventType(CustomEvent::kButtonReleased); 
				} // Check other masks...
			}
			processEvent(e);
		} else
		{
			MError("Error converting coordinates.");
		}
	}

	// Always continue to the next event.	
	return(CallNextHookEx(inputHook, nCode, wParam, lParam)); 
}
#endif



