/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// CatmullRomCurveManip.h - declarations for the CatmullRomCurveManip class

#ifndef _CATMULLROMCURVEMANIP_H_
#define _CATMULLROMCURVEMANIP_H_

#include "CustomManip3D.h"
#include "CustomEvent.h"
#include "CustomManipContainer.h"

#include <maya/MManipData.h>

class CatmullRomCurveManip : public CustomManip3D
{
protected:
	CatmullRomCurveManip(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);

public:
    static CatmullRomCurveManip* create(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);
    
    virtual void draw(M3dView& view);
    virtual void select(M3dView &view);
    
    MStatus connectToArrayPlug(MPlug& plug);

    // Getter/Setter methods for the class attributes
    void setDrawAxes(bool flag);
    void setDrawTangents(bool flag);
    void setLabel(const MString& label);
    void setPosition(const MVector& pos);
    void setSize(const MVector& size);

    bool getDrawAxes();
    bool getDrawTangents();
    MString getLabel();
    MVector getPosition();
    MVector getSize();

    unsigned controlPtsIndex();

    // event handler functions
    // Used when the container receives mouse events and has to update the manipulator
    virtual void onDrag(const CustomEvent &event);
    virtual void onRelease(const CustomEvent &event);
    virtual void onPress(const CustomEvent &event);

protected:
    // Slot datums
    // The controlPts for the bezier curve
    MManipData controlPts;

    // Class attributes
    // Draw the axes?
    bool drawAxes;
    // Draw the tangents
    bool drawTangents;
    // The label
    MString label;
    // The top-left position of the chart
    MVector position;
    // The bottom-right position of the chart
    MVector size;

    // Slots
    unsigned controlPtsSlot;

};


#endif
