/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Declarations for the viewport distamce manip....

#ifndef _VIEWPORTDISTANCEMANIP_H_
#define _VIEWPORTDISTANCEMANIP_H_

#include "CustomManip3D.h"
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MPoint.h>
#include <maya/MManipData.h>

class CustomEvent;

class ViewportDistanceManip : public CustomManip3D
{
protected:
	ViewportDistanceManip(CustomManipContainer* parent, MStatus* stat = NULL);

public:
    static ViewportDistanceManip* create(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);
    virtual void draw(M3dView& view);
    virtual void select(M3dView& view);
    // Update functions
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);
    
    MStatus connectToDistancePlug(MPlug& plug);
    MStatus connectToCenterPlug(MPlug& plug);
    
    // Setters for the ViewportDistance attributes
    void setCenter(const MPoint& point);
    void setDrawCenter(bool state);
    void setScalingFactor(double scale);
    void setLabel(MString label);
	enum TextPosition {
		kLeft = 0,
		kRight,
		kTop,
		kBottom,
		kCenter,
		kLast
	};
    MStatus setLabelPos(const TextPosition&);
	void setInfluencedByInteraction(bool);
    
    // Getters for the ViewportDistance attributes
    bool isDrawCenterOn() const;
    double scalingFactor() const;
    MString getLabel() const;
	TextPosition getLabelPosition() const;
    bool isInfluencedByInteraction() const;

    // The slot index getters
    unsigned centerIndex();
    unsigned radiusIndex();
    
protected:
    double getScale() const;
    
    // Slot datums
    // The center position
    MManipData center;
    // the radius of the manipulator
    MManipData radius;
    
    // Class attributes
    // scaling factor
    double scaleFactor;
    // Draw center flag
    bool drawCenter;
    // The manip's label
    MString label;
	// The offset to draw the label
	TextPosition labelPos;
    // Determines whether the manip should be zoomable and change size
    bool influencedByInteraction;
    
    // Slots
    unsigned centerSlot;
    unsigned radiusSlot;
    
private:
    void render(M3dView& view);
};

#endif
