/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// FreeTriadPointManip - replacement for Maya's broken FreePointTriadManip

#ifndef _FREETRIADPOINTMANIP_H_
#define _FREETRIADPOINTMANIP_H_

#include "CustomManip3D.h"
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MVector.h>
#include <maya/MManipData.h>

class CustomEvent;

class FreeTriadPointManip : public CustomManip3D
{
protected:
	FreeTriadPointManip(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);

public:
    static FreeTriadPointManip* create(CustomManipContainer* parent, MStatus* ReturnStatus = NULL);
    virtual void draw(M3dView& view);
    virtual void select(M3dView& view);
    // Update functions
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);
    
    MStatus connectToPointPlug(MPlug& plug);
    
    enum ManipPlane {
	kViewPlane = 0,
	kXYPlane,
	kXZPlane,
	kYZPlane 
    };

    // Setters for the manip attributes
    void setDrawAxes(bool state);
    void setDrawArrowHead(bool state);
    void setGlobalTriadPlane(ManipPlane plane);

    // Getters for the manip attributes
    bool isDrawAxesOn(void) const;
    bool isDrawArrowHeadOn(void) const;
    ManipPlane getGlobalTriadPlane(void) const;

    // The slot index getters
    // Slot for the center value. This works in absolute world coordinates
    unsigned pointIndex() { return(this->pointSlot); }

    // Component indices
    enum
    {
	kXAxis = 0,
	kYAxis,
	kZAxis,
	kPlane,
	kAll
    };
    
    
protected:    
    // The point data
    MManipData pointData;

    // ManipPlane 
    ManipPlane plane;
    // Draw axis flag
    bool drawAxes;
    // Draw arrow head flag
    bool drawArrowHead;
    
    // Slots
    unsigned pointSlot;
    
private:
    void renderPlane(M3dView& view, bool isSelect);
    void renderAxis(M3dView& view, int which);

    MStatus calcPrevPos(const CustomEvent& event, MVector& pos);

    double getScale(M3dView& view, MVector& center, MStatus* stat = NULL);
    double getHandleSize();

    // GLList for quickly rendering a GLUCylinder
    static GLuint renderList;

    // The previous hit position
    MVector prevPos;
};

#endif
