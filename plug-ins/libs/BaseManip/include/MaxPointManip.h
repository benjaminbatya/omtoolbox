/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Declarations for the viewport distamce manip....

#ifndef _MAXPOINTMANIP_H_
#define _MAXPOINTMANIP_H_

#include "CustomManip3D.h"
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MVector.h>
#include <maya/MManipData.h>

class CustomEvent;

/**
 * Simulates a 3D studio Max move manipulator. This is intended to demonstrate that
 * alternative manipulators are easy to build with the CustomManip framework.
 */
class MaxPointManip : public CustomManip3D
{
protected:
	MaxPointManip(CustomManipContainer* parent, MStatus* stat = NULL);

public:
    static MaxPointManip* create(CustomManipContainer* parent, MStatus* stat = NULL);
    virtual void draw(M3dView& view);
    virtual void select(M3dView& view);
    // Update functions
    virtual void onPress(const CustomEvent& event);
    virtual void onDrag(const CustomEvent& event);
    virtual void onRelease(const CustomEvent& event);
    
    MStatus connectToPointPlug(MPlug& plug);

    // Setters for the manip attributes
    void setDrawAxes(bool);
    void setDrawArrowHead(bool);
    void setDrawPlanes(bool);

    // Getters for the manip attributes
    bool isDrawAxesOn(void) const;
    bool isDrawArrowHeadOn(void) const;
    bool isDrawPlanesOn(void) const;
    
    // The slot index getters
    unsigned pointIndex();

    // Component indices
    enum
    {
	kXAxis = 0,
	kYAxis,
	kZAxis,
	kYZPlane,
	kXZPlane,
	kXYPlane,
	kAll
    };
    
    
protected:    
    // The point data
    MManipData point;

    // Draw flags
    bool drawAxes;
    bool drawArrowHead;
    bool drawPlanes;

    // Slots
    unsigned pointSlot;
    
private:
    void renderPlane(M3dView& view, int which);
    void renderAxis(M3dView& view, int which);

    MStatus calcPrevPos(const MVector&, const CustomEvent&);

    // GLList for quickly rendering a GLUCylinder
    static GLuint renderList;

    // The previous hit position
    MVector prevPos;
};

#endif
