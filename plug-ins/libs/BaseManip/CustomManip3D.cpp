/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// CustomManip3D - Base class for all custom manipulators
#include "CustomManip3D.h"
#include "CustomManipContainer.h"

#include <maya/MGlobal.h>
#include <maya/MCommandResult.h>
#include <maya/MColor.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnCamera.h>

#include <Log/MayaLog.h>

// Default constructor
CustomManip3D::CustomManip3D(CustomManipContainer* parent, unsigned numComponents, MStatus* stat) : parent(parent), numComponents(numComponents)
{ 
	MValidateStatPtr(stat);
	*stat = MS::kSuccess;
	// Add this to the parent
	this->parent->addManip(this);
	
	this->component = this->kNoComponentSelected;
    this->componentEnabled = NULL;

	// Allocate the correct number of components
    this->componentEnabled = new bool[numComponents];
	MAssertNoReturn(this->componentEnabled != NULL);
    // Initially set them to all true
    for(unsigned i=0; i<numComponents; i++)
    {
		this->componentEnabled[i] = true;
    }
}

CustomManip3D::~CustomManip3D()
{
    if(this->componentEnabled != NULL)
    {
		delete[](this->componentEnabled);
    }
}

CustomManip3D* CustomManip3D::create(CustomManipContainer* parent, MStatus* stat)
{
	MValidateStatPtr(stat);
	// This must be overriden
	MAssertReturnObj(false, NULL);
}

void CustomManip3D::setComponent(unsigned component)
{
    MAssertNoReturn(component < this->numComponents ||
		component == this->kNoComponentSelected);
	this->component = component;
}
unsigned CustomManip3D::getComponent(void)
{
    return(this->component);
}
bool CustomManip3D::isActive()
{
    return(this->component != this->kNoComponentSelected);
}

void CustomManip3D::setComponentEnabled(unsigned component, bool state)
{
    MAssertNoReturn(component < this->numComponents);
    this->componentEnabled[component] = state;
}
bool CustomManip3D::isComponentEnabled(unsigned component)
{
    MAssertReturnObj(component < this->numComponents, false);
    return(this->componentEnabled[component]);
}

// Update functions
void CustomManip3D::onPress(const CustomEvent& e)
{
	// Do nothing
}
void CustomManip3D::onDrag(const CustomEvent& e)
{
    // Do nothing
}
void CustomManip3D::onRelease(const CustomEvent& e)
{
    // Set this manipulator as not active
	this->setComponent(this->kNoComponentSelected);
}

// Return an index value for a specified color.
// Active indicates whether to retrive the active (true) or dormant (false) colors
int CustomManip3D::queryDisplayColor(MString color, bool active)
{
    // Get the active polyVertex and polyEdge colors
    MString cmd = "displayColor -q "; 
    if(active)
    {
	cmd += MString("-active ") + color; 
    } else
    {
	cmd += MString("-dormant ") + color; 
    }
    MCommandResult res;
    MGlobal::executeCommand(cmd, res);
    int idx;
    res.getResult(idx);
    // Subtract one to move the indice to M3dView 0 index based values 
    idx--;
    
    return(idx);
}	

MColor CustomManip3D::queryColorIndex(int idx)
{
    MCommandResult res;
    // Add one to move the indice to MEL 1-based values
    idx++;
    MGlobal::executeCommand(MString("colorIndex -q ") + idx, res);
    MDoubleArray colorArray;
    res.getResult(colorArray);
    return(MColor(colorArray[0], colorArray[1], colorArray[2]));
}

// NOTE: Refactor to move return MStatus instead of the matrix
MStatus CustomManip3D::getProjectionMatrix(M3dView& view, float mat[][4])
{
    MStatus stat;

    // Push the projection matrix onto the stack
    MFloatMatrix projMat;
    MDagPath cameraPath;
    stat = view.getCamera(cameraPath); MAssertStat(stat);
    MFnCamera cameraFn(cameraPath, &stat); MAssertStat(stat);
    projMat = cameraFn.projectionMatrix(&stat); MAssertStat(stat);
    
    stat = projMat.get(mat); MAssertStat(stat);

    // Convert from Left-handed to right-handed as per Maya docs
    mat[2][2] *= -1;
    mat[3][2] *= -1;
    // We need adjust the Y scaling to take in account for the aspect ratio...
    // For some reason, the projection Matrix doesn't. 1.333 Seems to be the RIGHT scaling value... weird
    // Maya's camera's aspect ratio has no effect...
    mat[1][1] /= double(view.portHeight())/double(view.portWidth()) * 1.333;

    return(stat);
}

// Empric value for making the scale factor 1
#define NORMALDIST 40.0
double CustomManip3D::getForeshortenScale(M3dView& view, MVector& center, MStatus* stat) const
{
    MValidateStatPtr(stat);

    // Calculate the scale factor
    MPoint rayStart;
    MVector rayDirection;
    *stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection); MAssertStatReturnObj(*stat, 0);
    // RayDirection is normalized already... just find the difference between rayStart and center
    MVector temp = center - rayStart;
    // The scale factor
    return(temp * rayDirection / NORMALDIST); 
}


/**
 * Find the projected point onto an axis
 * Equation from http://www.magic-software.com/Documentation/DistanceLine3Line3.pdf Eberly's site
 */
MStatus CustomManip3D::calcClosestPtToAxis(const MVector& rayStart, const MVector& rayDirection, 
					   const MVector& pos, const MVector& axis, 
					   MVector& retVal)
{
    // Calculate the variables we need
    double a = rayDirection * rayDirection; // m0 * m0
    double b = rayDirection * axis * -1.0; // m0 * m1 * -1; 
    double c = axis * axis; // m1 * m1
    double d = rayDirection * (rayStart - pos); // m0 * (b0 - b1)
    double e = axis * (rayStart - pos) * -1.0;  // m1 * (b0 - b1) * -1
    // Don't need to calculate f since it disappears in the differencation
    
    double det = a*c - b*b;
    // Invalid should be able to happen
    MCheck(det > 0.00001, "calcClosestPt: abnormal error, det is supposed to be much greater then zero.");
    
    double t = (b*d - a*e)/det;
    
    retVal = (axis * t) + pos; // position + XAxis * t; 

    return(MS::kSuccess);
}

MStatus CustomManip3D::calcPlaneLineIntersection(const MVector& rayStart, const MVector& rayDirection,
						 const MVector& pos, const MVector& normal,
						 MVector& retVal)
{
    // Determine the point of intersection between the line and plane
    // Solution from http://astronomy.swin.edu.au/~pbourke/geometry/planeline/
    double denom = normal * rayDirection;
    // Check if denom is really small
    if((denom*denom) < 0.00001)
    {
	return(MS::kFailure);
    }

    double numer = normal * (pos - rayStart);
    // MTrace(MString("calcPlaneLineIntersection: denom = ") + denom);
    // MTrace(MString("calcPlaneLineIntersection: numer = ") + numer);
    double u = numer / denom;
    
    retVal = rayStart + (rayDirection * u);
    return(MS::kSuccess);
}
