/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "CustomScaleManip.h"
#include "CustomManipContainer.h"
#include "CustomEvent.h"

#include <GL/glu.h>
#include <math.h>

#include <maya/MPoint.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MColorArray.h>
#include <maya/M3dView.h>

#include <Format/Format.h>
#include <Log/MayaLog.h>

// Initialize the renderList so it is initialized the first time inside the draw method 
GLuint CustomScaleManip::renderList = (GLuint)-1;

const ManipColor CustomScaleManip::manipColors[] = 
{
	{"Xaxis", false},
	{"Yaxis", false},
	{"Zaxis", false},
	{"manipHandle", true}
};

CustomScaleManip* CustomScaleManip::create(CustomManipContainer* parent, MStatus *stat)
{
	// make sure the pointer is ok.
	MValidateStatPtr(stat);

	// Create manip
	CustomScaleManip* manip = new CustomScaleManip(parent, stat); MAssertStatReturnObj(*stat, NULL); 
	MAssertReturnObj(manip != NULL, NULL);

	return(manip);
};

CustomScaleManip::CustomScaleManip(CustomManipContainer* parent, MStatus* stat) : CustomManip3D(parent, CustomScaleManip::kAll, stat)
{ 
	MValidateStatPtr(stat);

	// Create a numeric data functor
	MFnNumericData numericFn;
	// Create the pivot data object
	this->scalePivotData = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	// Set the data to default
	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the pivot Slot
	this->scalePivotSlot = this->getParent()->registerManip(&this->scalePivotData);

	// Create the scale Data object
	this->scaleData = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	// Set the data to default
	*stat = numericFn.setData(1.0, 1.0, 1.0); MAssertStatNoReturn(*stat);
	// Register the slot with the parent
	this->scaleSlot = this->getParent()->registerManip(&this->scaleData);

	// Initialize the axes
	this->axes[0] = MVector::xAxis; 
	this->axes[1] = MVector::yAxis; 
	this->axes[2] = MVector::zAxis;
	// Clear scaleVec
	this->scaleVec = MVector::zero;

	// Reset currScale
	this->currScale = 1.0;
}

double CustomScaleManip::getScale(M3dView& view, MVector& center, MStatus* stat)
{
	return(this->getForeshortenScale(view, center, stat) * this->globalSize()) * 6;
}
double CustomScaleManip::getHandleSize()
{
	return((this->handleSize()-4) / 630);
}

void CustomScaleManip::draw(M3dView& view)
{
	MStatus stat;

	this->setupDrawingData();

	// Get the center data
	MVector pivot;
	MFnNumericData numericFn(this->scalePivotData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pivot.x, pivot.y, pivot.z); MAssertStatNoReturn(stat);

	// The scale factor
	double scaleFactor = this->getScale(view, pivot, &stat); MAssertStatNoReturn(stat);
	// MTrace(MString("CustomScaleManip::draw: scaleFactor = ") + scaleFactor);
	double handleScale = scaleFactor * this->getHandleSize();

	// Get the color indices
	MColorArray colors;
	for(int i=0; i<this->kAll; i++)
	{
		MColor c;
		if(i == this->getComponent())
		{
			c = this->queryColorIndex(this->queryDisplayColor("manipCurrent", true));
		} else
		{  
			c = this->queryColorIndex(this->queryDisplayColor(this->manipColors[i].name, this->manipColors[i].active));  
		}
		stat = colors.append(c); MAssertStatNoReturn(stat);
	}

	// Set up lighting
	glPushAttrib(GL_LIGHTING_BIT);
	{ 
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glTranslated(pivot.x, pivot.y, pivot.z);     

			for(int i=this->kXAxis; i<=this->kZAxis; i++)
			{
				if(!this->isComponentEnabled(i))
				{
					continue;
				}

				MPoint rayStart;
				MVector rayDirection;
				view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection);
				double mag = rayDirection * this->axes[i];
				if(mag*mag > 0.9)
				{
					// Don't draw the manip   
					continue;
				}

				// Set the material
				GLfloat color[4];
				colors[i].get(color); color[3] = 1.0;
				// Set the material color
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);

				glPushMatrix();
				{
					MVector dir = this->axes[i] * scaleFactor; 
					// Stretch dir by the scale amount
					if(this->scaleVec[i] == 1)
					{
						dir *= this->currScale;
					}

					// Draw the line
					glBegin(GL_LINES);
					{
						glVertex3f(0, 0, 0);
						glVertex3f(dir.x, dir.y, dir.z);
					}
					glEnd();

					// Move to the location
					glTranslated(dir.x, dir.y, dir.z);   
					glScaled(handleScale, handleScale, handleScale);
					// Render the object.
					glCallList(this->renderList);
				}
				glPopMatrix();
			}

			// Render the global scale cube
			// Set the material
			GLfloat color[4];
			colors[this->kGlobal].get(color); color[3] = 1.0;
			// Set the material color
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
			glScaled(handleScale, handleScale, handleScale);
			if(this->isComponentEnabled(this->kGlobal))
			{   
				glCallList(this->renderList);
			}
		}
		glPopMatrix();
	}
	glPopAttrib();
}

void CustomScaleManip::select(M3dView& view)
{
	MStatus stat; 

	// Get the center data
	MVector pivot;
	MFnNumericData numericFn(this->scalePivotData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pivot.x, pivot.y, pivot.z); MAssertStatNoReturn(stat);

	// The scale factor
	double scaleFactor = this->getScale(view, pivot, &stat); MAssertStatNoReturn(stat);
	double handleScale = scaleFactor * this->getHandleSize();

	GLfloat mat[4][4];
	stat = this->getProjectionMatrix(view, mat); MAssertStatNoReturn(stat); 

	// Setup the correct projection matrix
	glMultMatrixf((GLfloat*)mat);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{
		glTranslated(pivot.x, pivot.y, pivot.z);

		for(int i=this->kXAxis; i<=this->kZAxis; i++)
		{
			if(!this->isComponentEnabled(i))
			{
				continue;
			}

			glLoadName(i);
			glPushMatrix();
			{   
				MPoint rayStart;
				MVector rayDirection;
				view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection);
				double mag = rayDirection * this->axes[i];
				if(mag*mag > 0.9)
				{
					// Don't draw the manip   
					continue;
				}
				
				MVector dir = this->axes[i] * scaleFactor;
				// move to the correct location
				glTranslated(dir.x, dir.y, dir.z);   
				glScaled(handleScale, handleScale, handleScale);
				// Render the object
				glCallList(this->renderList);
			}
			glPopMatrix();
		}

		// render the global scale
		glLoadName(this->kGlobal);
		glScaled(handleScale, handleScale, handleScale);
		if(this->isComponentEnabled(this->kGlobal))
		{  
			glCallList(this->renderList);
		}
	}
	glPopMatrix(); 
}   

MStatus CustomScaleManip::connectToScalePlug(MPlug& plug)
{
	MStatus stat;

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	MAssert(attr.apiType() == MFn::kAttribute3Double);

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug,
		(manipToPlugConversionCallback)&CustomManipContainer::k3DoubleMToPConversionCallback,
		this->scaleIndex());
	this->getParent()->addPlugToManipConversionCallback(this->scaleIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot); 
	return(stat);
}
MStatus CustomScaleManip::connectToScalePivotPlug(MPlug& plug)
{
	MStatus stat;

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	MAssert(attr.apiType() == MFn::kAttribute3Double);

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->getPlugSlot(plug);

	this->getParent()->addPlugToManipConversionCallback(this->scalePivotIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot); 
	return(stat);
}

// Update functions
void CustomScaleManip::onPress(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onPress(event);  

	M3dView view = M3dView::active3dView(&stat); MAssertStatNoReturn(stat);
	// Get the pivot position
	MVector pivot;
	MFnNumericData numericFn(this->scalePivotData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pivot.x, pivot.y, pivot.z); MAssertStatNoReturn(stat);
	
	// Record the current position
	// If the global is selected, record the mouse position in view space 
	if(this->getComponent() == this->kGlobal)
	{
		// Scaling is based on distance from a point 10 pixels left of the pivot so inverted
		// scaling is possible
		this->prevDistance = 10;
		// Scale on all three axes
		this->scaleVec = MVector(1, 1, 1);
	} else
	{
		// Get the selected axis
		MVector selectedAxis = this->axes[this->getComponent()];

		// Get the mouse ray
		MPoint rayStart;
		MVector rayDirection, axisPt;
		stat = view.viewToWorld(event.x, event.y, rayStart, rayDirection); MAssertStatNoReturn(stat);
		// Calc the position in world space constrained to the selected axis   
		stat = this->calcClosestPtToAxis(rayStart, rayDirection, pivot, selectedAxis, axisPt); MAssertStatNoReturn(stat);

		// Calculate the prevDistance as the dot of axisPt and this->selectedAxis
		this->prevDistance = (axisPt - pivot) * selectedAxis; 
		if(this->prevDistance < 0.000001) { this->prevDistance = 0.000001; }

		// Scale on only the selected axis
		for(int i=0; i<3; i++)
		{
			this->scaleVec[i] = fabs(selectedAxis[i]);   
		}  
		// Adjust for control
		if(event.getModifier() & CustomEvent::kCtrlKey)
		{
			// set scaleVector to it's complement vector
			this->scaleVec = MVector(1, 1, 1) - this->scaleVec;
		}
	} 

	// MTrace(MString("CustomScaleManip::onPress: prevDistance = ") + this->prevDistance); 
}
void CustomScaleManip::onDrag(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onDrag(event);

	M3dView view = M3dView::active3dView(&stat); MAssertStatNoReturn(stat);
	
	// Get the pivot position
	MVector pivot;
	MFnNumericData numericFn(this->scalePivotData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pivot.x, pivot.y, pivot.z); MAssertStatNoReturn(stat);

	// Calculate the current scale
	double currDistance;
	if(this->getComponent() == this->kGlobal)
	{
		// Scaling is based on distance from a point 10 pixels left of the pivot so inverted
		// scaling is possible
		short xPos, yPos;
		view.worldToView(pivot, xPos, yPos, &stat); MAssertStatNoReturn(stat);
		currDistance = event.x - (xPos - 10);
	} else
	{ 
		// Get the selected axis
		MVector selectedAxis = this->axes[this->getComponent()];

		// Get the mouse ray
		MPoint rayStart;
		MVector rayDirection, axisPt;
		stat = view.viewToWorld(event.x, event.y, rayStart, rayDirection); MAssertStatNoReturn(stat);
		// Calc the position in world space constrained to the selected axis   
		stat = this->calcClosestPtToAxis(rayStart, rayDirection, pivot, selectedAxis, axisPt); MAssertStatNoReturn(stat);      

		// Calculate the currDistance as the dot of axisPt and this->selectedAxis
		currDistance = (axisPt - pivot) * selectedAxis; 
	}
	if(currDistance == 0.0) { currDistance = this->prevDistance; }

	// Calculate the delta scale
	MVector deltaScale = this->scaleVec;
	deltaScale *= currDistance / this->prevDistance;

	// Add the complementry axes
	deltaScale += MVector(1, 1, 1) - this->scaleVec; 

	// Update the currentScale
	this->currScale *= currDistance / this->prevDistance;
	
	MTrace("CustomScaleManip::onDrag: current scale = " + this->currScale);
	// Set current scale as the previous scale 
	this->prevDistance = currDistance; 

	// Set the scale
	stat = numericFn.setObject(this->scaleData.asMObject()); MAssertStatNoReturn(stat);
	// Get the value
	MVector prevScale;
	stat = numericFn.getData(prevScale[0], prevScale[1], prevScale[2]); MAssertStatNoReturn(stat);
	prevScale[0] *= deltaScale[0]; prevScale[1] *= deltaScale[1]; prevScale[2] *= deltaScale[2];
	// Finally set the new scale data
	stat = numericFn.setData(prevScale[0], prevScale[1], prevScale[2]); MAssertStatNoReturn(stat);
}
void CustomScaleManip::onRelease(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onRelease(event); 
 
	// Flip the axes if the scaling is negetive
	if(this->currScale < 0)
	{ 
		for(int i=0; i<3; i++)
		{
			// Only flip axes which were scaled
			if(this->scaleVec[i] != 0.0)
			{
				this->axes[i][i] *= -1.0;
			} 
		}
	}
	// Clear scaleVec
	this->scaleVec = MVector::zero;
	// Reset currScale
	this->currScale = 1.0;
}

void CustomScaleManip::setupDrawingData()
{
	// Set up the calllist for one loop
	if(CustomScaleManip::renderList != (GLuint)-1)
	{
		return;
	}
	// Make one list
	CustomScaleManip::renderList = glGenLists(1);

	// Draw a cube with radius 1
	GLfloat verts[][3] = {
		{-1, -1, -1},
		{-1, -1, +1},
		{-1, +1, -1},
		{-1, +1, +1},
		{+1, -1, -1},
		{+1, -1, +1},
		{+1, +1, -1},
		{+1, +1, +1}
	};
	// Set up the normals for lighting
	GLfloat norms[][3] = {
		{0, 0, -1},
		{+1, 0, 0},
		{0, 0, +1},
		{-1, 0, 0},
		{0, -1, 0},
		{0, +1, 0}
	};
	GLubyte faces[][4] = {
		{2, 6, 4, 0}, 
		{6, 7, 5, 4}, 
		{7, 3, 1, 5},
		{3, 2, 0, 1},
		{4, 5, 1, 0},
		{3, 7, 6, 2}
	};

	glNewList(CustomScaleManip::renderList, GL_COMPILE);
	{
		glBegin(GL_QUADS);
		{
			for(int i=0; i<6; i++)
			{
				// Draw the normal first
				glNormal3f(norms[i][0], norms[i][1], norms[i][2]);
				// Draw the verts
				GLubyte* face = faces[i];
				for(int j=0; j<4; j++)
				{
					GLubyte vertIdx = face[j];
					GLfloat* vert = verts[vertIdx];
					glVertex3f(vert[0], vert[1], vert[2]);
				}
			}
		}
		glEnd();
	}
	glEndList();
}

