/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/****************************************
 * CustomManipSetAttrCmd.cpp - definitions of methods and static members.
 ***************************************/
 
#include "CustomManipSetAttrCmd.h"
#include <Log/MayaLog.h>

#include <maya/MArgList.h>

const MString CustomManipSetAttrCmd::commandName("SOB_SS_SetAttr");

MStatus CustomManipSetAttrCmd::doIt(const MArgList& argList)
{
    MStatus stat = MS::kSuccess;
    // MTrace(MString("CustomManipSetAttrCmd::doIt: argList = ") + argList.asString(0));

    // cmd looks like "__DPSI_SS_SetAttr__ SoftSelectNode1.falloffRadius redoArgs undoArgs"

    // Make sure there is more then 1 arguments
    MAssert(argList.length() > 1);
    // Make sure there is an even number of args
    MAssert(argList.length()%2 == 0);

    // get the arguments and set the internal data.

    // Get the arguments, half of which are the redo arguments and half are the undo arguments
    // Get the undo arguments
    unsigned i;
    for(i=0; i<argList.length()/2; i++)
    {
	this->redoArgs += MString(" ") + argList.asString(i, &stat); MAssertStat(stat);
    }

    // Get the redo arguments
    for(i=argList.length()/2; i<argList.length(); i++)
    {
	this->undoArgs += MString(" ") + argList.asString(i, &stat); MAssertStat(stat);
    }

    return(this->redoIt());
}

MStatus CustomManipSetAttrCmd::undoIt()
{
    return(MGlobal::executeCommand(MString("setAttr ") + this->undoArgs));
}

MStatus CustomManipSetAttrCmd::redoIt()
{
    // MTrace(MString("redo args: ") + this->redoArgs);

    return(MGlobal::executeCommand(MString("setAttr ") + this->redoArgs));
}

