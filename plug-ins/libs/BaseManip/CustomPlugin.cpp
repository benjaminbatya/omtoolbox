/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/********************************
 * CustomPlugin.cpp - registers all of the nodes required to use the CustomManip framework
 ********************************/
 
#define MNoVersionString
 
#include <Log/MayaLog.h>
#include <maya/MUserEventMessage.h>

#ifdef WIN32
#undef NT_PLUGIN
#endif
#include <maya/MFnPlugin.h>
#ifdef WIN32
#define NT_PLUGIN
#endif

#include "CustomPlugin.h"
#include "CustomManipSetAttrCmd.h"
#include "CustomManipContainer.h"

MStatus CustomPlugin::initialize(MFnPlugin& plugin)
{
    MTrace("CustomPlugin::initialize: Registering the Custom Commands.");

    MStatus stat = plugin.registerCommand(CustomManipSetAttrCmd::commandName, CustomManipSetAttrCmd::creator); MAssertStat(stat);

	// Register the userEvent
	stat = MUserEventMessage::registerUserEvent(CustomManipContainer::ViewChangedEvent); MAssertStat(stat);

    return(stat);
}

MStatus CustomPlugin::deinitialize(MFnPlugin& plugin)
{
    MTrace("CustomPlugin::deinitialize: Deregistering the Custom Commands.");
    
	// Unregister the userEvent
	MStatus stat = MUserEventMessage::deregisterUserEvent(CustomManipContainer::ViewChangedEvent); MAssertStat(stat);  

	stat = plugin.deregisterCommand(CustomManipSetAttrCmd::commandName); MAssertStat(stat);

    return(stat);
}
