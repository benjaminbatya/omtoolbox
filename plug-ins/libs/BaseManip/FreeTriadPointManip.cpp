/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "FreeTriadPointManip.h"
#include "CustomManipContainer.h"
#include "CustomEvent.h"

#include <GL/glu.h>
#include <math.h>

#include <maya/MFloatMatrix.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>

#include <Format/Format.h>
#include <Log/MayaLog.h>

// Initialize the renderList so it is initialized the first time inside the draw method
GLuint FreeTriadPointManip::renderList = (GLuint)-1;

FreeTriadPointManip* FreeTriadPointManip::create(CustomManipContainer* parent, MStatus *stat)
{
	MValidateStatPtr(stat);

	// Create manip
	FreeTriadPointManip* manip = new FreeTriadPointManip(parent, stat); MAssertStatReturnObj(*stat, NULL);
	MAssertReturnObj(manip != NULL, NULL);

	return(manip);
}

FreeTriadPointManip::FreeTriadPointManip(CustomManipContainer* parent, MStatus *stat) : CustomManip3D(parent, FreeTriadPointManip::kAll, stat)
{
	MValidateStatPtr(stat);
	// Create a numeric data object for center
	MFnNumericData numericFn;
	// Create an MObject representing the position data
	this->pointData = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the centerSlot manipulator
	this->pointSlot = this->getParent()->registerManip(&this->pointData);

	// Default is viewPlane and to draw everything
	this->plane = FreeTriadPointManip::kViewPlane;
	this->drawAxes = true;
	this->drawArrowHead = true;
};

void FreeTriadPointManip::draw(M3dView& view)
{
	// Get the active color for the current manipulator component
	int activeColor = this->queryDisplayColor("manipCurrent", true); 

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{ 
		// Add lighting...
		glPushAttrib(GL_LIGHTING_BIT);
		{ 
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);

			static char* axis[3] = {"Xaxis", "Yaxis", "Zaxis"}; 
			// Render the axis bars
			for(int i=FreeTriadPointManip::kXAxis; i<FreeTriadPointManip::kPlane; i++)
			{
				// Set the color 
				int idx;
				if(this->getComponent() == i)
				{
					// Set the current drawing color to the active color
					idx = activeColor;
				} else
				{
					// view.setDrawColor(getColor(axis[i], false));
					idx = this->queryDisplayColor(axis[i], false);
				}
				MColor c = this->queryColorIndex(idx);
				GLfloat color[4];
				c.get(color); color[3] = 1.0;
				// Set the material color
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
				this->renderAxis(view, i); 
			}
		} 
		glPopAttrib();

		// Set the color
		if(this->getComponent() == FreeTriadPointManip::kPlane)
		{
			// Set the current drawing color
			view.setDrawColor(activeColor); 
		} else
		{
			view.setDrawColor(this->queryDisplayColor("manipHandle", true));
		}
		
		// If the Plane == viewPlane, Load the identity
		if(this->plane == this->kViewPlane)
		{
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			{
				// Load the identity to clear the matrix stack.
				glLoadIdentity();
				this->renderPlane(view, false);
			}
			glMatrixMode(GL_PROJECTION);
			glPopMatrix(); 
		} else
		{
			this->renderPlane(view, false);
		}  
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void FreeTriadPointManip::select(M3dView& view)
{
	MStatus stat;
	
	// MTrace("FreeTriadPointManip::select");

	GLfloat mat[4][4];
	stat = this->getProjectionMatrix(view, mat); MAssertStatNoReturn(stat);

	glLoadName(FreeTriadPointManip::kPlane);  
	if(this->plane != this->kViewPlane)
	{
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		{   
			// Push the right projection matrix
			glMultMatrixf((GLfloat*)mat);
			// Render the plane
			this->renderPlane(view, true);
		}
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();  
	} else
	{
		// Render the plane
		this->renderPlane(view, true);
	}  

	// Render the axis bars
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Push matrix
		glMultMatrixf((GLfloat*)mat);

		// Render Axes
		for(int idx=FreeTriadPointManip::kXAxis; idx<=FreeTriadPointManip::kZAxis; idx++)
		{
			glLoadName(idx);
			this->renderAxis(view, idx);
		}
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();   
}

void FreeTriadPointManip::renderPlane(M3dView& view, bool isSelect)
{
	// If the component is not enabled, skip...
	if(!this->isComponentEnabled(this->kPlane))
	{
		return;
	}

	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->pointData.asMObject());
	numericFn.getData(center.x, center.y, center.z);  

	if(this->getGlobalTriadPlane() == this->kViewPlane)
	{
		// MGlobal::displayInfo("FreeTriadPointManip::render: drawing View plane.");	
		// Get the position of the center in view coordinates
		short x, y;
		view.worldToView(center, x, y);
		double totalScale = this->getHandleSize() * 54;

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		{ 
			// Setup the 2D projection matrix
			gluOrtho2D(0.0, (GLdouble)view.portWidth(),
					   0.0, (GLdouble)view.portHeight());

			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			{ 
				glLoadIdentity();
				glTranslated(x, y, 0);
				glScaled(totalScale, totalScale, totalScale);  
				if(isSelect)
				{
					glBegin(GL_POLYGON);
				} else
				{
					glBegin(GL_LINE_LOOP);
				}
				{
					glVertex2f(-1, -1);
					glVertex2f(-1, +1);
					glVertex2f(+1, +1);
					glVertex2f(+1, -1);
				}
				glEnd();
			}
			glPopMatrix();
		}
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	} else
	{
		// Get the forshortened distance between the locator and the camera

		double scaleFactor = this->getForeshortenScale(view, center) * this->getHandleSize() * 4.5f; 

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{ 
			// Translate to position
			glTranslated(center.x, center.y, center.z);
			// Push a transform matrix depending on this->plane field
			switch(this->getGlobalTriadPlane())
			{
			case(this->kXYPlane):
				// There is no transform
				break;
			case(this->kXZPlane):
				// Rotate by 90 degs around X axis
				glRotated(90, 1, 0, 0);
				break;
			case(this->kYZPlane):
				// Rotate by 90 degs around Y axis
				glRotated(90, 0, 1, 0);
				break;
			default:
				break;
			}
			glScaled(scaleFactor, scaleFactor, scaleFactor);

			// Draw a square for now centered at center
			// NOTE: Use only glBegin(GL_POLYGON) and glPolygonMode() so polygonoffset will work properly...
			if(isSelect)
			{
				glBegin(GL_POLYGON);
			} else
			{
				glBegin(GL_LINE_LOOP);
			}
			{
				glVertex2f(-1, -1);
				glVertex2f(-1, +1);
				glVertex2f(+1, +1);
				glVertex2f(+1, -1);
			}
			glEnd();
		}
		glPopMatrix();
	}
}

/**
 * Render the axes for rendering and selecting
 */
void FreeTriadPointManip::renderAxis(M3dView& view, int which)
{
	// If the component is not enabled, skip...
	if(!this->isComponentEnabled(which))
	{
		return;
	}

	MStatus stat;

	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->pointData.asMObject());
	numericFn.getData(center.x, center.y, center.z);

	// Calculate the direction vector
	MVector dir(0, 0, 0);
	dir[which] = 1.0;
	MPoint rayStart;
	MVector rayDirection;
	view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection);
	double mag = rayDirection * dir;
	// MGlobal::displayInfo(MString("FreeTriadPointManip::renderAxis: Mag for axis ") + which + " = " + mag);
	// If the axis is facing towrads the camera, don't draw the arrow
	if(mag*mag > 0.9)
	{
		// Arrow is facing towards camera
		// Don't draw...
		return;
	}

	// The scale factor
	double scaleFactor = this->getScale(view, center, &stat); MAssertStatNoReturn(stat);
	// Initialize the direction vector
	dir[which] = scaleFactor;

	// Set matrix mode to model
	glMatrixMode(GL_MODELVIEW);

	if(this->drawAxes)
	{
		glPushMatrix();
		{ 
			glTranslatef(center.x, center.y, center.z);
			// NOTE: change this to a gluCylinder so glOffset works...
			glBegin(GL_LINES);
			{  
				glVertex3d(0, 0, 0);
				glVertex3d(dir.x, dir.y, dir.z);
			}
			glEnd();
		}
		glPopMatrix();
	}

	dir = dir + center;
	if(this->drawArrowHead)
	{
		scaleFactor *= this->getHandleSize();

		// Check if renderList has been initialize or not
		if(FreeTriadPointManip::renderList == (GLuint)-1)
		{
			// Setup the renderlist to speed up the rendering
			FreeTriadPointManip::renderList = glGenLists(1);
			GLUquadricObj* qObj = gluNewQuadric();  
			gluQuadricDrawStyle(qObj, GLU_FILL);
			gluQuadricNormals(qObj, GLU_FLAT);
			glNewList(FreeTriadPointManip::renderList, GL_COMPILE);
			{
				gluCylinder(qObj, 1.0/2.0, 0, 2.0, 16, 10);
				glRotatef(180, 0, 1, 0); 
				gluDisk(qObj, 0.0, 1.0/2.0, 16, 1);
			}
			glEndList();
		}

		glPushMatrix();
		{  
			glTranslatef(dir.x, dir.y, dir.z); 

			static GLfloat remap[3][3] = {
				{0.0f, 1.0f, 0.0f},
				{-1.0f, 0.0f, 0.0f},
				{0.0f, 0.0f, 1.0f}
			};
			// Transform the cylinder the cylinder
			glRotatef(90, remap[which][0], remap[which][1], remap[which][2]); 
			// Scale
			glScalef(scaleFactor, scaleFactor, scaleFactor);
			// Call the list for the cylinder
			glCallList(this->renderList);
		}
		glPopMatrix();
	}
}

double FreeTriadPointManip::getScale(M3dView& view, MVector& center, MStatus* stat)
{
	return(this->getForeshortenScale(view, center, stat) * this->globalSize() * 3);
}
double FreeTriadPointManip::getHandleSize()
{
	return((this->handleSize()-4) / 180);
}

MStatus FreeTriadPointManip::connectToPointPlug(MPlug& plug)
{
	MStatus stat;

	MObject attr = plug.attribute(&stat); MAssertStat(stat);

	// Just check that the attribute is a DoubleLinear or FloatLinear attribute
	MAssert(attr.apiType() == MFn::kAttribute3Double);   

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug, 
		(manipToPlugConversionCallback)&CustomManipContainer::k3DoubleMToPConversionCallback,
		this->pointIndex());
	this->getParent()->addPlugToManipConversionCallback(this->pointIndex(), 
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot);
	return(stat);
}

// Setter and getter methods
void FreeTriadPointManip::setGlobalTriadPlane(ManipPlane plane) { this->plane = plane; }
void FreeTriadPointManip::setDrawAxes(bool state) { this->drawAxes = state; }
void FreeTriadPointManip::setDrawArrowHead(bool state) { this->drawArrowHead = state; }

FreeTriadPointManip::ManipPlane FreeTriadPointManip::getGlobalTriadPlane(void) const { return(this->plane); }
bool FreeTriadPointManip::isDrawAxesOn(void) const { return(this->drawAxes); }
bool FreeTriadPointManip::isDrawArrowHeadOn(void) const { return(this->drawArrowHead); }

MStatus FreeTriadPointManip::calcPrevPos(const CustomEvent& event, MVector& pos)
{
	MStatus stat;

	// Get the active view
	M3dView view = M3dView::active3dView(&stat); MAssertStat(stat);

	// Find the ray from the coord
	MPoint temp;
	MVector rayDirection;
	stat = view.viewToWorld(event.x, event.y, temp, rayDirection); MAssertStat(stat);
	MVector rayStart = temp;

	// Recalculate the previous position, this->prevPos
	switch(this->getComponent())
	{
	case(FreeTriadPointManip::kXAxis):  
		stat = this->calcClosestPtToAxis(rayStart, rayDirection, pos, MVector(1, 0, 0), this->prevPos); MAssertStat(stat);
		break;
	case(FreeTriadPointManip::kYAxis):
		stat = this->calcClosestPtToAxis(rayStart, rayDirection, pos, MVector(0, 1, 0), this->prevPos); MAssertStat(stat);
		break;
	case(FreeTriadPointManip::kZAxis):
		stat = this->calcClosestPtToAxis(rayStart, rayDirection, pos, MVector(0, 0, 1), this->prevPos); MAssertStat(stat);
		break;
	case(FreeTriadPointManip::kPlane):
		{    
			// 1) Calculate the plane normal
			MVector normal;  
			switch(this->plane)
			{
			case(FreeTriadPointManip::kXYPlane):
				normal = MVector(0, 0, 1);  
				break;
			case(FreeTriadPointManip::kXZPlane):
				normal = MVector(0, 1, 0);  
				break;
			case(FreeTriadPointManip::kYZPlane):
				normal = MVector(1, 0, 0);  
				break;
			case(FreeTriadPointManip::kViewPlane):
				{ 
					MPoint temp;
					stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal); MAssertStat(stat);
					normal *= -1;
				} 
				break;
			default:
				// Simulate a release event
				this->onRelease(event);
				// stat = MS::kFailure;
				MCheckStat(MS::kFailure, "FreeTriadPointManip::onPress: abnormal error!! invalid value for this->plane.");  
				break;
			}

			// MTrace(MString("FreeTriadPointManip::onPress: plane normal = ") + normal.length());
			// MTrace(MString("pos = ") + Format::toString(pos) + ", normal = " + Format::toString(normal));
			// MTrace(MString("RayStart = ") + Format::toString(rayStart) + ", rayDirection = " + Format::toString(rayDirection));
			stat = this->calcPlaneLineIntersection(rayStart, rayDirection, pos, normal, this->prevPos);
			// Check if we succeed, else fail...
			if(stat != MS::kSuccess)
			{
				// if so, ray is parallel to plane and it shouldn't be selected.
				// Simulate a release event
				this->onRelease(event);
				MTrace("FreeTriadPointManip::onPress: ray is parallel to plane. No selection allowed.");  
			}
		} 
		break;
	default:
		break;
	}

	return(stat);
}

// Update functions
void FreeTriadPointManip::onPress(const CustomEvent& event)
{
	MStatus stat;

	CustomManip3D::onPress(event);

	// Get the position
	MVector pos;
	MFnNumericData numericFn(this->pointData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat); 

	// NOTE: Change this so the current component (axis) gets highlighted but the plane is actually active...
	// Check if the ctrl modifier is enabled.
	if(event.getModifier() & CustomEvent::kCtrlKey)
	{ 
		// if so, check which component is selected. 
		switch(this->getComponent())
		{  
		// if kXAxis, set kPlane as selected and set manipPlane to kYZPlane
		case(this->kXAxis):
			this->setComponent(this->kPlane);
			this->setGlobalTriadPlane(this->kYZPlane);
			break;
			// if kYAxis, set kPlane as selected and set manipPlane to kXZPlane
		case(this->kYAxis):
			this->setComponent(this->kPlane);
			this->setGlobalTriadPlane(this->kXZPlane);
			break;
		// if kZAxis, set kPlane as selected and set manipPlane to kXYPlane
		case(this->kZAxis):
			this->setComponent(this->kPlane);
			this->setGlobalTriadPlane(this->kXYPlane);
			break;
		// if kPlane, set manipPlane to kViewPlane
		case(this->kPlane):
			this->setGlobalTriadPlane(this->kViewPlane);
			break;
		default:
			MAssertNoReturn(false);
			break;
		}
	}
	// calculate the intersection point
	stat = this->calcPrevPos(event, pos); MAssertStatNoReturn(stat);

	// MGlobal::displayInfo(MString("FreeTriadPointManip::onPress: previous position = ") + Format::toString(this->prevPos));
}
void FreeTriadPointManip::onDrag(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onDrag(event);

	// Save the previous position
	MVector delta = this->prevPos;

	// Get the position
	MVector pos;
	MFnNumericData numericFn(this->pointData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat);

	// Recalculate the previous position
	stat = this->calcPrevPos(event, pos); MAssertStatNoReturn(stat);

	// Calculate the delta
	delta = this->prevPos - delta; // delta = prevPos - delta
	// Update the position of the manip
	pos += delta; // pos = pos + delta

	// Set the new position data
	stat = numericFn.setData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat);
}
void FreeTriadPointManip::onRelease(const CustomEvent& event)
{
	CustomManip3D::onRelease(event);
}


