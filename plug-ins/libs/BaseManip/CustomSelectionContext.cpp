/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "CustomSelectionContext.h"
#include "CustomSelectToolCmd.h"
#include "CustomManipContainer.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <maya/MSelectionList.h>

#include <Log/MayaLog.h>

CustomSelectionContext::CustomSelectionContext() : MPxSelectionContext()
{
	this->active = false; 

	MAssertStatNoReturn( this->setTitleString ( "Custom Selection Tool" ));

	// Tell the context which XPM to use so the tool can properly
	// be a candidate for the 6th position on the mini-bar.
	MAssertStatNoReturn( this->setImage("marqueeTool.xpm", MPxContext::kImage1 ));
}

/** Sets up the tool.
 * <b>NOTE: If this method is overriden, this must be called BEFORE the rest of the tool setup. 
 * This is to insure that the context active flag is set correctly.</b>
 */
void CustomSelectionContext::toolOnSetup ( MEvent & )
{
	MAssertStatNoReturn(this->setHelpString( CUSTOM_CONTEXT_HELP_STRING ));
	this->active = true;
}

/** Cleans up the context
 * <b>NOTE: If this method is overriden, this must be called AFTER the rest of the context clean up. 
 * This is to insure that the context active flag is set correctly.</b>
 */
void CustomSelectionContext::toolOffCleanup()
{
	this->active = false;
}

/**
 * Indicates whether the context is active or not.
 * This is a better way to tell if this context is active or not then by checking `currentCtx`
 */
bool CustomSelectionContext::isActiveContext()
{
	return(this->active);
}

MStatus CustomSelectionContext::doPress( MEvent & event )
{
	// NOTE: This needs to discover if the pointer is in the same view as the current manipulator.
	// If not, it needs to move the manipulator to the current view and reinitialize it...

	// MTrace("CustomSelectionContext::doPress: calling...");
	// NOTE: If we're in a different modelPanel then the one the manip was created in, we gotta move the manip to the current one
	// and run manipContainer::onPress()

	// Check if CustomManipContainer is being selected.
	// If so, return so the selection doesn't change while a manipulator is being moved.
	if(CustomManipContainer::isActiveManip())
	{
		// MTrace("CustomSelectionContext::doPress: Manip in use, not selecting.");
		return MS::kFailure;
	} 

	// Let the MPxSelectionContext handle the doPress event
	MStatus stat = MPxSelectionContext::doPress(event); MAssertStat(stat);
	// Run the inheriting customPress event handler
	stat = this->doCustomPress(event); MAssertStat(stat);
	return(MS::kSuccess);
}

MStatus CustomSelectionContext::doHold(MEvent& event)
{
	// MTrace("CustomSelectionContext::doDrag: calling...");

	// Check if CustomManipContainer is being selected.
	// If so, return so the selection doesn't change while a manipulator is being moved.
	if(CustomManipContainer::isActiveManip())
	{
		// MTrace("CustomSelectionContext::doHold: Manip in use, not selecting.");
		return MS::kFailure;
	} 

	MStatus stat = MPxSelectionContext::doHold(event); MAssertStat(stat);
	stat = this->doCustomHold(event); MAssertStat(stat);
	return(MS::kSuccess);
}

MStatus CustomSelectionContext::doDrag( MEvent & event )
{
	// MTrace("CustomSelectionContext::doDrag: calling...");

	// Check if CustomManipContainer is being selected.
	// If so, return so the selection doesn't change while a manipulator is being moved.
	if(CustomManipContainer::isActiveManip())
	{
		// MTrace("CustomSelectionContext::doDrag: Manip in use, not selecting.");
		return MS::kFailure;
	} 

	MStatus stat = MPxSelectionContext::doDrag(event); MAssertStat(stat);
	stat = this->doCustomDrag(event); MAssertStat(stat);
	return(MS::kSuccess);
}

MStatus CustomSelectionContext::doRelease( MEvent & event )
{
	// MTrace("CustomSelectionContext::doRelease: calling...");
	
	// Check if CustomManipContainer is being selected.
	// If so, return so the selection doesn't change while a manipulator is being moved.
	if(CustomManipContainer::isActiveManip())
	{
		// MTrace("CustomSelectionContext::doRelease: Manip in use, not selecting.");
		return MS::kFailure;
	} 

	MStatus stat = MPxSelectionContext::doRelease(event); MAssertStat(stat);
	stat = this->doCustomRelease(event); MAssertStat(stat);
	return(MS::kSuccess);
}

MStatus CustomSelectionContext::doEnterRegion(MEvent& event)
{
	MAssertStat(this->setHelpString( CUSTOM_CONTEXT_HELP_STRING ));

	return(MS::kSuccess);
}
