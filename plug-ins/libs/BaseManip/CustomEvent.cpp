/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Define the CustomEvent methods

#include "CustomEvent.h"

CustomEvent::CustomEvent(int x, int y, MouseButton b, KeyModifier m, EventType t)
{
    this->x = x;
    this->y = y;
	this->button = this->kNoButton; this->setButton(b);
	this->modifier = this->kNoModifier; this->setModifier(m);
	this->type = this->kInvalidEventType; this->setEventType(t);
}
CustomEvent::CustomEvent(const CustomEvent& that)
{
	// Use the copy operator to move the info.
	*this = that;
}

void CustomEvent::setButton(const CustomEvent::MouseButton b)
{
	// Validate that only the bottom 3 bits are set...
	if((unsigned(b) & 0x7) == unsigned(b))
	{ 
		this->button = b;
	}
}
void CustomEvent::setPosition(const int x, const int y)
{
    this->x = x;
    this->y = y;
}
void CustomEvent::setModifier(const KeyModifier m)
{
   	// Validate that only the bottom 3 bits are set...
	if((unsigned(m) & 0x7) == unsigned(m))
	{ 
		this->modifier = m;
	}
}
void CustomEvent::setEventType(const EventType t)
{
	// validate
	if(t <= this->kMouseMoved)
	{ 
		this->type = t;
	}
}

int& CustomEvent::operator [](const int idx)
{	
    // ignore bounds checking because I can't throw an exception anyhow...
    if(idx == 0)
    {
	return(this->x);
    }
    else
    {
	return(this->y);
    }
}

CustomEvent& CustomEvent::operator =(const CustomEvent& that)
{
	this->x = that.x;
	this->y = that.y;
	this->button = that.button;
	this->modifier = that.modifier;
	this->type = that.type;

	return(*this);
}
