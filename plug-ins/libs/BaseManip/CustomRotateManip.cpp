/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "CustomRotateManip.h"
#include "CustomManipContainer.h"
#include "CustomEvent.h"

#include <GL/glu.h>
#include <math.h>

#include <maya/MQuaternion.h>
#include <maya/MEulerRotation.h>
#include <maya/MPoint.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MColorArray.h>
#include <maya/M3dView.h>

#include <Format/Format.h>
#include <Log/MayaLog.h>

// Define PI if some compilers (fucking VC) don't define it.
#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

// Initialize the renderList so it is initialized the first time inside the draw method 
GLuint CustomRotateManip::renderList = (GLuint)-1;
const MVector CustomRotateManip::normals[] = {
	MVector::zAxis,
	MVector::yAxis,
	MVector::xAxis
};

const ManipColor CustomRotateManip::manipColors[] = 
{
	{"Zaxis", false},
	{"Yaxis", false},
	{"Xaxis", false},
	{"manipHandle", true},
	{"manipLine", true} 
};


CustomRotateManip* CustomRotateManip::create(CustomManipContainer* parent, MStatus *stat)
{
	// make sure the pointer is ok.
	MValidateStatPtr(stat);

	// Create manip
	CustomRotateManip* manip = new CustomRotateManip(parent, stat); MAssertStatReturnObj(*stat, NULL);
	MAssertReturnObj(manip != NULL, NULL);

	return(manip);
}

CustomRotateManip::CustomRotateManip(CustomManipContainer* parent, MStatus *stat) : CustomManip3D(parent, CustomRotateManip::kAll, stat)
{
	MValidateStatPtr(stat);

	// Create a numeric data functor
	MFnNumericData numericFn;
	// Create the rotationCenter data object
	this->rotationPivotData = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	// Set the data to default
	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the rotationCenter Slot
	this->rotationPivotSlot = this->getParent()->registerManip(&this->rotationPivotData);
	// MTrace(MString("CustomRotateManip::create: center slot = ") + manip->rotationCenterSlot);

	// Create the rotation Data object
	this->rotationData = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	// Set the data to default
	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the slot with the parent
	this->rotationSlot = this->getParent()->registerManip(&this->rotationData);
};


/**
 * Draws the rotation manipulator which looks and acts like Maya's
 * TODO: Implement visual rotation feedback like Maya 
 * TODO: In windows the stencil blocking of the rear axis loops doesn't seem to work properly...
 * Has to be debugged.
 */
void CustomRotateManip::draw(M3dView& view)
{
	MStatus stat;

	this->setupDrawingData();

	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->rotationPivotData.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(center.x, center.y, center.z); MAssertStatNoReturn(stat);
	// MTrace(MString("CustomRotateManip::draw: Center = ") + Format::toString(center));

	// The scale factor
	double scaleFactor = this->getScale(view, center, &stat); MAssertStatNoReturn(stat);

	// Get the color indices
	MColorArray colors;
	for(int i=0; i<this->kAll; i++)
	{
		MColor c;
		// Highlight the component if it is selected
		if(this->getComponent() == i)
		{
			c = this->queryColorIndex(this->queryDisplayColor("manipCurrent", true));
		} else
		{  
			c = this->queryColorIndex(this->queryDisplayColor(this->manipColors[i].name, this->manipColors[i].active));  
		}
		stat = colors.append(c); MAssertStatNoReturn(stat);
	}

	// Calculate the rotation to the viewer.
	MPoint temp;
	MVector normal;
	view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal);
	MQuaternion rotQuat(MVector::zNegAxis, normal);   
	float rotMat[4][4];
	stat = rotQuat.asMatrix().get(rotMat); MAssertStatNoReturn(stat);

	// Always work in matrix mode
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	{
		glTranslated(center.x, center.y, center.z);   
		glScaled(scaleFactor, scaleFactor, scaleFactor);   

		// Setup a multipass rendering with stencil buffer so we can draw the axis loops being blocked by the screen
		// NOTE: We need the Enable bit because POLYGON bit doesn't restore the bits properly.
		glPushAttrib(GL_ENABLE_BIT);
		{  
			// Enable depth testing
			glEnable(GL_DEPTH_TEST);
			// Set the depth range so the axis loops are draw behind all of the other geometry
			// No stencil buffering is needed... (Doesn't work on windows anyway
			glDepthRange(0.98, 1);

			// Turn off the color buffers
			glColorMask(false, false, false, false);   
				
			// Draw the blocker
			glPushMatrix();
			{        
				glMultMatrixf((GLfloat*)rotMat);                                        
				// Draw the blocker
				glCallList(this->renderList+1);
			}
			glPopMatrix();
			
			// Turn on drawing and draw the loops
			glColorMask(true, true, true, true);
			glPushMatrix();
			{   
				for(int i=this->kXYPlane; i<=this->kYZPlane; i++)
				{   
					glColor3f(colors[i].r, colors[i].g, colors[i].b);   
					glRotatef(90, this->normals[i].x, this->normals[i].y, this->normals[i].z);   
					// Draw if the component is enabled
					if(this->isComponentEnabled(i))
					{
						// Don't draw if the loop axis is ~90deg from the view normal
						double mag = normal * this->normals[i];
						if(mag*mag >= 0.1)
						{
							glCallList(this->renderList);
						}
					}
				}
			}
			glPopMatrix();
			// Restore the depth range to normal
			glDepthRange(0, 1);
		}
		glPopAttrib();

		// Draw the other things
		glPushMatrix();
		{        
			glMultMatrixf((GLfloat*)rotMat);                      

			// Always draw the arcball loop
			// if(this->isComponentEnabled(this->kArcball))
			{   
				glColor3f(colors[this->kArcball].r, colors[this->kArcball].g, colors[this->kArcball].b);   
				glCallList(this->renderList);      
			}
   
			// Draw the viewLoop
			glScaled(1.1, 1.1, 1.1);
			if(this->isComponentEnabled(this->kViewPlane))
			{   
				glColor3f(colors[this->kViewPlane].r, colors[this->kViewPlane].g, colors[this->kViewPlane].b);   
				glCallList(this->renderList);
			}
		}
		glPopMatrix();  

		// Draw the feedback indicators
		for(int i=this->kYZPlane; i<=this->kYZPlane; i++)
		{
			/*if(this->getComponent() == 	i)
			 {		
			 // Draw the feedback indicator
			 glBegin(GL_LINES)
			 {
				 //glVertex2d(0, 0);
				 //glVertex2d(this->startVec.x, this->startVec.y);								
			 }
			 glEnd();
			 } 
		 */  
		}
	}
	glPopMatrix(); 
}

void CustomRotateManip::select(M3dView& view)
{
	MStatus stat; 

	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->rotationPivotData.asMObject());
	numericFn.getData(center.x, center.y, center.z);
	// MTrace(MString("CustomRotateManip::select: Center = ") + Format::toString(center));

	// The scale factor
	double scaleFactor = this->getScale(view, center, &stat); MAssertStatNoReturn(stat);

	// Calculate the rotation to the viewer.
	MPoint temp;
	MVector normal;
	view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal);
	MQuaternion rotQuat(MVector::zNegAxis, normal);   
	float rotMat[4][4];
	stat = rotQuat.asMatrix().get(rotMat); MAssertStatNoReturn(stat);

	// Get the projection matrix as a float
	GLfloat mat[4][4];
	stat = this->getProjectionMatrix(view, mat); MAssertStatNoReturn(stat); 

	// Setup the correct projection matrix
	glMultMatrixf((GLfloat*)mat);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{
		glTranslated(center.x, center.y, center.z);
		glScaled(scaleFactor, scaleFactor, scaleFactor);      

		glPushMatrix();
		{   
			// Select the loops
			for(int i=this->kXYPlane; i<=this->kYZPlane; i++)
			{
				glLoadName(i);
				glRotatef(90, this->normals[i].x, this->normals[i].y, this->normals[i].z);
				// Draw if the component is enabled
				if(this->isComponentEnabled(i))
				{
					// Don't draw if the loop axis is ~90deg from the view normal
					double mag = normal * this->normals[i];
					if(mag*mag >= 0.1)
					{
						glCallList(this->renderList);
					}
				}
			}   
		}
		glPopMatrix();

		glPushMatrix();
		{
			// Draw the object aligned towards the viewer
			glMultMatrixf((GLfloat*)rotMat);

			// Render the arcball
			glLoadName(this->kArcball);
			if(this->isComponentEnabled(this->kArcball))
			{ 
				glCallList(this->renderList+1);
			}

			// Render the view loop
			glScaled(1.1, 1.1, 1.1);
			glLoadName(this->kViewPlane);
			if(this->isComponentEnabled(this->kViewPlane))
			{ 
				glCallList(this->renderList);
			}
		}
		glPopMatrix();
	}
	glPopMatrix();   
}   

MStatus CustomRotateManip::connectToRotationPlug(MPlug& plug)
{
	MStatus stat;

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	MAssert(attr.apiType() == MFn::kAttribute3Double);

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug,
		(manipToPlugConversionCallback)&CustomManipContainer::k3DoubleMToPConversionCallback,
		this->rotationIndex());
	this->getParent()->addPlugToManipConversionCallback(this->rotationIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot); 
	return(stat);
}
MStatus CustomRotateManip::connectToRotationPivotPlug(MPlug& plug)
{
	MStatus stat;

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	MAssert(attr.apiType() == MFn::kAttribute3Double);

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->getPlugSlot(plug);

	this->getParent()->addPlugToManipConversionCallback(this->rotationPivotIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot); 
	return(stat);
}

double CustomRotateManip::getScale(M3dView& view, MVector& center, MStatus* stat)
{
	return(this->getForeshortenScale(view, center, stat) * this->globalSize() * 6);
}

MStatus CustomRotateManip::calcHalfSphericalCoords(const CustomEvent& event, MVector& rotVec)
{
	MStatus stat;

	// Get the center value
	MVector center;
	MFnNumericData numerData(this->rotationPivotData.asMObject(), &stat); MAssertStat(stat);
	stat = numerData.getData(center.x, center.y, center.z); MAssertStat(stat);

	// Get the active view
	M3dView view = M3dView::active3dView(&stat); MAssertStat(stat);

	// Get the view normal.
	MPoint temp;
	MVector normal;
	stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal); MAssertStat(stat);
	// Flip the normal so the plane faces the user
	normal *= -1;

	// Calculate the mouse ray
	MVector mouseRay;
	stat = view.viewToWorld(event.x, event.y, temp, mouseRay); MAssertStat(stat);
	MVector mouseStart = temp;

	// Get the mouse in world space as the point where the mouse ray intersects the viewplane centered at center
	MVector mouse;
	stat = this->calcPlaneLineIntersection(mouseStart, mouseRay, center, normal, mouse); MAssertStat(stat);

	// get the radius
	double radius = this->getScale(view, center, &stat); MAssertStat(stat); 

	// Start calculating the hemispherical vector as by the algorithm at
	// http://www.cse.ucsc.edu/~pang/160/f98/Gems/GemsIV/arcball/BallMath.c
	rotVec = (mouse - center) / radius;

	double mag = rotVec.x*rotVec.x + rotVec.y*rotVec.y + rotVec.z*rotVec.z;
	if(mag >= 1.0)
	{
		// The |difference vector| is greater then the radius,
		// Just normalize rotVec, don't add any out-of-view-plane components
		double sqMag = 1.0/sqrt(mag);
		rotVec *= sqMag;
	} else
	{
		double orthoMag = sqrt(1.0 - mag);
		// Add the normal scaled by orthoMag to rotVec so there is an out-of-view-plane component
		// And so |rotVec| == 1.0
		rotVec += normal*orthoMag;
	}

	// MTrace(MString("Mag of mouse vector == ") + rotVec.length());

	// Constrain startVec to an axis if any component besides kArcball is selected 
	switch(this->getComponent())
	{
	case(this->kXYPlane):
	case(this->kXZPlane):
	case(this->kYZPlane):
		normal = this->normals[this->getComponent()];
		break; 
	case(this->kViewPlane):
		// Nothing, normal is already set
		break;
	case(this->kArcball):
		// Don;t do anymore calculations on rotVec
		return(stat);
		break;
	default:
		break;
	}

	// Remove the components of normal from rotVec
	double compMag = (rotVec*normal);
	// Scale the normal
	normal *= compMag;
	// Subtract the normal components from the vector
	rotVec = rotVec - normal;

	// Renormalize the vector
	stat = rotVec.normalize(); MAssertStat(stat);

	return(stat);
}

// Update functions
void CustomRotateManip::onPress(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onPress(event);  

	// Calculate the half spherical coords for the start vector
	stat = this->calcHalfSphericalCoords(event, this->currVec); MAssertStatNoReturn(stat);
	// MTrace(MString("CustomRotateManip::onPress: currVec = ") + Format::toString(this->currVec)); 
}
void CustomRotateManip::onDrag(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onDrag(event);

	// Calculate the half spherical coords for the current rotation vector
	MVector currentVec;
	stat = this->calcHalfSphericalCoords(event, currentVec); MAssertStatNoReturn(stat);

	// MTrace(MString("CustomRotateManip::onDrag: startVec = ") + Format::toString(this->startVec) + 
	// 	   ", currVec = " + Format::toString(this->currVec));

	// Get the current rotation amount
	MFnNumericData numericFn(this->rotationData.asMObject(), &stat); MAssertStatNoReturn(stat);
	MVector origRotVec;
	stat = numericFn.getData(origRotVec[0], origRotVec[1], origRotVec[2]); MAssertStatNoReturn(stat);
	// Build an eulerRotation out of the vector as XYZ rotation
	MEulerRotation origERot = origRotVec;
	// Build a quaternion from the euler rotation
	MQuaternion origRotQuat;
	origRotQuat = origERot;
	// Get the current quaternion between startVec and currVec 
	MQuaternion newRot(this->currVec, currentVec);

	// Increment the rotation by the new rotation
	origRotQuat *= newRot;

	// Convert the rotation back into a eulerRotation
	origERot = origRotQuat;

	// Set the new rotation data
	stat = numericFn.setData(origERot[0], origERot[1], origERot[2]); MAssertStatNoReturn(stat);

	// Set this->currVec to currentVec so we can track global rotation as well
	this->currVec = currentVec; 

}
void CustomRotateManip::onRelease(const CustomEvent& event)
{
	MStatus stat;

	CustomManip3D::onRelease(event);
}

void CustomRotateManip::setupDrawingData()
{
	// Set up the calllist for one loop
	if(CustomRotateManip::renderList != (GLuint)-1)
	{
		return;
	}
	// Make two lists
	CustomRotateManip::renderList = glGenLists(2);

#define NUMSEGMENTS 40
	// Setup the renderlist to draw a unit circle in the XY plane
	const double multipler = 2.0*M_PI/NUMSEGMENTS;
	// Generate a line circle
	glNewList(CustomRotateManip::renderList, GL_COMPILE);
	{
		glBegin(GL_LINE_LOOP);
		{
			for(int i=0; i<NUMSEGMENTS; i++)
			{
				double angle = multipler * i;
				glVertex2d(cos(angle), sin(angle));  
			}
		}
		glEnd();
	}
	glEndList();  

	// Generate a disk
	glNewList(CustomRotateManip::renderList+1, GL_COMPILE);
	{
		glBegin(GL_POLYGON);
		{
			for(int i=0; i<NUMSEGMENTS; i++)
			{
				double angle = multipler * i;
				glVertex2d(cos(angle), sin(angle));  
			}
		}
		glEnd();
	}
	glEndList();   
}

