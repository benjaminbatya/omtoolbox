/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/************************************
 * CustomSelectToolCmd.cpp - definitions of the methods
 ***********************************/

#include "CustomSelectToolCmd.h"

#include <Log/MayaLog.h>
#include <maya/MArgList.h>
#include <maya/MEventMessage.h>

MString CustomSelectToolCmd::cmdString("CustomSelect");

MStatus CustomSelectToolCmd::doIt(const MArgList& args)
{
    // MTrace("CustomSelectToolCmd::doIt: Started selecting...");
    MStatus stat = MS::kSuccess;

    // Parse the arguments
    MGlobal::ListAdjustment listAdjustment = (MGlobal::ListAdjustment)args.asInt(0, &stat); MAssertStat(stat);

    short start_x = args.asInt(1, &stat); MAssertStat(stat);
    short start_y = args.asInt(2, &stat); MAssertStat(stat);
    short last_x  = args.asInt(3, &stat); MAssertStat(stat);
    short last_y  = args.asInt(4, &stat); MAssertStat(stat);

    // Save the pre and post selection lists
    stat = MGlobal::getActiveSelectionList(this->preList); MAssertStat(stat);
    
    // Set the new selection list.
    // If we have a zero dimension box, just do a point pick
    //
    if( abs(start_x - last_x) < 2 && abs(start_y - last_y) < 2 )
    {
	stat = MGlobal::selectFromScreen( start_x, start_y, listAdjustment); MAssertStat(stat);
    } else
    {
	// Select all the objects or components within the marquee.
	stat = MGlobal::selectFromScreen( start_x, start_y, last_x, last_y, listAdjustment); MAssertStat(stat);
    }
    
    // Save the new selection list.
    stat = MGlobal::getActiveSelectionList(this->postList); MAssertStat(stat); 
    
    return(stat);
}

MStatus CustomSelectToolCmd::undoIt()
{
    // MTrace("CustomSelectToolCmd::undoIt: calling...");
    MStatus stat = MS::kSuccess;
    // Set the active list
    stat = MGlobal::setActiveSelectionList(this->preList); MAssertStat(stat);
    
    return(stat);
}
MStatus CustomSelectToolCmd::redoIt()
{
    // MTrace("CustomSelectToolCmd::redoIt: setting active selection list.");
    MStatus stat = MS::kSuccess;
    // Set the active list
    stat = MGlobal::setActiveSelectionList(this->postList); MAssertStat(stat);
        
    return(stat);
}

