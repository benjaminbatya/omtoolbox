/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <maya/MPoint.h>

#include "ViewportDistanceManip.h"
#include "CustomManipContainer.h"
#include "CustomEvent.h"

#include <GL/glu.h>
#include <math.h>
// Define PI if some compilers (fucking VC) don't define it.
#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#include <maya/MMatrix.h>
#include <maya/MFloatMatrix.h>

#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MCommandResult.h>

#include <Log/MayaLog.h>
#include <Format/Format.h>

ViewportDistanceManip* ViewportDistanceManip::create(CustomManipContainer* parent, MStatus *stat)
{
	MValidateStatPtr(stat);

	// Create manip
	ViewportDistanceManip* manip = new ViewportDistanceManip(parent, stat); MAssertStatReturnObj(*stat, NULL);
	MAssertReturnObj(manip != NULL, NULL);

	return(manip);
}

ViewportDistanceManip::ViewportDistanceManip(CustomManipContainer* parent, MStatus* stat) : CustomManip3D(parent, 1, stat)
{
	// Create a numeric data object for center
	MFnNumericData numericFn;
	// Store the MObject representing the numeric data into the center MManipData
	this->center = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);

	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the centerSlot manipulator
	this->centerSlot = this->getParent()->registerManip(&this->center);
	// MGlobal::displayInfo(MString("ViewportDistanceManip::create: center slot = ") + manip->centerSlot);

	this->radius = 1.0;
	this->radiusSlot = this->getParent()->registerManip(&this->radius);
	// MGlobal::displayInfo(MString("ViewportDistanceManip::create: radius slot = ") + manip->radiusSlot);

	this->scaleFactor = 1.0;
	this->drawCenter = true;
	this->label = "";
	this->labelPos = ViewportDistanceManip::kRight;
	this->influencedByInteraction = true;
};

void ViewportDistanceManip::draw(M3dView& view)
{
	// Get the current drawing color for manipHandle
	MString color = "manipHandle";
	if(this->isActive())
	{
		color = "manipCurrent";
	}

	// MGlobal::displayInfo(MString("ViewportDistanceManip::draw: drawColor index = ") + drawColor);
	// Set the current drawing color
	view.setDrawColor(this->queryDisplayColor(color, true)); 

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Clear the projection matrix
		glLoadIdentity();
		this->render(view);

		if(this->label != "")
		{
			// Draw the label
			// Get the center data
			MPoint center;
			MFnNumericData numericFn(this->center.asMObject());
			numericFn.getData(center.x, center.y, center.z);
			short x, y;
			view.worldToView(center, x, y);
			double totalScale = this->radius.asDouble()*this->getScale();

			// Get the position
			MPoint nearPt, farPt;
			switch(this->labelPos)
			{
			case(ViewportDistanceManip::kLeft):
				x -= short(totalScale);
				view.viewToWorld(x, y, nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kRight);
				break;
			case(ViewportDistanceManip::kRight):
				x += short(totalScale);
				view.viewToWorld(x, y, nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kLeft);
				break;
			case(ViewportDistanceManip::kBottom):
				y -= short(totalScale) + 12; // Correction to place text below the circle
				view.viewToWorld(x, y, nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kCenter);
				break;
			case(ViewportDistanceManip::kTop):
				y += short(totalScale);
				view.viewToWorld(x, y, nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kCenter);
				break;
			case(ViewportDistanceManip::kCenter):
				view.viewToWorld(x, y, nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kCenter);
				break;
			default:
				MAssertNoReturn(false); // Problem with program!!   
				break;
			}
		}
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); 
}

void ViewportDistanceManip::select(M3dView& view)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Assume that the identity matrix is already loaded into the projection matrix
		this->render(view);
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); 
}

#define NUMSEGMENTS 40
void ViewportDistanceManip::render(M3dView& view)
{
	// Get the center data
	MPoint center;
	MFnNumericData numericFn(this->center.asMObject());
	numericFn.getData(center.x, center.y, center.z);
	// MTrace(MString("ViewportDistanceManip::render: center = ") + Format::toString(center));


	// Get the position of the center of the manipulator in the viewport
	short x, y;
	view.worldToView(center, x, y);
	// MGlobal::displayInfo(MString("ViewportDistanceManip::draw: manip center = (") + x + ", " + y + ")");

	double totalScale = this->radius.asDouble() * this->getScale();
	// MGlobal::displayInfo(MString("ViewportDistanceManip::draw: totalScale = ") + totalScale);

	// Setup the viewing matrix
	gluOrtho2D(0.0, (GLdouble)view.portWidth(),
			   0.0, (GLdouble)view.portHeight());

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{
		glLoadIdentity(); 

		glTranslatef(x, y, 0);
		// Draw the circle
		glScaled(totalScale, totalScale, totalScale);
		// Draw a center dot, draw a better loop here...
		glBegin(GL_LINE_LOOP);
		{
			for(unsigned i=0; i<NUMSEGMENTS; i++)
			{
				double angle = double(i)/NUMSEGMENTS * 2.0 * M_PI;
				glVertex2d(cos(angle), sin(angle));
			}  
		}
		glEnd();  
	}
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix(); 
}

MStatus ViewportDistanceManip::connectToDistancePlug(MPlug& plug)
{
	MStatus stat = MS::kSuccess; 

	// Check that the attribute the plug is referring to is kDistance
	MObject attr = plug.attribute(&stat); MAssertStat(stat);

	// Just check that the attribute is a DoubleLinear or FloatLinear attribute
	MAssert(attr.apiType() == MFn::kDoubleLinearAttribute ||
			attr.apiType() == MFn::kFloatLinearAttribute);

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug, 
		(manipToPlugConversionCallback)&CustomManipContainer::doubleMToPConversionCallback,
		this->radiusIndex());
	this->getParent()->addPlugToManipConversionCallback(this->radiusIndex(), 
		(plugToManipConversionCallback)&CustomManipContainer::doublePToMConversionCallback,
		plugSlot);    
	return(stat);
}

MStatus ViewportDistanceManip::connectToCenterPlug(MPlug& plug)
{
	MStatus stat;

	// Check that the attribute the plug is referring to is kDistance
	MObject attr = plug.attribute(&stat); MAssertStat(stat);

	// Just check that the attribute is a k3DoubleAttribute
	MAssert(attr.apiType() == MFn::kAttribute3Double);

	// Don't let this manip set the plug, only get data from it
	unsigned plugSlot = this->getParent()->getPlugSlot(plug);
	// MTrace(MString("ViewportDistanceManip::connectToCenterPlug: plug slot = ") + plugSlot);

	this->getParent()->addPlugToManipConversionCallback(this->centerIndex(), 
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot);    
	return(stat);
}

// Setter and getter methods
void ViewportDistanceManip::setCenter(const MPoint& point)
{
	MFnNumericData numericData(this->center.asMObject());
	numericData.setData(point.x, point.y, point.z);
}
void ViewportDistanceManip::setDrawCenter(bool state)
{
	this->drawCenter = state;
}
void ViewportDistanceManip::setScalingFactor(double scale)
{
	this->scaleFactor = scale;
}
void ViewportDistanceManip::setLabel(MString label)
{
	this->label = label;
}
MStatus ViewportDistanceManip::setLabelPos(const ViewportDistanceManip::TextPosition& pos)
{
	if(pos >= ViewportDistanceManip::kLast)
	{
		return(MS::kInvalidParameter);
	}

	this->labelPos = pos;
	return(MS::kSuccess);
}
void ViewportDistanceManip::setInfluencedByInteraction(bool val)
{
	this->influencedByInteraction = val;
}

bool ViewportDistanceManip::isDrawCenterOn() const
{
	return(this->drawCenter);
}
double ViewportDistanceManip::scalingFactor() const
{
	return(this->scaleFactor);
}
ViewportDistanceManip::TextPosition ViewportDistanceManip::getLabelPosition() const
{
	return(this->labelPos);
}
MString ViewportDistanceManip::getLabel() const
{
	return(this->label);
}
bool ViewportDistanceManip::isInfluencedByInteraction() const
{
	return(this->influencedByInteraction);
}

// Distance for the scaling to match the screen
double ViewportDistanceManip::getScale() const
{
	double totalScale = this->scaleFactor;
	// Check if the user is supposed to be able to modify the scaling of the circle.
	if(this->influencedByInteraction)
	{
		totalScale *= this->globalSize();
	} else // Reduce the manip by the distance the user is from the point.
	{
		// Get the center data
		MVector center;
		MFnNumericData numericFn(this->center.asMObject());
		numericFn.getData(center.x, center.y, center.z);

		M3dView view = M3dView::active3dView();
		totalScale /= this->getForeshortenScale(view, center);  
	}
	return(totalScale);
}

unsigned ViewportDistanceManip::radiusIndex()
{
	return(this->radiusSlot);
}
unsigned ViewportDistanceManip::centerIndex()
{
	return(this->centerSlot);
}

// Update functions
void ViewportDistanceManip::onPress(const CustomEvent& event)
{
	CustomManip3D::onPress(event);

}
void ViewportDistanceManip::onDrag(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onDrag(event);
	// MGlobal::displayInfo("ViewportDistanceManip::onDrag: in event.");

	double scale = this->getScale();

	// Calculate the current radius
	// Get the center data
	MPoint center;
	MFnNumericData numericFn(this->center.asMObject(), &stat); MAssertStatNoReturn(stat);
	stat = numericFn.getData(center.x, center.y, center.z); MAssertStatNoReturn(stat);

	// Get the position of the center of the manipulator in the viewport
	M3dView view = M3dView::active3dView(&stat); MAssertStatNoReturn(stat);
	short x, y;
	view.worldToView(center, x, y, &stat); MAssertStatNoReturn(stat);

	short dx = event.x - x;
	short dy = event.y - y;

	// Set the current radius.
	this->radius = sqrt(dx*dx + dy*dy) / scale;

	// MGlobal::displayInfo(MString("ViewportDistanceManip::onDrag: new radius = ") + this->radius.asDouble());
}
void ViewportDistanceManip::onRelease(const CustomEvent& event)
{
	CustomManip3D::onRelease(event);
}


