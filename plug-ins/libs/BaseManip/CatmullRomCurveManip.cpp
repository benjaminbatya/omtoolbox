/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// CatmullRomCurveManip.cpp - method definitions for the CatmullRom Curve Base Manipulator

#include "CatmullRomCurveManip.h"

#include <GL/glu.h>
#include <math.h>

#include <Log/MayaLog.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MColor.h>
#include <maya/MMatrix.h>

CatmullRomCurveManip* CatmullRomCurveManip::create(CustomManipContainer* parent, MStatus* stat)
{
	MValidateStatPtr(stat);

	// Create the manip
	CatmullRomCurveManip* manip = new CatmullRomCurveManip(parent, stat); MAssertStatReturnObj(*stat, NULL); 
	MAssertReturnObj(manip != NULL, NULL);

	return(manip);
}

CatmullRomCurveManip::CatmullRomCurveManip(CustomManipContainer* parent, MStatus* stat) : CustomManip3D(parent, 4, stat)
{
	MValidateStatPtr(stat);
	// Initialize the manipulator datums
	MDoubleArray array(4);

	MFnDoubleArrayData arrayData;
	// Store a doubleArray into controlPts
	this->controlPts = arrayData.create(array, stat); MAssertStatNoReturn(*stat);

	// Get a slot for the controlPt manip value
	this->controlPtsSlot = this->getParent()->registerManip(&this->controlPts);

	// Initialize the manip attributes to default
	this->drawAxes = true;
	this->drawTangents = true;
	this->label = "";
	this->position = MVector(50, 50);
	this->size = MVector(50, 50);
}

MStatus CatmullRomCurveManip::connectToArrayPlug(MPlug& plug)
{
	MStatus stat = MS::kSuccess; 

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	// Check that the attribute type is typed
	MAssert(attr.apiType() == MFn::kTypedAttribute);

	// Check that the typedAttribute is a doubleArray
	MFnTypedAttribute typedAttr(attr, &stat); MAssertStat(stat);
	// MGlobal::displayInfo(MString("ViewportDistanceManip::connectToDistancePlug: trying to connect to attribute =") + typedAttr.attrType());
	MAssert(typedAttr.attrType() == MFnData::kDoubleArray);

	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug,
		(manipToPlugConversionCallback)&CustomManipContainer::doubleArrayMToPConversionCallback,
		this->controlPtsIndex());
	this->getParent()->addPlugToManipConversionCallback(this->controlPtsIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::doubleArrayPToMConversionCallback,
		plugSlot);

	return(stat);
}

void CatmullRomCurveManip::setDrawAxes(bool flag)
{
	this->drawAxes = flag;
}
void CatmullRomCurveManip::setDrawTangents(bool flag)
{
	this->drawTangents = flag;
}
void CatmullRomCurveManip::setLabel(const MString& label)
{
	this->label = label;
}
void CatmullRomCurveManip::setPosition(const MVector& pos)
{
	this->position = pos;
}
void CatmullRomCurveManip::setSize(const MVector& size)
{
	this->size = size;
}

bool CatmullRomCurveManip::getDrawAxes()
{
	return this->drawAxes;
}
bool CatmullRomCurveManip::getDrawTangents()
{
	return this->drawTangents;
}
MString CatmullRomCurveManip::getLabel()
{
	return this->label;
}
MVector CatmullRomCurveManip::getPosition()
{
	return this->position;
}
MVector CatmullRomCurveManip::getSize()
{
	return this->size;
}

unsigned CatmullRomCurveManip::controlPtsIndex()
{
	return(this->controlPtsSlot);
}

// This should probably use caching somehow...
inline unsigned factorial(unsigned n)
{
	unsigned total = 1;
	for(unsigned i=2; i<n; i++)
	{
		total *= i;
	}
	return(total);
}

inline double calcCatmullRomCoeff(unsigned n, unsigned i, double t)
{
	// ASSERT(n >= i);

	double minT = 1.0 - t;

	// Calculate the choose formula
	double coeff = factorial(n) / (factorial(i) * factorial(n-i));

	return(coeff * pow(minT, n-i) * pow(t, i));
}

void CatmullRomCurveManip::draw(M3dView& view)
{
	MStatus stat;

	MObject val = this->controlPts.asMObject();
	MFnDoubleArrayData array(val, &stat); MAssertStatNoReturn(stat);

	MDoubleArray newArray;
	stat = array.copyTo(newArray); MAssertStatNoReturn(stat);
	stat = newArray.insert(newArray[0], 0); MAssertStatNoReturn(stat);
	stat = newArray.append(newArray[newArray.length()-1]); MAssertStatNoReturn(stat);
	// Calculate the curve points, make this a manipulator attribute.
#define NUMSEGMENTS 40
	// Calculate the approximation to the catmullRom curve.
	// Stolen from sergio's code hehehe HAhahaha BAHAHAHAHA
	MDoubleArray dArray;

	for(int i=0; i<NUMSEGMENTS; i++)
	{
		double t = ((double)i)/(NUMSEGMENTS-1);
		int maxT = newArray.length() - 3;

		// map t to the correct span
		int spanStart;
		float spanT;

		if(t == 1.0)
		{
			spanStart = maxT - 1;
			spanT = 1.0;
		} else
		{
			float extendedT = t * maxT;
			spanStart = (int)floor(extendedT);
			spanT = extendedT - spanStart;
		}

		double p0 = newArray[spanStart];
		double p1 = newArray[spanStart + 1];
		double p2 = newArray[spanStart + 2];
		double p3 = newArray[spanStart + 3];


		double val = 0.5 * ( (2.0 * p1) +
							 ((-p0 + p2) * spanT) +
							 ((2.0*p0 - 5.0*p1 + 4.0*p2 - p3) * pow(spanT,2.0)) +
							 ((-p0 + 3.0*p1 - 3.0*p2 + p3) * pow(spanT,3.0) ));
		dArray.append(val);
	} 

#undef NUMSEGMENTS	

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Clear the projection matrix
		glLoadIdentity();

		// Set up the viewing matrix
		gluOrtho2D(0.0, (GLdouble)view.portWidth(),
				   0.0, (GLdouble)view.portHeight());

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glLoadIdentity();
			glTranslated(this->position[0], this->position[1], 0);
			glScaled(this->size[0], this->size[1], 0);

			// Draw the border
			glColor3d(0, 0, 0);
			glBegin(GL_LINE_LOOP);
			{
				glVertex2d(0, 0);
				glVertex2d(1, 0);
				glVertex2d(1, 1);
				glVertex2d(0, 1);
			}
			glEnd();

			// Draw the curve as a filled polygon
			// Later...

			// Draw the curve 
			glColor3d(1, 0, 0);
			glBegin(GL_LINE_STRIP);
			{
				for(int i=0; i<dArray.length(); i++)
				{
					double x = double(i) / (dArray.length() - 1);
					double y = dArray[i];
					glVertex2d(x, y);
				}
			}
			glEnd();

			// Draw the axes


			// Draw the tangents

			// Draw the points
			glPushAttrib(GL_POINT_BIT);
			{ 
				// Make the points pretty big so they show properly...
				glPointSize(5.0f);
				glEnable(GL_POINT_SMOOTH);

				// Get the active and manip colors
				int currentColor = this->queryDisplayColor("manipCurrent", true);
				int handleColor = this->queryDisplayColor("manipHandle", true);

				glBegin(GL_POINTS);
				{
					for(int i=0; i<array.length(); i++)
					{
						// Set the right color
						int idx = handleColor;
						if(i == this->getComponent())
						{
							idx = currentColor;
						}
						MColor color = this->queryColorIndex(idx);
						glColor3f(color.r, color.g, color.b);
						double x = double(i) / (array.length()-1);
						double y = array[i];
						glVertex2d(x, y);
					}
				}
				glEnd();
			}
			glPopAttrib();   
			// this->render(view); 

			// Draw the label
			glColor3d(0, 0, 0);
			if(this->label != "")
			{
				// Draw the label
				MPoint nearPt, farPt;
				view.viewToWorld(short(this->position[0]), short(this->position[1]+this->size[1]+4), nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kLeft);
			}
		}
		glPopMatrix();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void CatmullRomCurveManip::select(M3dView& view)
{
	MObject val = this->controlPts.asMObject();
	MFnDoubleArrayData array(val);

	// Setup the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Set up the viewing matrix
		gluOrtho2D(0.0, (GLdouble)view.portWidth(),
				   0.0, (GLdouble)view.portHeight());

		// Setup the modelview matrix
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glLoadIdentity();
			glTranslated(this->position[0], this->position[1], 0);
			glScaled(this->size[0], this->size[1], 0);   

			double size = 5.0/this->size[0];

			// Draw the dots
			for(int i=0; i<array.length(); i++)
			{
				glLoadName(i);
				glBegin(GL_QUADS);
				{
					// MGlobal::displayInfo(MString("Drawing point ") + i);
					double x = double(i) / (array.length()-1);
					double y = array[i];
					glVertex2d(x-size, y-size);
					glVertex2d(x-size, y+size);
					glVertex2d(x+size, y+size);
					glVertex2d(x+size, y-size);
				}
				glEnd();
			}
		}
		glPopMatrix();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); 
}

// Mouse event handlers
void CatmullRomCurveManip::onPress(const CustomEvent& event)
{
	CustomManip3D::onPress(event);
	// nothing...
}
void CatmullRomCurveManip::onDrag(const CustomEvent& event)
{
	CustomManip3D::onDrag(event);

	// Get the current y location.
	double val = event.y;

	// Adjust for scaling and position
	val = (val-this->position[1]) / this->size[1];

	// Set the array element
	MObject obj = this->controlPts.asMObject();
	MFnDoubleArrayData array(obj);
	array[this->getComponent()] = val;
}
void CatmullRomCurveManip::onRelease(const CustomEvent& event)
{
	CustomManip3D::onRelease(event);
}

