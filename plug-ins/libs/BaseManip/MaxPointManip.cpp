/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "MaxPointManip.h"
#include "CustomManipContainer.h"
#include "CustomEvent.h"

#include <GL/glu.h>
#include <math.h>

#include <maya/MFloatMatrix.h>
#include <maya/MPoint.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>

#include <Format/Format.h>
#include <Log/MayaLog.h>

#define NORMALDIST 40.0

// Initialize the renderList so it is initialized the first time inside the draw method
GLuint MaxPointManip::renderList = (GLuint)-1;

MaxPointManip* MaxPointManip::create(CustomManipContainer* parent, MStatus *stat)
{
	MValidateStatPtr(stat);

	// Create manip
	MaxPointManip* manip = new MaxPointManip(parent, stat); MAssertStatReturnObj(*stat, NULL); 
	MAssertReturnObj(manip!=NULL, NULL);
	return(manip);
}
MaxPointManip::MaxPointManip(CustomManipContainer* parent, MStatus* stat) : CustomManip3D(parent, MaxPointManip::kAll, stat) 
{	
	MValidateStatPtr(stat);

	// Create a numeric data object for center
	MFnNumericData numericFn;
	// Store the MObject representing the numeric data into the point MManipData
	this->point = numericFn.create(MFnNumericData::k3Double, stat); MAssertStatNoReturn(*stat);
	*stat = numericFn.setData(0.0, 0.0, 0.0); MAssertStatNoReturn(*stat);
	// Register the centerSlot manipulator
	this->pointSlot = this->getParent()->registerManip(&this->point);
	// MGlobal::displayInfo(MString("MaxPointManip::create: center slot = ") + manip->centerSlot);

	// Default is to draw everything
	this->drawPlanes = true;
	this->drawAxes = true;
	this->drawArrowHead = true;
};

void MaxPointManip::draw(M3dView& view)
{
	// Get the active color for the current manipulator component
	int activeColor = this->queryDisplayColor("manipCurrent", true); 

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{ 
		// Add lighting...
		glPushAttrib(GL_LIGHTING_BIT);
		{    
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);

			// Entries into Maya's color lookup tables
			static char* colorNames[] = {"Xaxis", "Yaxis", "Zaxis", "Xaxis", "Yaxis", "Zaxis"}; 
			// Render the axis bars
			int i;
			for(i=MaxPointManip::kXAxis; i<MaxPointManip::kYZPlane; i++)
			{
				// Set the color 
				int idx;
				if(this->getComponent() == i)
				{
					// Set the current drawing color
					//  view.setDrawColor(activeColor); 
					idx = activeColor;
				} else
				{
					// view.setDrawColor(getColor(axis[i], false));
					idx = this->queryDisplayColor(colorNames[i], false);
				}
				MColor c = this->queryColorIndex(idx);
				GLfloat color[4];
				c.get(color); color[3] = 1.0;
				// Set the material color
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
				// Render the axis   
				this->renderAxis(view, i);    
			};

			// Render the opposite plane without lighting
			glDisable(GL_LIGHTING);
			for(i=MaxPointManip::kYZPlane; i<MaxPointManip::kAll; i++)
			{
				// Set the color 
				int idx;
				if(this->getComponent() == i)
				{
					// Set the current drawing color
					//  view.setDrawColor(activeColor); 
					idx = activeColor;
				} else
				{
					idx = this->queryDisplayColor(colorNames[i], false);
				}
				MColor c = this->queryColorIndex(idx);
				GLfloat color[4];
				c.get(color); color[3] = 1.0;

				glColor3f(color[0], color[1], color[2]);
				// NOTE: Setup alpha to 50%
				this->renderPlane(view, i);
			}
		} 
		glPopAttrib();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void MaxPointManip::select(M3dView& view)
{
	GLfloat mat[4][4];
	this->getProjectionMatrix(view, mat);

	// MGlobal::displayInfo(MString("MaxPointManip::select: Projection matrix = ") + Format::toString(mat));
	// MGlobal::displayInfo(MString("Projection matrix = ") + Format::toString(mat));

	// Render the axis bars
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Push matrix
		glMultMatrixd((GLdouble*)mat);
		// Render Axes
		for(int i=MaxPointManip::kXAxis; i<MaxPointManip::kYZPlane; i++)
		{
			glLoadName(i);
			this->renderAxis(view, i);
			glLoadName(MaxPointManip::kYZPlane+i);
			this->renderPlane(view, MaxPointManip::kYZPlane+i);
		}
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();   
}

#define NUMSEGMENTS 40

void MaxPointManip::renderPlane(M3dView& view, int which)
{
	// Don't do anything if we're not supposed to draw the planes
	if(!this->drawPlanes)
	{
		return;
	}
	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->point.asMObject());
	numericFn.getData(center.x, center.y, center.z);  

	// Get the forshortened distance between the locator and the camera
	MPoint rayStart;
	MVector rayDirection;
	// World ray and position.
	view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection);
	// RayDirection is normalized already... just find the difference between rayStart and center  
	MVector temp = center - rayStart;
	// The scale factor
	double scaleFactor = (temp * rayDirection) / NORMALDIST;
	scaleFactor *= this->handleSize()/NORMALDIST * 0.5;	// 0.5 is an arbitrary amount... 

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{ 
		// Translate to position
		glTranslated(center.x, center.y, center.z);

		// Scale to size
		glScaled(scaleFactor, scaleFactor, scaleFactor);

		// Rotate depending on which plane we are drawing
		switch(which)
		{
		case(MaxPointManip::kXYPlane):
			// There is no transform
			break;
		case(MaxPointManip::kXZPlane):
			// Rotate by 90 degs around Z axis
			glRotated(90, 1, 0, 0);
			break;
		case(MaxPointManip::kYZPlane):
			// Rotate by 90 degs around -Y axis
			glRotated(-90, 0, 1, 0);
			break;
		default:
			break;
		}

		// Translate by 1 to the corner
		glTranslated(1.0, 1.0, 0.0);

		// Draw a square
		glBegin(GL_QUADS);
		{  
			// Draw is one direction
			glVertex2f(-1, -1);
			glVertex2f(-1, +1);
			glVertex2f(+1, +1);
			glVertex2f(+1, -1);
			// Draw in the other direction
			glVertex2f(-1, -1);
			glVertex2f(+1, -1);
			glVertex2f(+1, +1);
			glVertex2f(-1, +1);
		}
		glEnd();
	}
	glPopMatrix();
}

/**
 * Render the axes for rendering and selecting
 */
void MaxPointManip::renderAxis(M3dView& view, int which)
{
	// Get the center data
	MVector center;
	MFnNumericData numericFn(this->point.asMObject());
	numericFn.getData(center.x, center.y, center.z);

	MVector dir(0, 0, 0);
	dir[which] = 1.0;
	// If the axis and view Direction are almost parallel, don't draw the arrow
	MPoint rayStart;
	MVector rayDirection;
	view.viewToWorld(view.portWidth()/2, view.portHeight()/2, rayStart, rayDirection);
	// RayDirection is normalized already... just find the difference between rayStart and center
	MVector temp = center - rayStart;

	double mag = rayDirection * dir;
	// MGlobal::displayInfo(MString("MaxPointManip::renderAxis: Mag for axis ") + which + " = " + mag);
	if(mag*mag > 0.9)
	{
		// Arrow is facing towards camera
		// Don't draw...
		return;
	}

	// The scale factor
	double scaleFactor = temp * rayDirection / NORMALDIST;

	dir[which] = scaleFactor*this->globalSize()*3;

	if(this->drawAxes)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{ 
			glTranslatef(center.x, center.y, center.z);
			// NOTE: change this to a gluCylinder so glOffset works...
			glBegin(GL_LINES);
			{  
				glVertex3d(0, 0, 0);
				glVertex3d(dir.x, dir.y, dir.z);
			}
			glEnd();
		}
		glPopMatrix();
	}

	dir = dir + center;
	if(this->drawArrowHead)
	{
		// Check if renderList has been initialize or not
		if(MaxPointManip::renderList == (GLuint)-1)
		{
			// Setup the renderlist to speed up the rendering
			MaxPointManip::renderList = glGenLists(1);
			GLUquadricObj* qObj = gluNewQuadric();  
			gluQuadricDrawStyle(qObj, GLU_FILL);
			gluQuadricNormals(qObj, GLU_FLAT);
			glNewList(MaxPointManip::renderList, GL_COMPILE);
			{
				gluCylinder(qObj, 1.0/2.0, 0, 2.0, 20, 10);
				glRotatef(180, 0, 1, 0); 
				gluDisk(qObj, 0.0, 1.0/2.0, 20, 1);
			}
			glEndList();
		}

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{  
			glTranslatef(dir.x, dir.y, dir.z); 

			static GLfloat remap[3][3] = {
				{0.0f, 1.0f, 0.0f},
				{-1.0f, 0.0f, 0.0f},
				{0.0f, 0.0f, 1.0f}
			};
			// Transform the cylinder the cylinder
			glRotatef(90, remap[which][0], remap[which][1], remap[which][2]); 

			scaleFactor *= this->handleSize()/NORMALDIST;
			glScalef(scaleFactor, scaleFactor, scaleFactor);
			// Call the list for the cylinder
			glCallList(this->renderList);
		}
		glPopMatrix();
	}
}

MStatus MaxPointManip::connectToPointPlug(MPlug& plug)
{
	MStatus stat = MS::kSuccess; 

	// Check that the attribute the plug is referring to is kDistance
	// MGlobal::displayInfo(MString("plug info = ") + plug.info());
	// MGlobal::displayInfo(MString("MaxPointManip::connectToPlugPlug: trying to connect to plug =") + plug.node().apiTypeStr());
	// MGlobal::displayInfo(MString("MaxPointManip::connectToPlugPlug: trying to connect to attribute =") + plug.attribute().apiTypeStr());
	// MGlobal::displayInfo(MString("MaxPointManip::connectToPlugPlug: trying to connect to attribute number=") + plug.attribute().apiType());
	MObject attr = plug.attribute(&stat);
	MCheckStat(stat, "MaxPointManip::connectToDistancePlug: Couldn't get plug's attribute.");

	// Just check that the attribute is a DoubleLinear or FloatLinear attribute
	if(attr.apiType() != MFn::kAttribute3Double &&
	   attr.apiType() != MFn::kAttribute3Float)
	{
		MCheckStat(MS::kFailure, "MaxPointManip::connectToPointPlug: cannot connect to nonpoint plug.");
	}

	// Hook up the standard conversion callbacks
	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug, 
		(manipToPlugConversionCallback)&CustomManipContainer::k3DoubleMToPConversionCallback,
		this->pointIndex());
	this->getParent()->addPlugToManipConversionCallback(this->pointIndex(), 
		(plugToManipConversionCallback)&CustomManipContainer::k3DoublePToMConversionCallback,
		plugSlot);

	return(stat);
}

// Setter and getter methods
void MaxPointManip::setDrawAxes(bool state)
{
	this->drawAxes = state;
}
void MaxPointManip::setDrawArrowHead(bool state)
{
	this->drawArrowHead = state;
}
void MaxPointManip::setDrawPlanes(bool state)
{
	this->drawPlanes = state;
}

bool MaxPointManip::isDrawAxesOn(void) const
{
	return(this->drawAxes);
}
bool MaxPointManip::isDrawArrowHeadOn(void) const
{
	return(this->drawArrowHead);
}
bool MaxPointManip::isDrawPlanesOn(void) const
{
	return(this->drawPlanes);
}

// Index functions
unsigned MaxPointManip::pointIndex()
{
	return(this->pointSlot);
}

/**
 * Find the projected point onto the xaxis
 * Equation from http://www.magic-software.com/Documentation/DistanceLine3Line3.pdf Eberly's site
 */
MStatus calcClosestPt(const MVector& rayStart, const MVector& rayDirection, const MVector& pos, const MVector& axis, MVector& finalPt)
{
	// Calculate the variables we need
	double a = rayDirection * rayDirection;	// m0 * m0
	double b = rayDirection * axis * -1.0; // m0 * m1 * -1; 
	double c = axis * axis;	// m1 * m1
	double d = rayDirection * (rayStart - pos);	// m0 * (b0 - b1)
	double e = axis * (rayStart - pos) * -1.0;	// m1 * (b0 - b1) * -1
	// Don't need to calculate f since it disappears in the differencation

	double det = a*c - b*b;
	// Invalid should be able to happen
	MCheck(det > 0.00001, "calcClosestPt: abnormal error, det is supposed to be much greater then zero.");

	double t = (b*d - a*e)/det;

	finalPt = (axis * t) + pos;	// position + XAxis * t; 

	return(MS::kSuccess);
}

MStatus calcPlaneLineIntersection(const MVector& pos, const MVector& normal, 
								  const MVector& rayStart, const MVector& rayDirection, 
								  MVector& prevPos)
{
	// Determine the point of intersection between the line and plane
	// Solution from http://astronomy.swin.edu.au/~pbourke/geometry/planeline/
	double denom = normal * rayDirection;
	// Check if denom is really small
	if((denom*denom) < 0.00001)
	{
		return(MS::kFailure);
	}

	double numer = normal * (pos - rayStart);
	MTrace(MString("calcPlaneLineIntersection: denom = ") + denom);
	MTrace(MString("calcPlaneLineIntersection: numer = ") + numer);
	double u = numer / denom;

	prevPos = rayStart + (rayDirection * u);
	return(MS::kSuccess);
}

MStatus MaxPointManip::calcPrevPos(const MVector& pos, const CustomEvent& event)
{
	MStatus stat;

	// Get the active view
	M3dView view = M3dView::active3dView(&stat); MAssertStat(stat);

	// Find the ray from the coord
	MPoint temp;
	MVector rayDirection;
	stat = view.viewToWorld(event.x, event.y, temp, rayDirection); MAssertStat(stat);
	MVector rayStart = temp;

	// Recalculate the previous position, this->prevPos
	switch(this->getComponent())
	{
	case(MaxPointManip::kXAxis):  
		stat = calcClosestPt(rayStart, rayDirection, pos, MVector(1, 0, 0), this->prevPos); MAssertStat(stat);
		break;
	case(MaxPointManip::kYAxis):
		stat = calcClosestPt(rayStart, rayDirection, pos, MVector(0, 1, 0), this->prevPos); MAssertStat(stat);
		break;
	case(MaxPointManip::kZAxis):
		stat = calcClosestPt(rayStart, rayDirection, pos, MVector(0, 0, 1), this->prevPos); MAssertStat(stat);
		break;
	case(MaxPointManip::kXYPlane):
		stat = calcPlaneLineIntersection(pos, MVector(0, 0, 1), rayStart, rayDirection, this->prevPos); MAssertStat(stat);
		break;
	case(MaxPointManip::kXZPlane):
		stat = calcPlaneLineIntersection(pos, MVector(0, 1, 0), rayStart, rayDirection, this->prevPos); MAssertStat(stat);
		break;
	case(MaxPointManip::kYZPlane):
		stat = calcPlaneLineIntersection(pos, MVector(1, 0, 0), rayStart, rayDirection, this->prevPos); MAssertStat(stat);
		break;
	default:
		break;
	}

	return(stat);
}

// Update functions
void MaxPointManip::onPress(const CustomEvent& event)
{
	MStatus stat;

	CustomManip3D::onPress(event);
	// Get the position
	MFnNumericData numericFn(this->point.asMObject(), &stat); MAssertStatNoReturn(stat);
	MVector pos;
	stat = numericFn.getData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat); 
	// calculate the intersection point
	stat = this->calcPrevPos(pos, event); MAssertStatNoReturn(stat);

	// MGlobal::displayInfo(MString("MaxPointManip::onPress: previous position = ") + Format::toString(this->prevPos));
}
void MaxPointManip::onDrag(const CustomEvent& event)
{
	MStatus stat;
	CustomManip3D::onDrag(event);

	MFnNumericData numerData(this->point.asMObject(), &stat); MAssertStatNoReturn(stat);
	MVector pos;
	stat = numerData.getData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat);

	// Save the previous position
	MVector delta = this->prevPos;

	// Recalculate the previous position
	stat = this->calcPrevPos(pos, event); MAssertStatNoReturn(stat);

	// Calculate the delta
	delta = this->prevPos - delta; // delta = prevPos - delta
	// Update the position of the manip
	pos += delta; // pos = pos + delta

	// Set the new position data
	stat = numerData.setData(pos.x, pos.y, pos.z); MAssertStatNoReturn(stat);
}
void MaxPointManip::onRelease(const CustomEvent& event)
{
	CustomManip3D::onRelease(event);
}



