/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// CustomManipContainerCallbacks.cpp - definitions for the standard callbacks for the different custom base manipulators

#include "CustomManipContainer.h"

#include <maya/MGlobal.h>
#include <maya/MManipData.h>
#include <maya/MMatrix.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnDoubleArrayData.h>

#include <Log/MayaLog.h>

// generic double PtoM and MtoP conversion callbacks
MManipData CustomManipContainer::doublePToMConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;
    
	MAssert(index < this->manipSlots.size());
    // Look up the current value in the plug
    unsigned plugSlot = this->manipSlots[index]->associatedPlugSlot;
    // Get the plug
    double value;
    MStatus stat = this->getConvertorPlugValue(plugSlot, value); MAssertStatReturnObj(stat, 0);
    return(value);
}
MManipData CustomManipContainer::doubleMToPConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;

    MAssert(index < this->plugSlots.size());
    unsigned manipSlot = this->plugSlots[index]->associatedManipSlot;
    double value;
    MStatus stat = this->getConvertorManipValue(manipSlot, value); MAssertStatReturnObj(stat, 0);
    return(value);
}

// generic MPoint PtoM and MtoP conversion callbacks
MManipData CustomManipContainer::k3DoublePToMConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;

    MAssert(index < this->manipSlots.size());
    // Look up the current value in the plug
    unsigned plugSlot = this->manipSlots[index]->associatedPlugSlot;
    // Get the plug
    MVector value;
    MStatus stat = this->getConvertorPlugValue(plugSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a NumericData Object
    MFnNumericData numerData;
    MObject obj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);
    stat = numerData.setData(value[0], value[1], value[2]); MAssertStatReturnObj(stat, MObject::kNullObj);
    return(obj);
}
MManipData CustomManipContainer::k3DoubleMToPConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;

    MAssert(index < this->plugSlots.size());
    unsigned manipSlot = this->plugSlots[index]->associatedManipSlot;
    MVector value;
    MStatus stat = this->getConvertorManipValue(manipSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a NumericData Object
    MFnNumericData numerData;
    MObject obj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);
    stat = numerData.setData(value[0], value[1], value[2]); MAssertStatReturnObj(stat, MObject::kNullObj);
    return(obj);
}

// generic MDoubleArray PtoM and MtoP conversion callbacks
MManipData CustomManipContainer::doubleArrayPToMConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;

    MAssert(index < this->manipSlots.size());
    // Look up the current value in the plug
    unsigned plugSlot = this->manipSlots[index]->associatedPlugSlot;
    // Get the plug
    MDoubleArray value;
    MStatus stat = this->getConvertorPlugValue(plugSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a DoubleArray Object
    MFnDoubleArrayData arrayData;
    MObject obj = arrayData.create(value, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);
    return(obj);
}
MManipData CustomManipContainer::doubleArrayMToPConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;
	
	MAssert(index < this->plugSlots.size());
    unsigned manipSlot = this->plugSlots[index]->associatedManipSlot;
    MDoubleArray value;
    MStatus stat = this->getConvertorManipValue(manipSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a DoubleArray Object
    MFnDoubleArrayData arrayData;
    MObject obj = arrayData.create(value, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);
    return(obj);
}

MManipData CustomManipContainer::matrixPToMConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;

    MAssert(index < this->manipSlots.size());
    unsigned plugSlot = this->manipSlots[index]->associatedPlugSlot;
    MMatrix value;
    MStatus stat = this->getConvertorPlugValue(plugSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a matrix object
    MFnMatrixData matrixData;
    MObject obj = matrixData.create(value, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);

    return(obj);				     
}
MManipData CustomManipContainer::matrixMToPConversionCallback(unsigned index)
{
	MAssert(index >= SLOT_OFFSET);
	// Undo the offset amount
	index -= SLOT_OFFSET;
	
	MAssert(index < this->plugSlots.size());
    unsigned manipSlot = this->plugSlots[index]->associatedManipSlot;
    MMatrix value;
    MStatus stat = this->getConvertorManipValue(manipSlot, value); MAssertStatReturnObj(stat, MObject::kNullObj);
    // Convert the value into a matrix object
    MFnMatrixData matrixData;
    MObject obj = matrixData.create(value, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);

    return(obj);				     
}
