/**
BaseManip - Library used to construct custom child manipulators in Maya.
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// BezierCurveManip.cpp - method definitions for the Bezier Curve Base Manipulator

#include "BezierCurveManip.h"

#include <GL/glu.h>
#include <math.h>

#include <Log/MayaLog.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MColor.h>

BezierCurveManip* BezierCurveManip::create(CustomManipContainer* parent, MStatus* stat)
{
    MValidateStatPtr(stat);
	// Create the manip
	BezierCurveManip* manip = new BezierCurveManip(parent, stat); MAssertStatReturnObj(*stat, NULL);
	MAssertReturnObj(manip!=NULL, NULL);

	return(manip);
}

BezierCurveManip::BezierCurveManip(CustomManipContainer* parent, MStatus* stat) : CustomManip3D(parent, 4, stat)
{
	MValidateStatPtr(stat);
	// Initialize the manipulator datums
	MDoubleArray array(4);

	MFnDoubleArrayData arrayData;
	// Store a doubleArray into controlPts
	this->controlPts = arrayData.create(array, stat); MAssertStatNoReturn(*stat);
	// Get a slot for the controlPt manip value
	this->controlPtsSlot = this->getParent()->registerManip(&this->controlPts);

	// Initialize the manip attributes to default
	this->drawAxes = true;
	this->drawTangents = true;
	this->label = "";
	this->position = MVector(50, 50);
	this->size = MVector(50, 50);
}

MStatus BezierCurveManip::connectToArrayPlug(MPlug& plug)
{
	MStatus stat = MS::kSuccess; 

	MObject attr = plug.attribute(&stat); MAssertStat(stat);
	// Check that the attribute type is typed
	MAssert(attr.apiType() == MFn::kTypedAttribute);

	// Check that the typedAttribute is a doubleArray
	MFnTypedAttribute typedAttr(attr, &stat);
	// MGlobal::displayInfo(MString("ViewportDistanceManip::connectToDistancePlug: trying to connect to attribute =") + typedAttr.attrType());
	MAssert(typedAttr.attrType() == MFnData::kDoubleArray);

	unsigned plugSlot = this->getParent()->addManipToPlugConversionCallback(plug,
		(manipToPlugConversionCallback)&CustomManipContainer::doubleArrayMToPConversionCallback,
		this->controlPtsIndex());
	this->getParent()->addPlugToManipConversionCallback(this->controlPtsIndex(),
		(plugToManipConversionCallback)&CustomManipContainer::doubleArrayPToMConversionCallback,
		plugSlot); 
	return(stat);
}

void BezierCurveManip::setDrawAxes(bool flag)
{
	this->drawAxes = flag;
}
void BezierCurveManip::setDrawTangents(bool flag)
{
	this->drawTangents = flag;
}
void BezierCurveManip::setLabel(const MString& label)
{
	this->label = label;
}
void BezierCurveManip::setPosition(const MVector& pos)
{
	this->position = pos;
}
void BezierCurveManip::setSize(const MVector& size)
{
	this->size = size;
}

bool BezierCurveManip::getDrawAxes()
{
	return this->drawAxes;
}
bool BezierCurveManip::getDrawTangents()
{
	return this->drawTangents;
}
MString BezierCurveManip::getLabel()
{
	return this->label;
}
MVector BezierCurveManip::getPosition()
{
	return this->position;
}
MVector BezierCurveManip::getSize()
{
	return this->size;
}

unsigned BezierCurveManip::controlPtsIndex()
{
	return(this->controlPtsSlot);
}

// This should probably use caching somehow...
inline unsigned factorial(unsigned n)
{
	unsigned total = 1;
	for(unsigned i=2; i<n; i++)
	{
		total *= i;
	}
	return(total);
}

inline double calcBezierCoeff(unsigned n, unsigned i, double t)
{
	// ASSERT(n >= i);

	double minT = 1.0 - t;

	// Calculate the choose formula
	double coeff = factorial(n) / (factorial(i) * factorial(n-i));

	return(coeff * pow(minT, n-i) * pow(t, i));
}

void BezierCurveManip::draw(M3dView& view)
{
	MObject val = this->controlPts.asMObject();
	MFnDoubleArrayData array(val);

	// Calculate the curve points, make this a manipulator attribute.
#define NUMSEGMENTS 40

	// Assume the number of control points is always 4 for now...
	MDoubleArray dArray(NUMSEGMENTS);
	for(int i=0; i<NUMSEGMENTS; i++)
	{
		double t = double(i)/ (NUMSEGMENTS - 1);

		for(unsigned j=0; j<array.length(); j++)
		{
			dArray[i] += array[j] * calcBezierCoeff(array.length()-1, j, t);
		}
	}
#undef NUMSEGMENTS	
	// Get the A

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Clear the projection matrix
		glLoadIdentity();

		// Set up the viewing matrix
		gluOrtho2D(0.0, (GLdouble)view.portWidth(),
				   0.0, (GLdouble)view.portHeight());

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glLoadIdentity();
			glTranslated(this->position[0], this->position[1], 0);
			glScaled(this->size[0], this->size[1], 0);

			// Draw the border
			glColor3d(0, 0, 0);
			glBegin(GL_LINE_LOOP);
			{
				glVertex2d(0, 0);
				glVertex2d(1, 0);
				glVertex2d(1, 1);
				glVertex2d(0, 1);
			}
			glEnd();

			// Draw the curve as a filled polygon
			// Later...

			// Draw the curve 
			glColor3d(1, 0, 0);
			glBegin(GL_LINE_STRIP);
			{
				for(unsigned i=0; i<dArray.length(); i++)
				{
					double x = double(i) / (dArray.length() - 1);
					double y = dArray[i];
					glVertex2d(x, y);
				}
			}
			glEnd();

			// Draw the axes


			// Draw the tangents

			// Draw the points
			glPushAttrib(GL_POINT_BIT);
			{ 
				// Make the points pretty big so they show properly...
				glPointSize(5.0f);
				glEnable(GL_POINT_SMOOTH);

				// Get the active and manip colors
				int currentColor = this->queryDisplayColor("manipCurrent", true);
				int handleColor = this->queryDisplayColor("manipHandle", true);

				glBegin(GL_POINTS);
				{
					for(unsigned i=0; i<array.length(); i++)
					{
						// Set the right color
						int idx = handleColor;
						if(i == this->getComponent())
						{
							idx = currentColor;
						}
						MColor color = this->queryColorIndex(idx);
						glColor3f(color.r, color.g, color.b);
						double x = double(i) / (array.length()-1);
						double y = array[i];
						glVertex2d(x, y);
					}
				}
				glEnd();
			}
			glPopAttrib();
			// this->render(view); 

			// Draw the label
			glColor3d(0, 0, 0);
			if(this->label != "")
			{
				// Draw the label
				MPoint nearPt, farPt;
				view.viewToWorld(short(this->position[0]), short(this->position[1]+this->size[1]+4), nearPt, farPt);
				view.drawText(this->label, nearPt, M3dView::kLeft);
			}
		}
		glPopMatrix();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void BezierCurveManip::select(M3dView& view)
{
	MObject val = this->controlPts.asMObject();
	MFnDoubleArrayData array(val);

	// Setup the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		// Set up the viewing matrix
		gluOrtho2D(0.0, (GLdouble)view.portWidth(),
				   0.0, (GLdouble)view.portHeight());

		// Setup the modelview matrix
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glLoadIdentity();
			glTranslated(this->position[0], this->position[1], 0);
			glScaled(this->size[0], this->size[1], 0);   

			double size = 5.0/this->size[0];

			// Draw the dots
			for(unsigned i=0; i<array.length(); i++)
			{
				glLoadName(i);
				glBegin(GL_QUADS);
				{
					// MGlobal::displayInfo(MString("Drawing point ") + i);
					double x = double(i) / (array.length()-1);
					double y = array[i];
					glVertex2d(x-size, y-size);
					glVertex2d(x-size, y+size);
					glVertex2d(x+size, y+size);
					glVertex2d(x+size, y-size);
				}
				glEnd();
			}
		}
		glPopMatrix();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); 
}

void BezierCurveManip::render(M3dView& view)
{
	MObject val = this->controlPts.asMObject();
	MFnDoubleArrayData array(val);

	// Make the points pretty big
	glPushAttrib(GL_POINT_BIT);
	{ 
		glPointSize(5.0f);
		glEnable(GL_POINT_SMOOTH);

		glBegin(GL_POINTS);
		{
			for(int i=0; i<array.length(); i++)
			{
				double x = double(i) / (array.length()-1);
				double y = array[i];
				glVertex2d(x, y);
			}
		}
		glEnd();
	}
	glPopAttrib();
}

// Mouse event handlers
void BezierCurveManip::onPress(const CustomEvent& event)
{
	CustomManip3D::onPress(event);
	// nothing...
}
void BezierCurveManip::onDrag(const CustomEvent& event)
{
	CustomManip3D::onDrag(event);

	// Get the current y location.
	double val = event.y;

	// Adjust for scaling and position
	val = (val-this->position[1]) / this->size[1];

	// Set the array element
	MObject obj = this->controlPts.asMObject();
	MFnDoubleArrayData array(obj);
	array[this->getComponent()] = val;
}
void BezierCurveManip::onRelease(const CustomEvent& event)
{
	CustomManip3D::onRelease(event);
}

