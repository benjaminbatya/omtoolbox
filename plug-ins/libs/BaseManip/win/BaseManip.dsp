# Microsoft Developer Studio Project File - Name="BaseManip" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=BaseManip - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BaseManip.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BaseManip.mak" CFG="BaseManip - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BaseManip - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "BaseManip - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BaseManip - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "BaseManip - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "C:\Program Files\Alias\Maya6.0\include" /I "..\include" /I "c:\cygwin\work\CoreLibraries\MayaUtils\include" /I "c:\cygwin\work\CoreLibraries\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /D "NT_PLUGIN" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy   Debug\BaseManip.lib   ..\..\..\bin\ 
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "BaseManip - Win32 Release"
# Name "BaseManip - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\BezierCurveManip.cpp
# End Source File
# Begin Source File

SOURCE=..\CatmullRomCurveManip.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomEvent.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomManip3D.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomManipContainer.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomManipContainerCallbacks.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomManipSetAttrCmd.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomRotateManip.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomScaleManip.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomSelectionContext.cpp
# End Source File
# Begin Source File

SOURCE=..\CustomSelectToolCmd.cpp
# End Source File
# Begin Source File

SOURCE=..\FreeTriadPointManip.cpp
# End Source File
# Begin Source File

SOURCE=..\MaxPointManip.cpp
# End Source File
# Begin Source File

SOURCE=..\ViewportDistanceManip.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\include\BezierCurveManip.h
# End Source File
# Begin Source File

SOURCE=..\include\CatmullRomCurveManip.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomEvent.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomLocatorManip.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomManip3D.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomManipContainer.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomManipSetAttrCmd.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomPlugin.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomRotateManip.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomScaleManip.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomSelectionContext.h
# End Source File
# Begin Source File

SOURCE=..\include\CustomSelectToolCmd.h
# End Source File
# Begin Source File

SOURCE=..\include\FreeTriadPointManip.h
# End Source File
# Begin Source File

SOURCE=..\include\MaxPointManip.h
# End Source File
# Begin Source File

SOURCE=..\include\ViewportDistanceManip.h
# End Source File
# End Group
# End Target
# End Project
