#
# Build framework - a primitive crossplatform build system for Maya plugins
# Written by Benjamin Schleimer
# Copyright (C) 2005 Benjamin A. Schleimer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

CXX=g++
RM=rm
MV=mv
CP=cp
LD=g++

OUT=-o
DSO_EXTENSION=so
LIB_EXTENSION=a
LIB_PREFIX=lib
OBJ_EXTENSION=o

PLATFORM_LIBS=

DEBUG_FLAGS=-g -D_DEBUG
RELEASE_FLAGS=-O2 -D_RELEASE
LINK_LIB_PREFIX=-l
LINK_LIB_POSTFIX=
CXXFLAGS=-Wall -c -D_BOOL -Wno-deprecated -DLINUX -DUSE_SOFTWARE_OVERLAYS
LDEXEFLAGS=
LDLIBFLAGS=-shared -Wl
LDDSOFLAGS=-shared -Wl
INCL=-I
LIBPATH=-L

