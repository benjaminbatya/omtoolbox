#
# Build framework - a primitive crossplatform build system for Maya plugins
# Written by Benjamin Schleimer
# Copyright (C) 2005 Benjamin A. Schleimer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Include Platform specific variables
UNAME=$(shell uname)
ifeq ($(UNAME),Linux)
    ifeq ($(USE_WINE),1)
	include $(WORK_PATH)/libs/build/def-wine.mak
    else
	include $(WORK_PATH)/libs/build/def-linux.mak
    endif 
else
    include $(WORK_PATH)/libs/build/def-win32.mak
endif

DSO_NAME=$(MODULE_NAME).$(DSO_EXTENSION)
LIB_NAME=$(LIB_PREFIX)$(MODULE_NAME).$(LIB_EXTENSION)	
ifeq ($(BUILD_LIB),1)
	TARGET_NAME=$(LIB_NAME)
else
	TARGET_NAME=$(DSO_NAME) 
endif 

# make sure MAYA_PATH is set correctly
ifndef MAYA_PATH
	$(error environment variable MAYA_PATH must be defined!)
endif 

ifeq ($(BUILD_LIB),1)
	LDFLAGS=$(LDLIBFLAGS) 
else
	LDFLAGS=$(LDDSOFLAGS)
endif  

# Stupid hack to work around the space in "Program Files"
# THIS WILL NOT WORK IN GENERAL!!!
NWS_MAYA_PATH=$(subst Program Files,Program!Files,$(MAYA_PATH))

# Define global variables
OBJECTS = $(patsubst %.cpp,%.$(OBJ_EXTENSION), $(SOURCES))

# Maya definitions
MAYA_INCLUDES=$(NWS_MAYA_PATH)/include
MAYA_LIB_PATH=$(NWS_MAYA_PATH)/lib
MAYA_LIBS=Foundation OpenMaya OpenMayaUI OpenMayaAnim

BIN_PATH=$(WORK_PATH)/bin
ifeq ($(BUILD_LIB),1)
	INSTALL_PATH=$(BIN_PATH)
else
	INSTALL_PATH=plug-ins
endif

# Build the INCLUDE_PATHS
__INCLUDE_PATHS=$(MAYA_INCLUDES) $(PROJECT_INCLUDE_PATHS) $(WORK_PATH)/include
# Expand the include paths
_INCLUDE_PATHS=$(foreach path,$(__INCLUDE_PATHS),$(INCL)"$(path)")
INCLUDE_PATHS=$(subst !, ,$(_INCLUDE_PATHS))

# Build the lib paths
__LIB_PATHS=$(MAYA_LIB_PATH) $(BIN_PATH) $(PROJECT_LIB_PATHS)
# Expand the lib paths
_LIB_PATHS=$(foreach path,$(__LIB_PATHS),$(LIBPATH)"$(path)")
LIB_PATHS=$(subst !, ,$(_LIB_PATHS))

# Build the libs 
__LIBS=$(MAYA_LIBS) $(PLATFORM_LIBS) $(PROJECT_LIBS)
# expand the libs
_LIBS=$(foreach lib,$(__LIBS),$(LINK_LIB_PREFIX)$(lib)$(LINK_LIB_POSTFIX))
LIBS=$(subst !, ,$(_LIBS))

# Make a PROJECT_LIBS_IN_BIN string
PROJECT_LIBS_IN_BIN=$(foreach lib,$(PROJECT_LIBS),$(BIN_PATH)/$(LINK_LIB_PREFIX)$(lib)$(LINK_LIB_POSTFIX))

# Build Targets
TARGETS= all clean install doc install-doc

.PHONY:	all

%.$(OBJ_EXTENSION): %.cpp $(HEADERS)
	$(CXX) $(CPPFLAGS) $(INCLUDE_PATHS) $< $(OUT)$@ 

all : debug

# Make the target rely on the PROJECT_LIBS_IN_BIN
$(TARGET_NAME) : $(OBJECTS) $(PROJECT_LIBS_IN_BIN)
	$(LD) $(LDFLAGS) $(OBJECTS) $(LIB_PATHS) $(LIBS) $(OUT)$@

clean: 
	$(RM) -f $(OBJECTS) *.$(LIB_EXTENSION)
	$(RM) -f *.idb *.pdb *.exp
	$(RM) -f 
	$(RM) -f $(TARGET_NAME)

debug: CPPFLAGS=$(DEBUG_FLAGS) $(CXXFLAGS)
debug: $(TARGET_NAME)
    
release: CPPFLAGS=$(RELEASE_FLAGS) $(CXXFLAGS)
release: $(TARGET_NAME)

# Install defaults to debug for now...
install: debug
	-mkdir $(INSTALL_PATH) 
	@echo "Installing module $(MODULE_NAME)"
	$(CP) $(TARGET_NAME) $(INSTALL_PATH)
	
