# Useful bash script for automatically generating projects
# Written by Benjamin Schleimer
# Copyright (C) 2005 Benjamin A. Schleimer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#!/bin/bash

if [ ! $# -eq 2 ]; then
    echo "USAGE: $0 <Category name> <project name>"
fi

CATEGORY_NAME=$1
PROJECT_NAME=$2

# Create the category parallel to CoreLibraries
BASE_PATH=../../..

mkdir $BASE_PATH/$CATEGORY_NAME
mkdir $BASE_PATH/$CATEGORY_NAME/$PROJECT_NAME

sed "s/MY_MODULE/$PROJECT_NAME/g" Makefile > $BASE_PATH/$CATEGORY_NAME/$PROJECT_NAME/Makefile

echo "project is created in \"$BASE_PATH/$CATEGORY_NAME/$PROJECT_NAME\""
echo "it also contains a Makefile that will create a Plug-in of that name"
echo "(for win32: $PROJECT_NAME.mll and for linux lib$PROJECT_NAME.so)"
echo
echo n-joy
