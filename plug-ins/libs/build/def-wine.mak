#
# Build framework - a primitive crossplatform build system for Maya plugins
# Written by Benjamin Schleimer
# Copyright (C) 2005 Benjamin A. Schleimer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

CXX=wine CL
RM=rm 
MV=mv
CP=cp
LD=wine link.exe
# MKDIR=mkdir

OUT=/out:
# OUT=/Fo
DSO_EXTENSION=mll
LIB_EXTENSION=lib
LIB_PREFIX=
OBJ_EXTENSION=obj

PLATFORM_LIBS=opengl32 glu32 gdi32 user32

DEBUG_FLAGS=/MTd /Od /GZ /ZI /D"_DEBUG" /DEBUG
RELEASE_FLAGS=/MT /Ox /D"_RELEASE"
LINK_LIB_PREFIX= 
LINK_LIB_POSTFIX=.lib 
CXXFLAGS  =/c /nologo /W3 /GX /FD /D"NT_PLUGIN" /D"WIN32" /D"USE_SOFTWARE_OVERLAYS"
LDEXEFLAGS= /nologo /subsystem:console 
LDLIBFLAGS=-lib /nologo
LDDSOFLAGS=/nologo /dll
INCL=/I
LIBPATH=/libpath:
