/**
Format - Conversion library for performing Maya class -> MString conversions
Written by Benjamin Schleimer
Copyright (C) 2005 Benjamin A. Schleimer

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <Format.h>

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>

MString Format::toString(const MVector& vec)
{
	return(MString("(") + vec.x + ", " + vec.y + ", " + vec.z + ")");
}

MString Format::toString(const double* row)
{
	return(MString("(") + row[0] + ", " + row[1] + ", " + row[2] + ", " 
		   + row[3] + ")");
}

MString Format::toString(const float* row)
{
	return(MString("(") + row[0] + ", " + row[1] + ", " + row[2] + ", " 
		   + row[3] + ")");
}

MString Format::toString(const MMatrix& mat)
{
	MString str = MString("{\n") + 
				  Format::toString(mat[0]) + "\n" +
				  Format::toString(mat[1]) + "\n" +
				  Format::toString(mat[2]) + "\n" +
				  Format::toString(mat[3]) + "\n}";

	return(str);
}

MString Format::toString(const float** mat)
{
	MString str = MString("{\n") + 
				  Format::toString(mat[0]) + "\n" +
				  Format::toString(mat[1]) + "\n" +
				  Format::toString(mat[2]) + "\n" +
				  Format::toString(mat[3]) + "\n}";

	return(str);
}

MString Format::toString(const MPoint& pos)
{
	MString str = MString("(") + pos.x + ", " + pos.y + ", " + pos.z + 
				  ", " + pos.w + ")";

	return(str);
}
