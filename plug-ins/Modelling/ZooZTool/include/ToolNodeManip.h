/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/*******************************************************
 * ToolNodeManip.h - manipulator node for the ToolNode.
 * Inherits from CustomManipContainer so custom base manips can be used.
 *******************************************************/
 
#ifndef _TOOLNODEMANIP_H_
#define _TOOLNODEMANIP_H_

#include <BaseManip/CustomManipContainer.h>

class FreeTriadPointManip;
class CustomRotateManip;
class CustomScaleManip;

class ToolNodeManip : public CustomManipContainer
{
public:
	static void* creator();

	virtual MStatus customConnectToDependNode(const MObject & depNode);

	virtual void drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status);

	static const MTypeId typeId;
	static const MString typeName;

private:
	// The base transformation manips
	FreeTriadPointManip* moveManip;
	CustomRotateManip* rotateManip;
	CustomScaleManip* scaleManip;

	// Modifier GUI manips
	// Add later...

	// Object the manip will be working on
	MObject targetObj;
};


#endif
