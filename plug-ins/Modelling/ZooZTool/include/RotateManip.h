/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/***************************************************
 * MoveToolManip.h - manipulator for the MoveTool.
 * This DOES NOT attach to any node. It simply informs the MoveToolContext instance that it has been
 * updated and queries the MoveToolContext forpoint position data.
 * Inherits from MPxManipContainer for now. 
 * Maybe later will use the CustomManipContainer framework for custom base manips.
 ***************************************************/

#ifndef _ROTATEMANIP_H_
#define _ROTATEMANIP_H_

#include "BaseManip.h"

// Forward declare the needed classes
class CustomRotateManip;

class RotateManip : public BaseManip
{
public:
	static void* creator();
	// Override virtual
	virtual MStatus customConnectToDependNode(const MObject&);
	
	// Override the press and release event methods so
	// we can signal the context that we are starting/ending drag.
	virtual void onPress(const CustomEvent&);
	virtual void onRelease(const CustomEvent&);

	static MTypeId typeId;
	static MString typeName;

private:
	// Add the callback functions 
	MManipData centerPtoMCB(unsigned);
	MManipData rotatePToMCB(unsigned);
	MManipData rotateMToPCB(unsigned);

private: 
	// Pointer to the base rotate manip
	CustomRotateManip* rotateManip;
};


#endif

