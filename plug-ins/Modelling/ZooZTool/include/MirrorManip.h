/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/***************************************************
 * MirrorManip.h - Mirror modifier manipulator.
 * This attaches if mirror is enabled
 ***************************************************/

#ifndef _MIRRORMANIP_H_
#define _MIRRORMANIP_H_

#include "BaseManip.h"

class MirrorManip : public BaseManip
{
public:
	static void* creator();
	virtual MStatus customConnectToDependNode(const MObject&);
	virtual void drawManip(M3dView&, const MDagPath&, M3dView::DisplayStyle, M3dView::DisplayStatus);

	static MTypeId typeId;
	static MString typeName;

private:
	MManipData centerPToMCB(unsigned);
	MManipData directionPToMCB(unsigned);
	MManipData generalMToPCB(unsigned);

private:
	MDagPath directionManip;
	MDagPath dummyManip;

};


#endif


