/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#ifndef _CMDFLAGS_H_
#define _CMDFLAGS_H_

// Stupid C declaration fuckups for multiple types!!!
typedef char* charPtr;


/** 
 * Flags for both the MoveToolCmd and MoveToolCommandContext
 */
 
 /* MoveSpace flag - indicates the type of space the move axes (and other manips?) should be in.
 * Types are:
 *		World - Use the world space
 *		Local - Use the parent's space
 *		Object - Use the object's space
 *		Normal - Use the selected vertices' normals
 * Default is World
 */
extern const charPtr kMoveSpace, kMoveSpaceLong;

/* *
 * Falloff Type - indicates which type of falloff should be in effect
 * Falloff distance for each vertex is calculated as the dist from the closest selected vertex
 * Types are:
 *		None - disable falloff
 *		Spherical - closest distance = sqrt(dx^2 + dy^2 + dz^2)
 *		LinearX - 
 *		LinearY -
 *		LinearZ - The falloff axis is used to determine the closest distance between 
 *	  			the vert and selected verts by distance = (dVec * normal)/|normal|
 *		LinearView - should this be used?? 
 *		LinearCustom - Same as the linearX/Y/Z but with Custom Axis defined
 *		CylinderX -
 *		CylinderY -
 *		CylinderZ - closest distance = 
 *				project both selected and normal verts onto plane and calc sqrt(dx^2 + dy^2)
 *		CylinderView - same but plane is the view plane, 
 *				dx is left/right in the screen, dy is up/down in the screen and
 *		CylinderCustom - User defines the X and Y axes
 *	    Topology - distance depends on the shortest edge distance from the vert to the closest selected vert 
 * 	Default is none
 */
extern const charPtr kFalloffType, kFalloffTypeLong;
/**
 * Falloff outer radius - value to scale x=1 to in world space (or screen space, incase of screen falloffType)
 * default is 1
 */
extern const charPtr kFalloffOuterRadius, kFalloffOuterRadiusLong;
/** 
 * Falloff inner radius - value to scale x=0 to in world space (or screen space, incase of screen falloffType)
 * default is 0
 */
extern const charPtr kFalloffInnerRadius, kFalloffInnerRadiusLong;

/**
 * Falloff curve - the falloff curve expressed as a 4-point bezier curve.
 * (NOTE: Later make this selectable between bezier, NURBS, and Catmull-Rom) and allow the
 * number of CVs to be controllable
 * default is [1, 1, 1, 1]
 */
extern const charPtr kFalloffCurve, kFalloffCurveLong;

/**
 * Falloff axis flags - the axis vector for the Linear and cylinder falloffs
 * default is [0, 0, 1]
 */ 
extern const charPtr kFalloffAxis, kFalloffAxisLong;

/**
 * Mirror type flag  
 * Types are:
 *		None - disable mirroring
 *		X - mirror along the X axis
 *		Y - mirror along the Y axis
 *		Z - mirror along the Z axis
 *		View - cross the lookat vector and the screen's up vector to determine the mirror axis
 *		Custom - use a custom axis for mirroring (see below)
 * Default is None
 */
extern const charPtr kMirrorType, kMirrorTypeLong;
/**
 * Custom mirror axis flag (has 3 float args)
 * default is [0, 0, 1]
 */
extern const charPtr kMirrorAxis, kMirrorAxisLong;

/**
 * Transformation adjustment - a matrix to adjust the transformation matrix by. 
 * This is so the user can move/rotate/scale the space the transform will happen in
 * without modifying the object's pivot. basically TMat' = AdjMat * TMat * AdjMat^-1
 * default is identity matrix
 */
// const  ktransformAdjust = "-ta", ktransformAdjustLong = "-transformAdjust";

/**
 * Constraint - None, Grid, Vertices, Edges, Faces
 * Default is None
 */
// const  kConstraint = "-c", kConstraintLong = "-constraint";

/**
 * Constraint magnetic distance - the distance over which the closest element (grid point, vertex, edge, or face)
 * "pulls" the center of movement
 */
// const  kConstraintDist = "-cd", kConstraintDistLong = "-constraintDist";

// Add discrete movement both a distance and direction arguments


#endif
