/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#ifndef _SCALECONTEXTCMD_H_
#define _SCALECONTEXTCMD_H_

#include <maya/MStatus.h>

#include "BaseContextCmd.h"

class ScaleContext;

class ScaleContextCmd : public BaseContextCmd
{
public:
    virtual MPxContext* makeObj();
    static void* creator();
    
	static const MString toolName;
};

#endif
