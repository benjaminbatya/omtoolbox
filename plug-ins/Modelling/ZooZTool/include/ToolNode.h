/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

// ToolNode.h - interface for the eventual node which will be created and
// inserted into the DG when the MoveToolCmd::redo is called.
// Undo will remove the node and restore the DG state to before.

#ifndef _TOOLNODE_H_
#define _TOOLNODE_H_

#include <maya/MPxDeformerNode.h>
#include "ToolModel.h"

// typedefs for setter/getter functions for the model
typedef MStatus (MoveToolCmdModel::*modelSetterFunc)(const MVector&);
typedef MVector (MoveToolCmdModel::*modelGetterFunc)(MStatus*) const;

class MoveToolNode : public MPxDeformerNode
{
	///
public:
	MoveToolNode();
	virtual ~MoveToolNode();

	static void* creator() { return(new MoveToolNode); }
	static MStatus initialize();

	virtual MStatus compute(const MPlug&, MDataBlock&);

	// Type info	
    static const MTypeId typeId;
    static const MString typeName; 

	// I guess this isn't really enabled yet
	// virtual bool getInternalValueInContext( const MPlug&, MDataHandle&, MDGContext&); 
    // virtual bool setInternalValueInContext( const MPlug&, const MDataHandle&, MDGContext&);
 
	virtual bool getInternalValue(const MPlug&, MDataHandle&);
	virtual bool setInternalValue(const MPlug&, const MDataHandle&);
	virtual void copyInternalData( MPxNode* that);

private:  
	bool setVectorPlug(const MObject&, const MDataHandle&, const MObject&, modelGetterFunc, modelSetterFunc, MStatus* = NULL);
	bool getVectorPlug(const MObject&, MDataHandle&, const MObject&, modelGetterFunc, MStatus* = NULL);
	
	// Model
	MoveToolCmdModel* model;

	// translation to apply to the mesh
	static MObject translation;
	// Scale to apply
	static MObject scale;
	// Rotation to apply
	static MObject rotation;

	// falloff attributes
	static MObject falloffType;
	static MObject falloffInnerRadius;
	static MObject falloffOuterRadius;
	static MObject falloffCurve;
	static MObject falloffInvert;
	static MObject falloffAxis;
	static MObject falloffWeights;

	// mirror attributes
	static MObject mirrorType;
	static MObject mirrorAxis;
	static MObject mirrorWeights;
};


#endif
