/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/***************************************************
 * FalloffManip.h - Falloff modifier manipulator.
 * This attaches if falloff is enabled
 ***************************************************/

#ifndef _FALLOFFMANIP_H_
#define _FALLOFFMANIP_H_

#include "BaseManip.h"

// Forward declare the needed classes
class ViewportDistanceManip;
class CatmullRomCurveManip;

class FalloffManip : public BaseManip
{
public:
	static void* creator();
	virtual MStatus customConnectToDependNode(const MObject&);
	virtual void drawManip(M3dView&, const MDagPath&, M3dView::DisplayStyle, M3dView::DisplayStatus);

	static MTypeId typeId;
	static MString typeName;

private:
	// Add the callback functions
	MManipData generalMToPCB(unsigned);
	
	MManipData centerPToMCB(unsigned);
	MManipData innerRadiusPToMCB(unsigned);
	MManipData outerRadiusPToMCB(unsigned);
	MManipData curvePToMCB(unsigned); 

private: 
	// Outer and inner radius manips
	ViewportDistanceManip* outerDistManip;
	ViewportDistanceManip* innerRadiusManip;

	// Pointer to the falloff curve manip.
	CatmullRomCurveManip* curveManip;
};


#endif


