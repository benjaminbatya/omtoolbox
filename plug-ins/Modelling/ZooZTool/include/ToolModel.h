/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/*************************************
 * MoveToolCmdModel - I seperated the MoveToolCmd from it's model because of very strange things
 */

#ifndef _TOOLMODEL_H_
#define _TOOLMODEL_H_

#include <maya/MVector.h>
#include <maya/MPoint.h>
#include <maya/MDistance.h>
#include <maya/MDoubleArray.h>
#include <maya/MIntArray.h>
#include <maya/MPointArray.h>
#include <maya/MDagPath.h>
#include <maya/MDataHandle.h>

#include "Subject.h"

// Define the different kinds of notification messages
enum {
	MSG_FALLOFF_TYPE = 0,
	MSG_FALLOFF_OUTERRADIUS,
	MSG_FALLOFF_INNERRADIUS,
	MSG_FALLOFF_CURVE,
	MSG_FALLOFF_INVERT,
	MSG_FALLOFF_AXIS,
	MSG_MIRROR_TYPE,
	MSG_MIRROR_AXIS,
	MSG_MODEL_LAST
};

class MoveToolCmdModel;
typedef double (MoveToolCmdModel::*FalloffDistFunc)(const MPoint&, const MPoint&) const;

class MoveToolCmdModel : public Subject
{
public:
	// Update type information
	// Can either be:
	// 1) DagPath and Component for context updates
	// 2) DataHandle and groupId for node updates
	enum IterationType 
	{
		// kNoTypeSet = 0,
		kContextSet,
		kNodeSet
	};
	MoveToolCmdModel(const IterationType type);
	virtual ~MoveToolCmdModel();

	MStatus clear();
	MStatus resetCurve();
	MoveToolCmdModel& operator=(const MoveToolCmdModel& that);
 
	MStatus setContextGeom(MDagPath& dagPath, MObject& component);
	MStatus setNodeGeom(MDataHandle& dataHandle, unsigned groupId);

	virtual MStatus undoIt();
	virtual MStatus redoIt();
 
	// This resets the model everytime a transformation is completed 
	MStatus reset();
	
	// Transformation setters and getters
	MStatus resetTransform(); 
	MVector getTranslation(MStatus* stat = NULL) const;
	MStatus setTranslation(const MVector&);
	MVector getScale(MStatus* stat = NULL) const;
	MStatus setScale(const MVector&);
	MVector getRotation(MStatus* stat = NULL) const;
	MStatus setRotation(const MVector&);

	MPoint getCenterPoint(MStatus* stat = NULL);
	

	// Enums for the different
	enum FalloffType
   	{
		kNone = 0,
		kSpherical,
		kLinearX,
		kLinearY,
		kLinearZ,
		kLinearView,
		kLinearCustom,
		kCylinderX,
		kCylinderY,
		kCylinderZ,
		kCylinderView,
		kCylinderCustom,
		kTopology,   
		kLastFalloffType
	};
	MStatus setFalloffType(const FalloffType&);
	FalloffType getFalloffType() const;
	MStatus setFalloffInnerRadius(const MDistance&);
	MDistance getFalloffInnerRadius() const;
	MStatus setFalloffOuterRadius(const MDistance&);
	MDistance getFalloffOuterRadius() const;
	MStatus setFalloffCurve(const MDoubleArray&);
	MDoubleArray getFalloffCurve() const;
	MStatus setFalloffAxis(const MVector&);
	MVector getFalloffAxis(MStatus* stat = NULL) const;    

	// This forces the falloff and mirror weights to be recalculated next time they are needed
	// by processGeom or getXXXWeights. Use sparingly
	MStatus setViewChanged(); 

	// This gets the up-to-date falloff weights
	MDoubleArray getFalloffWeights(MStatus* stat = NULL);

	enum MirrorType {
		kMirrorNone = 0,
		kMirrorX,
		kMirrorY,
		kMirrorZ,
		kMirrorView,
		kMirrorCustom,
		kMirrorLast
	};
	MStatus setMirrorType(const MirrorType&);
	MirrorType getMirrorType() const;
	MStatus setMirrorAxis(const MVector&);
	MVector getMirrorAxis(MStatus* stat = NULL) const;   

	// This gets the up-to-date mirror weights
	MDoubleArray getMirrorWeights(MStatus* stat = NULL);

	// This processes the selected mesh
	MStatus processGeom();

private:
	// Calculates the center point
	MStatus calcCenterPoint();
	// Get the regular matrix
	MMatrix getRegularMatrix(MStatus* stat = NULL);
	// Gets the mirror transform
	MMatrix getMirrorMatrix(MStatus* stat = NULL);

	// Recalculates the falloff weights for the manipulator
	// The two different forms are for context calculations
	MStatus calcFalloffWeights();
	// General calcWeights method
	MStatus calcWeights(MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt, MObject& shapeObj, const MMatrix& worldMatrix);
	// Specific type calc weights methods
	MStatus calcNoneFalloff(MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt) const;
	MStatus calcFalloff(FalloffDistFunc, MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt) const;
	MStatus calcTopologyFalloff(MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt, MObject& shapeObj, const MMatrix& worldMatrix) const;
	// Completes the weight calculation for calcFalloff and calcTopology methods
	MStatus final_calcFalloffWeights(MDoubleArray& weights, const MDoubleArray& deltas) const;

	// Methods and members for calcFalloff method
	// Direction vector is used for all of the "Interesting" falloff types
	MVector falloffDirection;
	double calcSphericalDist(const MPoint& a, const MPoint& b) const;
	double calcLinearDist(const MPoint& a, const MPoint& b) const;
	double calcCylinderDist(const MPoint& a, const MPoint& b) const;
	
	// Recalculates the mirror weights
	MStatus calcMirrorWeights();

	// Gets the geometry information from the private geometry data
	MStatus getGeometryInfo(MObject&, MMatrix&, MItGeometry*&, MItGeometry*& );

	// Helper method for calculating the current view side vector in world space
	MVector calcSideVector(MStatus* stat = NULL) const;

private:
	MTransformationMatrix transform; 

	// Center point, used for the pivot point of the transform
	// and center point of any manips
	MPoint centerPoint;
	bool centerPointDirtyFlag;

	// Falloff properties
	FalloffType falloffType;
	MDistance falloffInnerRadius; 
	MDistance falloffOuterRadius;
	MDoubleArray falloffCurve;
	MVector falloffAxis;
	MDoubleArray falloffWeights; 
	// The falloffWeights dirty flag.
	// Used to determine if the falloffweights need to be recalculated or not
	// NOTE: rename this to falloffWeightsDirty to correspond to mirrorWeightsDirty
	bool falloffWeightsDirtyFlag;

	// Mirror properties
	MirrorType mirrorType;
	MVector mirrorAxis;
	MDoubleArray mirrorWeights;
	bool mirrorWeightsDirty;

	// The type of geometry data the model can receive
	const IterationType updateType;

	// Data types for context updates
	MDagPath dagPath;
	MObject component;
	// Data types for node updates
	MDataHandle dataHandle;
	unsigned groupId;

	// These are the saved positions for undoing the movement.
	MPointArray undoPositions;
};

#endif
