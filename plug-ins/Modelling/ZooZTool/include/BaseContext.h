/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#ifndef _BASECONTEXT_H_
#define _BASECONTEXT_H_

#include <BaseManip/CustomSelectionContext.h>
#include <maya/MVector.h>
#include <maya/MDistance.h>

#include "ToolModel.h"
#include "Observer.h"

// Forward declare the PropertySheet
class PropertySheetView;

enum {
	MSG_CENTER = MSG_MODEL_LAST
};

class BaseContext : public CustomSelectionContext, public Subject, public Observer
{
public:
    BaseContext(const MString& name, const MString& image,
		const MString& titleStr, const MString& helpStr, const MString& toolName);
	virtual ~BaseContext();
	virtual void getClassName(MString& name) const;
	virtual MStatus doEnterRegion(MEvent& event);

    virtual void toolOnSetup(MEvent& event);
    virtual void toolOffCleanup();

    // Used whenever the button is released so the selection can be made
	MStatus doSelectionChanged();
	// Used whenever the user moves the camera so we can update the falloff if needed
	MStatus doViewDirty();
	// Update's whenever the cmdModel notifies and this will notify all of its listeners.
	virtual void Update(const NotificationMsg& msg);
private:    
	// Registered callback IDs
    MIntArray callbackIDs; 
	// Context info
	MString name, image, titleStr, helpStr, toolName;

public:
	MStatus calcPlaneLineIntersection(const MPoint& rayStart, const MVector& rayDirection,
		const MPoint& pos, const MVector& normal,
		MPoint& retVal);
 
private:
	// The propertySheet view
	PropertySheetView* propertySheetView;

protected:
	// Override deleteManipulators to clear moveManip
	virtual MStatus addManipulators(const MObject& depNode);
	virtual MStatus deleteManipulators();

	MStatus finalizeCmd(); 
	virtual bool isChangeLarge(MStatus* stat = NULL) = 0;

public:
	// Gets the model for the views
	MoveToolCmdModel* getModel();
private:
	// The currently working command
	MoveToolCmdModel* model;

public:
	MStatus getCenter(MPoint& center);
	
	virtual MStatus doManipPress();
	virtual MStatus doManipRelease(); 

	// Getters/Setters for the display attributes
	MStatus setDisplayFalloffManip(const bool);
	bool getDisplayFalloffManip() const;
	MStatus setDisplayMirrorManip(const bool);
	bool getDisplayMirrorManip() const;

private:
	bool displayFalloffManip;
	bool displayMirrorManip;
};

#endif
