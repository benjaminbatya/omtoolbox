/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#ifndef _TRANSLATECONTEXT_H_
#define _TRANSLATECONTEXT_H_

#include <maya/MVector.h>
#include <maya/MDistance.h>

#include "BaseContext.h"

enum {
	MSG_DRAW_ARROW_HEADS = MSG_CENTER+1,
	MSG_CONTEXT_LAST
};

class TranslateContext : public BaseContext
{
public:
    TranslateContext();

	// Override the do events so we can implement immedient Click-n-Drag 
	virtual MStatus doCustomPress( MEvent & event );
	virtual MStatus doCustomDrag( MEvent & event );
	virtual MStatus doCustomRelease( MEvent & event );
	virtual MStatus doCustomHold(MEvent& event) { return(MS::kSuccess); }

	// Getter/setters
	MVector getTranslation(MStatus* stat = NULL);
	MStatus setTranslation(const MVector& vec);

	bool getDrawArrowHead() const;
	void setDrawArrowHead(const bool);

protected:
	virtual bool isChangeLarge(MStatus* stat = NULL);

private:
	bool _drawArrowHead;

	// The drag offset to apply when in the customDrag method
	MVector customDragDelta;
};

#endif
