/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/*******************************************
 * BaseManip.h - base class for all of the toolManips
 * This stores the Context and some other useful stuff.
 *******************************************/
 
#ifndef _BASEMANIP_H_
#define _BASEMANIP_H_

#include <BaseManip/CustomManipContainer.h>
#include "Observer.h"

class BaseContext;

class BaseManip : public CustomManipContainer, public Observer
{
public:
	BaseManip();
	virtual ~BaseManip();
	MStatus setContext(BaseContext*);

	// Implement CustomManipContainer's abstract method
	virtual void drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status);
	// Implement Observer's abstract methods
	virtual void Update(const NotificationMsg&);

protected:
	// Pointer to the containing manipulator
	BaseContext* context;
};


#endif
