/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ScaleManip.h"
#include "ScaleContext.h"

#include <Log/MayaLog.h>
#include <Format/Format.h>
#include <BaseManip/CustomScaleManip.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MManipData.h>

MTypeId ScaleManip::typeId(0x80020);
MString ScaleManip::typeName("ScaleManip");

void* ScaleManip::creator()
{
	return(new ScaleManip);
}

/**
 * Used to connect the manip to a depNode.
 * @param depNode   a dependency node for the manip to attach to. It can be anything because
 * NONE OF IT'S ATTRIBUTES ARE MODIFIED. 
 * @return an MStatus<br><b>kSuccess</b> if the connection succeed<br><b>kFailure</b> otherwise
 */
MStatus ScaleManip::customConnectToDependNode(const MObject& node)
{
	MStatus stat;
 
	MFnDependencyNode nodeFn(node, &stat); MAssertStat(stat);
	MPlug nodeStatePlug = nodeFn.findPlug("nodeState", &stat); MAssertStat(stat);

	// Create a scale manip
	this->scaleManip = CustomScaleManip::create(this, &stat); MAssertStat(stat);

	this->addPlugToManipConversionCallback(this->scaleManip->scalePivotIndex(), 
		(manipToPlugConversionCallback)this->centerPtoMCB);
	this->addPlugToManipConversionCallback(this->scaleManip->scaleIndex(),
		(manipToPlugConversionCallback)this->scalePToMCB);
	this->addManipToPlugConversionCallback(nodeStatePlug, 
		(plugToManipConversionCallback)this->scaleMToPCB);

	return(stat);
}

/**
 * This function fetches the current center value from the context and returns it as a MVector
 */
MManipData ScaleManip::centerPtoMCB(unsigned index)
{
	MStatus stat;
	// MGlobal::displayInfo(MString("ScaleManip::centerPtoMCB: called");
 
	// Pass through the center position.
	MPoint center;
	// Sanity
	MAssertReturnObj(this->context != NULL, 0);
	stat = this->context->getCenter(center); MAssertStat(stat);
	// Always sent the center to the freepointManip.
	MFnNumericData numData;
	MObject numDataObj = numData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	stat = numData.setData(center.x, center.y, center.z); MAssertStat(stat);
	return(numDataObj);
}

MManipData ScaleManip::scalePToMCB(unsigned index)
{
	MStatus stat;
	// MTrace("ScaleManip::scalePToMCB: called.");

	MAssertReturnObj(this->context != NULL, 0);
	MVector rot = static_cast<ScaleContext*>(this->context)->getScale(&stat); MAssertStatReturnObj(stat, 0);

	MFnNumericData numFn;
	MObject numObj = numFn.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, 0);
	stat = numFn.setData(rot.x, rot.y, rot.z); MAssertStatReturnObj(stat, 0);
	return(numObj);
}

MManipData ScaleManip::scaleMToPCB(unsigned index)
{
	MStatus stat;
	// MTrace("ScaleManip::scaleMToPCB: called.");
	int plugVal;
	stat = this->getConvertorPlugValue(index, plugVal); MAssertStatReturnObj(stat, 0);

	// Get the manip's rotate value
	MVector val;
	stat = this->getConvertorManipValue(this->scaleManip->scaleIndex(), val); MAssertStatReturnObj(stat, 0);

	// Sanity
	MAssertReturnObj(this->context != NULL, 0);
	// Call the 
	stat = static_cast<ScaleContext*>(this->context)->setScale(val); MAssertStatReturnObj(stat, 0);

	return(plugVal);
}

void ScaleManip::onPress(const CustomEvent& e)
{
	// MTrace("ScaleManip::onPress: called");
	CustomManipContainer::onPress(e);

	// If no manip was selected, just return
	if(this->selectedManip != NULL)
	{ 
		// Sanity check	
		MAssertNoReturn(this->context != NULL);
		MStatus stat = this->context->doManipPress(); MAssertStatNoReturn(stat);
	}
}

void ScaleManip::onRelease(const CustomEvent& e)
{	
	// MTrace("ScaleManip::onRelease: called");
	if(this->selectedManip != NULL)
	{
		// Sanity check
		MAssertNoReturn(this->context != NULL);
		MStatus stat = this->context->doManipRelease(); MAssertStatNoReturn(stat); 
	}
	// Finally run the parent's release code to clear selectedManip in the end
	CustomManipContainer::onRelease(e);
}

