/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "RotateContext.h"
#include "RotateManip.h"

// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MDagPath.h>

#define HELPSTR	    "Drag manipulator to rotate the selected vertices."
#define TITLESTR	"ZooZ Rotate Tool"
#define CONTEXTNAME	    "RotateTool"
#define CONTEXTIMAGE	"rotate.xpm"

RotateContext::RotateContext() : BaseContext(CONTEXTNAME, CONTEXTIMAGE, TITLESTR, HELPSTR, RotateManip::typeName)
{
	MStatus stat;
}

MVector RotateContext::getRotation(MStatus* stat)
{
	MValidateStatPtr(stat);
	MVector vec = this->getModel()->getRotation(stat); MAssertStatReturnObj(*stat, vec);
	// MTrace("RotateContext::getRotation: rotation = " + Format::toString(vec));
	return(vec);
}

MStatus RotateContext::setRotation(const MVector& vec)
{
	MStatus stat;
	// Update the command
	stat = this->getModel()->undoIt(); MAssertStat(stat);
	stat = this->getModel()->setRotation(vec); MAssertStat(stat);
	stat = this->getModel()->redoIt(); MAssertStat(stat);

	return(MS::kSuccess);
}

bool RotateContext::isChangeLarge(MStatus* stat)
{
	MValidateStatPtr(stat); 
	double size = this->getModel()->getRotation(stat).length(); MAssertStatReturnObj(*stat, false);

	if(size >= 1e-4)
	{
		return(true);
	} else 
	{
		return(false);
	}
}

MStatus RotateContext::doCustomPress(MEvent& event)
{
	// MTrace("RotateContext::doCustomPress: called");	
	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// NOTE: do the initial rotation setup here...

	return(MS::kSuccess);
}
MStatus RotateContext::doCustomDrag(MEvent& event)
{
	// MTrace("RotateContext::doCustomDrag: called");
	MStatus stat;

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }   

	// NOTE: update the rotation here...

	return(MS::kSuccess);
}
MStatus RotateContext::doCustomRelease(MEvent& event)
{
	// MTrace("RotateContext::doCustomRelease: called");

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// Finalize the command
	// MStatus stat = this->finalizeCmd(); MAssertStat(stat);

	return(MS::kSuccess);
}

