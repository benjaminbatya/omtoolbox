/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "BaseManip.h"
#include "BaseContext.h"

#include <Log/MayaLog.h>

BaseManip::BaseManip()
{
	this->context = NULL;
}
BaseManip::~BaseManip()
{
	// deinitialization stuff
	if(this->context != NULL)
	{
		this->context->Detach(this);
	}
}

MStatus BaseManip::setContext(BaseContext* ctx)
{
	// MTrace("BaseManip::setContext: context = " + (unsigned)ctx);
	// First detach from the previous context
	if(this->context != NULL)
	{
		this->context->Detach(this);
	}
	
	// Set the context
	this->context = ctx;
	this->context->Attach(this);
	
	return(MS::kSuccess);
}

void BaseManip::drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{
	// Don't do anything 
}

void BaseManip::Update(const NotificationMsg& msg)
{
	// Don;t do anything...
}
