/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ScaleContext.h"
#include "ScaleManip.h"

// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MDagPath.h>

#define HELPSTR	    "Drag manipulator to scale the selected vertices."
#define TITLESTR	"ZooZ Scale Tool"
#define CONTEXTNAME	    "ScaleTool"
#define CONTEXTIMAGE	"scale.xpm"

ScaleContext::ScaleContext() : BaseContext(CONTEXTNAME, CONTEXTIMAGE, TITLESTR, HELPSTR, ScaleManip::typeName)
{
	MStatus stat;
}

MVector ScaleContext::getScale(MStatus* stat)
{
	MValidateStatPtr(stat);
	MVector vec = this->getModel()->getScale(stat); MAssertStatReturnObj(*stat, vec);
	return(vec);
}

MStatus ScaleContext::setScale(const MVector& delta)
{
	// MTrace("ScaleContext::updateCmd: called.");
	MStatus stat;
	// Update the command
	stat = this->getModel()->undoIt(); MAssertStat(stat);
	stat = this->getModel()->setScale(delta); MAssertStat(stat);
	stat = this->getModel()->redoIt(); MAssertStat(stat);

	return(stat);
}

bool ScaleContext::isChangeLarge(MStatus* stat)
{
	MValidateStatPtr(stat);
	double size = this->getModel()->getScale(stat).length(); MAssertStatReturnObj(*stat, false);

	if(size >= 1e-4)
	{
		return(true);
	} else
	{
		return(false);
	}
}

MStatus ScaleContext::doCustomPress(MEvent& event)
{
	// MTrace("ScaleContext::doCustomPress: called");	
	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// NOTE: do the initial  setup here...

	return(MS::kSuccess);
}
MStatus ScaleContext::doCustomDrag(MEvent& event)
{
	// MTrace("ScaleContext::doCustomDrag: called");
	MStatus stat;

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }   

	// NOTE: update the scaling here...

	return(MS::kSuccess);
}
MStatus ScaleContext::doCustomRelease(MEvent& event)
{
	// MTrace("ScaleContext::doCustomRelease: called");

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// Finalize the command
	// MStatus stat = this->finalizeCmd(); MAssertStat(stat);

	return(MS::kSuccess);
}

