/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ToolCmd.h"
#include "ToolNode.h"
#include "CmdFlags.h"
// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MArgList.h>
#include <maya/MArgParser.h>
#include <maya/M3dView.h>
#include <maya/MSelectionList.h>
#include <maya/MFloatVector.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MCommandResult.h>
#include <maya/MFnDagNode.h>
#include <maya/MPlugArray.h>
#include <maya/MDagModifier.h>

MString MoveToolCmd::cmdName("ZooZToolCmd");

MoveToolCmd::MoveToolCmd()
{
	this->setCommandString(this->cmdName); 
	this->deformerName.clear(); 
	this->model = new MoveToolCmdModel(MoveToolCmdModel::kContextSet);
	MAssertNoReturn(this->model != NULL);
}
MoveToolCmd::~MoveToolCmd()
{ 
	delete(this->model);
}

void* MoveToolCmd::creator()
{
	return(new MoveToolCmd);
}

// The type of transformation
// At least one of these flags must be specified
const charPtr kTranslate = "-t", kTranslateLong = "-translate";
const charPtr kScale = "-s", kScaleLong = "-scale";
const charPtr kRotate = "-r", kRotateLong = "-rotate";

// Define moveSpace flags
const charPtr kMoveSpace = "-ms", kMoveSpaceLong = "-moveSpace";
// Define falloff flags
const charPtr kFalloffType = "-ft", kFalloffTypeLong = "-falloffType";
const charPtr kFalloffInnerRadius = "-fir", kFalloffInnerRadiusLong = "-falloffInnerRadius";
const charPtr kFalloffOuterRadius = "-for", kFalloffOuterRadiusLong = "-falloffOuterRadius";
const charPtr kFalloffCurve = "-fc", kFalloffCurveLong = "-falloffCurve";
const charPtr kFalloffAxis = "-fa", kFalloffAxisLong = "-falloffAxis";
// Define mirror flags
const charPtr kMirrorType = "-mt", kMirrorTypeLong = "-mirrorType";
const charPtr kMirrorAxis = "-ma", kMirrorAxisLong = "-mirrorAxis";

MSyntax MoveToolCmd::newSyntax()
{
	MStatus stat;
	MSyntax syntax; 

	stat = syntax.addFlag(kTranslate, kTranslateLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kScale, kScaleLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kRotate, kRotateLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);

	stat = syntax.addFlag(kMoveSpace, kMoveSpaceLong, MSyntax::kUnsigned); MAssertStatReturnObj(stat, syntax);

	stat = syntax.addFlag(kFalloffType, kFalloffTypeLong, MSyntax::kUnsigned); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kFalloffInnerRadius, kFalloffInnerRadiusLong, MSyntax::kDistance); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kFalloffOuterRadius, kFalloffOuterRadiusLong, MSyntax::kDistance); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kFalloffCurve, kFalloffCurveLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kFalloffAxis, kFalloffAxisLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);
	
	// Mirror flags
	stat = syntax.addFlag(kMirrorType, kMirrorTypeLong, MSyntax::kUnsigned); MAssertStatReturnObj(stat, syntax);
	stat = syntax.addFlag(kMirrorAxis, kMirrorAxisLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStatReturnObj(stat, syntax);

	return(syntax);
}

MStatus MoveToolCmd::finalize()
{
	MStatus stat;
	MArgList command;
	stat = command.addArg(this->commandString()); MAssertStat(stat);
	// Add the translation if it's not zero
	MVector delta = this->model->getTranslation(&stat); MAssertStat(stat);
	if(!delta.isEquivalent(MVector::zero))
	{ 
		stat = command.addArg(MString(kTranslateLong)); MAssertStat(stat);
		stat = command.addArg(delta); MAssertStat(stat);
	}
	// Add the rotation if it's not zero
	delta = this->model->getRotation(&stat); MAssertStat(stat);
	if(!delta.isEquivalent(MVector::zero))
	{ 
		stat = command.addArg(MString(kRotateLong)); MAssertStat(stat);
		stat = command.addArg(delta); MAssertStat(stat);
	}
	// Add the scaling if it's not zero
	delta = this->model->getScale(&stat); MAssertStat(stat);
	if(!delta.isEquivalent(MVector::one))
	{ 
		stat = command.addArg(MString(kScaleLong)); MAssertStat(stat);
		stat = command.addArg(delta); MAssertStat(stat);
	}

	// Ignore the result of this, seems to always return kFailure
	MPxToolCommand::doFinalize(command);
	return(stat);
}

const MString usageStr = MString("Usage: ") + MoveToolCmd::cmdName + " [transform option]+ [falloff option]* [mirror option]*\n" +
"where at least one of the transform options is required:\n" +
"\t" + kTranslateLong + "/" + kTranslate + " t0 t1 t2: translate amount.\n" +
"\t" + kScaleLong + "/" + kScale + " s0 s1 s2: scale amount.\n" +
"\t" + kRotateLong + "/" + kRotate + " r0 r1 r2: rotate amount.\n" +
"The falloff options are:\n" +
"\t" + kFalloffTypeLong + "/" + kFalloffType + " type: type can be " + 
MoveToolCmdModel::kNone + "(None), " + MoveToolCmdModel::kSpherical + "(Spherical), " +
MoveToolCmdModel::kLinearX + "(LinearX), " + MoveToolCmdModel::kLinearY + "(LinearY), " + MoveToolCmdModel::kLinearZ + "(LinearZ), " + MoveToolCmdModel::kLinearView + "(LinearView), " + MoveToolCmdModel::kLinearCustom + "(LinearCustom), " + 
MoveToolCmdModel::kCylinderX + "(CylinderX), " + MoveToolCmdModel::kCylinderY + "(CylinderY), " + MoveToolCmdModel::kCylinderZ + "(CylinderZ), " + MoveToolCmdModel::kCylinderView + "(CylinderView), " + MoveToolCmdModel::kCylinderCustom + "(CylinderCustom), " + 
MoveToolCmdModel::kTopology + "(Topology).\n" +
"\t" + kFalloffInnerRadiusLong + "/" + kFalloffInnerRadius + " radius: radius is the size of the inner falloff.\n" +
"\t" + kFalloffOuterRadiusLong + "/" + kFalloffOuterRadius + " radius: radius is the size of the outer falloff.\n" +
"\t" + kFalloffCurveLong + "/" + kFalloffCurve + " f0 f1 f2 f3: f[0..3] are floats which are the CVs of Bezier curve.\n" +
"\t" + kFalloffAxisLong + "/" + kFalloffAxis + " x y z: Direction vector for falloff axis. (only used for LinearCustom and CylinderCustom)\n" +
"The mirror options are:\n" +
"\t" + kMirrorTypeLong + "/" + kMirrorType + " type: type can be " +
MoveToolCmdModel::kMirrorNone + "(kMirrorNone), " + 
MoveToolCmdModel::kMirrorX + "(kMirrorX), " + MoveToolCmdModel::kMirrorY + "(kMirrorY), " + MoveToolCmdModel::kMirrorZ + "(kMirrorZ), " +
MoveToolCmdModel::kMirrorView + "(kMirrorView), " + MoveToolCmdModel::kMirrorCustom + "(kMirrorCustom).\n" +
"\t" + kMirrorAxisLong + "/" + kMirrorAxis + " x y z: Direction vector for the mirror axis. (only used when mirror type is kMirrorCustom)\n" +
"";


#define MFlagCheck(stat) if((stat) != MS::kSuccess) { MWarn(usageStr); return((stat)); }

MStatus MoveToolCmd::doIt(const MArgList& args)
{ 
	MStatus stat;
	
	// Validate the current selection
	// Check that there is only one(1) selected object.
	MSelectionList sList;
	stat = MGlobal::getActiveSelectionList(sList); MAssertStat(stat);
	// Check that there is only one object selected
	unsigned length = sList.length(&stat); MAssertStat(stat);
	if(length != 1)
	{
		MError(this->commandString() + ": Only one(1) object can be selected to move at a time.");
		return(MS::kFailure);
	} 
	// Check that a component is selected
	MDagPath dagPath;
	MObject component;
	stat = sList.getDagPath(0, dagPath, component); MAssertStat(stat);
	if(component == MObject::kNullObj)
	{
		MError(this->commandString() + ": Components must be selected, not the transform node.");
		return(MS::kFailure);
	}

	// Parse the arguments 
	MArgParser parser(this->syntax(), args, &stat); MFlagCheck(stat);
  
	bool transformSpecified = false;

	// check for transform args. If least one must be specified
	bool flagSet = parser.isFlagSet(kTranslate, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector val;
		stat = parser.getFlagArgument(kTranslate, 0, val[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kTranslate, 1, val[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kTranslate, 2, val[2]); MFlagCheck(stat);
		stat = this->model->setTranslation(val); MAssertStat(stat);
		transformSpecified = true;
	} 
	flagSet = parser.isFlagSet(kScale, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector val;
		stat = parser.getFlagArgument(kScale, 0, val[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kScale, 1, val[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kScale, 2, val[2]); MFlagCheck(stat);
		stat = this->model->setScale(val); MAssertStat(stat);
		transformSpecified = true;
	}
	flagSet = parser.isFlagSet(kRotate, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector val;
		stat = parser.getFlagArgument(kRotate, 0, val[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kRotate, 1, val[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kRotate, 2, val[2]); MFlagCheck(stat);
		stat = this->model->setRotation(val); MAssertStat(stat);
		transformSpecified = true;
	} 
	if(!transformSpecified)
	{
		MFlagCheck(MS::kInvalidParameter);
	}

	// Check the falloffType
	flagSet = parser.isFlagSet(kFalloffType, &stat); MAssertStat(stat);
	if(flagSet)
	{
		unsigned temp;
		stat = parser.getFlagArgument(kFalloffType, 0, temp); MFlagCheck(stat);
		stat = this->model->setFalloffType((MoveToolCmdModel::FalloffType)temp); MAssertStat(stat);
	}

	// Check innner radius
	flagSet = parser.isFlagSet(kFalloffInnerRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MDistance dist;
		stat = parser.getFlagArgument(kFalloffInnerRadius, 0, dist); MFlagCheck(stat);
		stat = this->model->setFalloffInnerRadius(dist); MAssertStat(stat);
	}
	// Check the outerDistance
	flagSet = parser.isFlagSet(kFalloffOuterRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MDistance dist;
		stat = parser.getFlagArgument(kFalloffOuterRadius, 0, dist); MFlagCheck(stat);
		stat = this->model->setFalloffOuterRadius(dist); MAssertStat(stat);
	}
	// Check the falloff curve
	flagSet = parser.isFlagSet(kFalloffCurve, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MDoubleArray curve(4);
		stat = parser.getFlagArgument(kFalloffCurve, 0, curve[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kFalloffCurve, 1, curve[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kFalloffCurve, 2, curve[2]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kFalloffCurve, 3, curve[3]); MFlagCheck(stat);
		stat = this->model->setFalloffCurve(curve); MAssertStat(stat);
	}
	
	// Check the falloff axis flags
	flagSet = parser.isFlagSet(kFalloffAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector axis;
		stat = parser.getFlagArgument(kFalloffAxis, 0, axis[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kFalloffAxis, 1, axis[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kFalloffAxis, 2, axis[2]); MFlagCheck(stat);
		stat = this->model->setFalloffAxis(axis); MAssertStat(stat);
	}

	// Check the mirror type flag
	flagSet = parser.isFlagSet(kMirrorType, &stat); MAssertStat(stat);
	if(flagSet)
   	{
		unsigned temp;
		stat = parser.getFlagArgument(kMirrorType, 0, temp); MFlagCheck(stat);
		stat = this->model->setMirrorType((MoveToolCmdModel::MirrorType)temp); MAssertStat(stat);
	}
	// Check the mirror axis flags
	flagSet = parser.isFlagSet(kMirrorAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector axis;
		stat = parser.getFlagArgument(kMirrorAxis, 0, axis[0]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kMirrorAxis, 1, axis[1]); MFlagCheck(stat);
		stat = parser.getFlagArgument(kMirrorAxis, 2, axis[2]); MFlagCheck(stat);
		stat = this->model->setMirrorAxis(axis); MAssertStat(stat);
	}

	// finally run redo...
	return(this->redoIt());
}

MStatus MoveToolCmd::redoIt()
{
	MStatus stat;
    MCommandResult res; 

   	MSelectionList sList;
	stat = MGlobal::getActiveSelectionList(sList); MAssertStat(stat);
	// MTrace("Selection length = " + sList.length()); 
	MStringArray comps;
	stat = sList.getSelectionStrings(comps); MAssertStat(stat);

    // Create the deformer
    MString cmd = "deformer -type " + MoveToolNode::typeName + " -is";
    stat = MGlobal::executeCommand(cmd, res, false); MAssertStat(stat);
    // MTrace(MString("command result = ") + res.resultType(&stat)); MAssertStat(stat);
    MStringArray names;
    stat = res.getResult(names); MAssertStat(stat);
    // Sanity checking
    MAssert(names.length() == 1);
    this->deformerName = names[0];
    // MTrace(MString("SoftSelectToolCmd::redoIt: deformer node is ") + this->deformerName);

    // Add the required geometry to the deformer
    for(unsigned i=0; i<comps.length(); i++)
    {
		cmd = "deformer -e -g " + comps[i] + " " + this->deformerName;
		stat = MGlobal::executeCommand(cmd, false); MAssertStat(stat);
    }

	// Initialize the parameters of the deformer
	// 1) Get a handle to the deformer node
	stat = sList.clear(); MAssertStat(stat);
	stat = sList.add(this->deformerName); MAssertStat(stat);
	MObject nodeObj;
	stat = sList.getDependNode(0, nodeObj); MAssertStat(stat);
	MFnDependencyNode depFn(nodeObj, &stat); MAssertStat(stat);

	// 2) Get each of the attribute plugs and set them
	// Set the transformation plugs
	MPlug plug = depFn.findPlug("translation", &stat); MAssertStat(stat);
	MVector vec = this->model->getTranslation(&stat); MAssertStat(stat);
	MFnNumericData numerData;
	MObject transformObj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	// MTrace("Setting vector " + Format::toString(tVec));
	stat = numerData.setData(vec.x, vec.y, vec.z); MAssertStat(stat);
	stat = plug.setValue(transformObj); MAssertStat(stat);

	plug = depFn.findPlug("rotation", &stat); MAssertStat(stat);
	vec = this->model->getRotation(&stat); MAssertStat(stat);
	MObject rotateObj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	stat = numerData.setData(vec.x, vec.y, vec.z); MAssertStat(stat);
	stat = plug.setValue(rotateObj); MAssertStat(stat);

	plug = depFn.findPlug("scale", &stat); MAssertStat(stat);
	vec = this->model->getScale(&stat); MAssertStat(stat);
	MObject scaleObj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	stat = numerData.setData(vec.x, vec.y, vec.z); MAssertStat(stat);
	stat = plug.setValue(scaleObj); MAssertStat(stat);

	// Set the falloff plugs 
	plug = depFn.findPlug("falloffType", &stat); MAssertStat(stat);
	stat = plug.setValue(this->model->getFalloffType()); MAssertStat(stat);

	plug = depFn.findPlug("falloffAxis", &stat); MAssertStat(stat);
	MVector axis = this->model->getFalloffAxis(&stat); MAssertStat(stat);
	MObject axisObj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	stat = numerData.setData(axis.x, axis.y, axis.z); MAssertStat(stat);
	stat = plug.setValue(axisObj); MAssertStat(stat);
	
	plug = depFn.findPlug("falloffInnerRadius", &stat); MAssertStat(stat);
	stat = plug.setValue(this->model->getFalloffInnerRadius()); MAssertStat(stat);

	plug = depFn.findPlug("falloffOuterRadius", &stat); MAssertStat(stat);
	stat = plug.setValue(this->model->getFalloffOuterRadius()); MAssertStat(stat);

	plug = depFn.findPlug("falloffCurve", &stat); MAssertStat(stat);
	MDoubleArray curve = this->model->getFalloffCurve();
	MFnDoubleArrayData curveFn;
	MObject curveObj = curveFn.create(curve, &stat); MAssertStat(stat);
	stat = plug.setValue(curveObj); MAssertStat(stat);

	// Set the mirror plugs 
	plug = depFn.findPlug("mirrorType", &stat); MAssertStat(stat); 
	stat = plug.setValue(this->model->getMirrorType()); MAssertStat(stat);

	plug = depFn.findPlug("mirrorAxis", &stat); MAssertStat(stat);
	axis = this->model->getMirrorAxis(&stat); MAssertStat(stat);
	MObject mirrorAxisObj = numerData.create(MFnNumericData::k3Double, &stat); MAssertStat(stat);
	stat = numerData.setData(axis.x, axis.y, axis.z); MAssertStat(stat);
	stat = plug.setValue(mirrorAxisObj); MAssertStat(stat);

	// Force the UI to update...
	MGlobal::executeCommand("scriptJob -runOnce true -event \"idle\" \"OMT_ZT_RefreshUI\"");

	return(MS::kSuccess);
}

MStatus MoveToolCmd::undoIt()
{	
	MTrace("MoveToolCmd::undoIt: called.");
	// If the deformer name is empty, immediately return
	if(this->deformerName.length() == 0)
// 		 ||
// 	   !this->selectedPath.isValid())
	{
		return(MS::kFailure);
	}

    // Delete the deformer
    MString cmd = MString("delete ") + this->deformerName;
	MStatus stat = MGlobal::executeCommand(cmd); MAssertStat(stat);

/*  Not using this code because it doesn't work when there's no history. So I'm leaving this
commented out until someone complains and it needs to be changed.
   
	// Find the connected node with name = this->selectedPath.name() + "Orig" and same type name 
	// and delete it.
	MObject selectedNode = this->selectedPath.node(&stat); MAssertStat(stat);
	MFnDependencyNode selectedFn(selectedNode, &stat); MAssertStat(stat);
	MString selectedName = selectedFn.name(&stat); MAssertStat(stat);
	MString selectedType = selectedFn.typeName(&stat); MAssertStat(stat);
	// MTrace("Selected node name = " + selectedName + ", type = " + selectedType);
 
	MPlugArray plugArray;
	stat = selectedFn.getConnections(plugArray); MAssertStat(stat);
	for(unsigned i=0; i<plugArray.length(); i++)
	{
		// Get the connected plug
		MPlugArray otherPlugs;
		// The plug needs to be	a destination only and connected to only one plug
		bool connected = plugArray[i].connectedTo(otherPlugs, true, false, &stat); MAssertStat(stat);
		if(connected && otherPlugs.length() == 1)
		{   
			MObject connectedNode = otherPlugs[0].node(&stat); MAssertStat(stat);
			MFnDependencyNode connectedFn(connectedNode, &stat); MAssertStat(stat);
			MString connectedName = connectedFn.name(&stat); MAssertStat(stat);
			MString connectedType = connectedFn.typeName(&stat); MAssertStat(stat);
			// MTrace("Connected node name = " + connectedName + ", type = " + connectedType);
			if(connectedType == selectedType &&
			   connectedName == (selectedName + "Orig"))
			{
				// MTrace("Deleting node " + connectedName);
				stat = MGlobal::deleteNode(connectedNode); MAssertStat(stat);
				break;
			}
		}
	}
*/
	return(MS::kSuccess);
}

