/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "CatmullRomCurve.h"

#include <Log/MayaLog.h>
#include <math.h>

CatmullRomCurve::CatmullRomCurve(const MDoubleArray& array)
{
	MStatus stat = this->array.copy(array); MAssertStatNoReturn(stat);
	// Double the end points
	stat = this->array.insert(array[0], 0); MAssertStatNoReturn(stat);
	stat = this->array.append(array[array.length()-1]); MAssertStatNoReturn(stat);
	this->maxT = this->array.length() - 3;
}

// Calculate the falloff curve of the catmullrom curve
// Stolen from Sergio's code 
double CatmullRomCurve::calcT(double t)
{
	// map t to the correct span
	int spanStart;
	double spanT;

	if(t <= 0.0)
	{
		return(this->array[0]);
	} else if(t >= 1.0)
	{
		return(this->array[this->array.length()-1]);
	}
		
	double extendedT = t * this->maxT;
	spanStart = (int)floor(extendedT);
	spanT = extendedT - spanStart; 

	double p0 = this->array[spanStart];
	double p1 = this->array[spanStart + 1];
	double p2 = this->array[spanStart + 2];
	double p3 = this->array[spanStart + 3];

	return(0.5 * ( (2.0 * p1) +
				  ((-p0 + p2) * spanT) +
				  ((2.0*p0 - 5.0*p1 + 4.0*p2 - p3) * pow(spanT,2.0)) +
				  ((-p0 + 3.0*p1 - 3.0*p2 + p3) * pow(spanT,3.0) )));
}
