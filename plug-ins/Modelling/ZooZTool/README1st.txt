ZooZTool installation:
1) Close Maya if it is running.
2) Copy plug-ins/ZooZTool.mll -> My Documents/maya/6.x/plug-ins/
3) Copy scripts/*.* -> My Documents/maya/6.x/scripts/
4) Copy icons/*.* -> My Documents/maya/6.x/icons/
5) Start Maya.
6) Click on Window -> Settings/Preferences -> Plug-in Manager and
click on the ZooZTool.mll loaded checkbox. The ZooZTool Tools window 
should be displayed with the Translate, Rotate, and Scale Tools and the
Modifier List.

Help with the tools and modifier list can be obtained at:
scripts/ZooZTool.html

Project and files are hosted at:
https://sourceforge.net/projects/omtoolbox/

Cheers,
Ben Schleimer
bensch128@yahoo.com
