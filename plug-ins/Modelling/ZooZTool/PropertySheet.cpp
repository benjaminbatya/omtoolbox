/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "PropertySheet.h"
#include "BaseContext.h"

#include <Log/MayaLog.h>

#include <maya/MToolsInfo.h>

PropertySheetView::PropertySheetView()
{
	this->context = NULL;
}
PropertySheetView::~PropertySheetView() { }

MStatus PropertySheetView::setContext(BaseContext* context)
{
	this->context = context;
	this->context->Attach(this);
	return(MS::kSuccess);
}

void PropertySheetView::Update(const NotificationMsg& msg)
{
	// MTrace("PropertySheetView::Update: msg = " + msg);

	MStatus stat;
	M3dView view;
	switch(msg)
	{
	case(MSG_FALLOFF_OUTERRADIUS):
	case(MSG_FALLOFF_INNERRADIUS):
	case(MSG_FALLOFF_TYPE):
	case(MSG_FALLOFF_AXIS):
	case(MSG_MIRROR_TYPE):
	case(MSG_MIRROR_AXIS):
		// Sanity
		MAssertNoReturn(this->context != NULL);
		// Refresh the property sheet
		MToolsInfo::setDirtyFlag(*(this->context));
		// Fall through...
	case(MSG_FALLOFF_CURVE):
	case(MSG_FALLOFF_INVERT):
	case(MSG_CENTER):
		// Refresh the 3D view
		view = M3dView::active3dView(&stat); MAssertStatNoReturn(stat);
		stat = view.refresh(true, true); MAssertStatNoReturn(stat);  
		break;
	default:
		break;
	}
}

