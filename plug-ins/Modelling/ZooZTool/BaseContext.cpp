/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "BaseContext.h"
#include "ToolCmd.h"
#include "FalloffManip.h"
#include "MirrorManip.h"
#include "PropertySheet.h"

// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MEventMessage.h>
#include <maya/MUserEventMessage.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>

BaseContext::BaseContext(const MString& name, const MString& image,
		const MString& titleStr, const MString& helpStr, const MString& toolName)
{
	MStatus stat;
	MTrace("BaseContext::BaseContext: New context created.");
	
	this->name = name;
	this->image = image;
	this->titleStr = titleStr;
	this->helpStr = helpStr;
	this->toolName = toolName;

	// Set the icon
	stat = this->setImage(this->image, MPxContext::kImage1); MAssertStatNoReturn(stat);

	// Setup the model
	this->model = new MoveToolCmdModel(MoveToolCmdModel::kContextSet);
	MAssertNoReturn(this->model != NULL);
	// Make this observe cmdModel's notifications and it'll forward them to all of it's observers
	this->model->Attach(this);

	this->displayFalloffManip = false;
	this->displayMirrorManip = false;

	// Set the propertysheet context to this
	this->propertySheetView = new PropertySheetView;
	stat = this->propertySheetView->setContext(this); MAssertStatNoReturn(stat);
} 

BaseContext::~BaseContext()
{
	delete(this->propertySheetView);
	delete(this->model);
}

void BaseContext::getClassName(MString& name) const
{
	// MTrace("MoveToolContext::getClassName: called.");
	name = this->name;
}
MStatus BaseContext::doEnterRegion(MEvent& event)
{ 
	MStatus stat = CustomSelectionContext::doEnterRegion(event); MAssertStat(stat);
	stat = this->setHelpString(this->helpStr); MAssertStat(stat);
	return(stat);
}
void BaseContext::Update(const NotificationMsg& msg)
{
	// Forward all notification messages from the model to our listeners
	this->Notify(msg);
}

// Turn the callbacks into event methods
void selectionChanged(void* data) { ((BaseContext*)data)->doSelectionChanged(); }
void viewDirty(void* data) { ((BaseContext*)data)->doViewDirty(); }
void BaseContext::toolOnSetup(MEvent& event)
{
	// MTrace("BaseContext::toolOnSetup: called");
	CustomSelectionContext::toolOnSetup(event); 

	MStatus stat = this->setHelpString(this->helpStr); MAssertStatNoReturn(stat);
	stat = this->setTitleString(this->titleStr); MAssertStatNoReturn(stat);

	// Add a callback for SelectionChanged event
	stat = this->callbackIDs.clear(); MAssertStatNoReturn(stat);
	int id = MEventMessage::addEventCallback("SelectionChanged", selectionChanged, this, &stat); MAssertStatNoReturn(stat);
	stat = this->callbackIDs.append(id); MAssertStatNoReturn(stat);
	
	// Add the selectionChanged callback for the undo event so the manip is always in a good place
	id = MEventMessage::addEventCallback("Undo", selectionChanged, this, &stat); MAssertStatNoReturn(stat);
	stat = this->callbackIDs.append(id); MAssertStatNoReturn(stat);

	// Add the selectionChanged callback for the redo event so the manip is always in a good place
	id = MEventMessage::addEventCallback("Redo", selectionChanged, this, &stat); MAssertStatNoReturn(stat);
	stat = this->callbackIDs.append(id); MAssertStatNoReturn(stat);
	
	// Add a callback for viewDirty event which the CustomSelectionContext throws
	id = MUserEventMessage::addUserEventCallback(CustomManipContainer::ViewChangedEvent, viewDirty, this, &stat); MAssertStatNoReturn(stat);
	stat = this->callbackIDs.append(id); MAssertStatNoReturn(stat);

	// Immediately run the selection changed code
	stat = this->doSelectionChanged(); MAssertStatNoReturn(stat);
}

void BaseContext::toolOffCleanup()
{
	// MTrace("BaseContext::toolOffCleanup: called.");
	// Run the superclass's offCleanup method
	CustomSelectionContext::toolOffCleanup();

	// Remove all of the callbacks at once
	MStatus stat = MMessage::removeCallbacks(this->callbackIDs); MAssertStatNoReturn(stat);

	// Remove all of the currently active manipulators
	stat = this->deleteManipulators(); MAssertStatNoReturn(stat);

	// stat = this->finalizeCmd(); MAssertStatNoReturn(stat);
}

MStatus BaseContext::deleteManipulators()
{
	// MTrace("BaseContext::deleteManipulators: called.");
	MStatus stat = CustomSelectionContext::deleteManipulators(); MAssertStat(stat);

	return(stat);
}

MStatus BaseContext::addManipulators(const MObject& depNode)
{
	MTrace("BaseContext::addManipulators: toolName = " + this->toolName);
	MStatus stat;
	MObject manipObj;
	BaseManip* manipPtr = (BaseManip*)BaseManip::newManipulator(this->toolName, manipObj, &stat); MAssertStat(stat);  
	MAssert(manipPtr != NULL);
	// First set the context
	stat = manipPtr->setContext(this); MAssertStat(stat); 	
	stat = this->addManipulator(manipObj); MAssertStat(stat);
	stat = manipPtr->connectToDependNode(depNode); MAssertStat(stat);

	if(this->displayFalloffManip)
	{ 
		// Add a falloff manipulator
		MObject falloffManipObj;
		FalloffManip* falloffManip = (FalloffManip*)FalloffManip::newManipulator(FalloffManip::typeName, falloffManipObj, &stat); MAssertStat(stat);
		MAssert(falloffManip != NULL);
		stat = falloffManip->setContext(this); MAssertStat(stat); 
		// Add the falloff manip to this context
		stat = this->addManipulator(falloffManipObj); MAssertStat(stat);
		// Initialize it
		stat = falloffManip->connectToDependNode(depNode); MAssertStat(stat);
	}
 
	if(this->displayMirrorManip)
	{
		// Add a mirror manipulator
		MObject mirrorManipObj;
		MirrorManip* mirrorManip = (MirrorManip*)MirrorManip::newManipulator(MirrorManip::typeName, mirrorManipObj, &stat); MAssertStat(stat);
		MAssert(mirrorManip != NULL);
		stat = mirrorManip->setContext(this); MAssertStat(stat);
		// add the mirror manip to the context
		stat = this->addManipulator(mirrorManipObj); MAssertStat(stat);
		// Initialize it
		stat = mirrorManip->connectToDependNode(depNode); MAssertStat(stat);
	}

	return(stat);
}

MStatus BaseContext::finalizeCmd()
{
	MTrace("BaseContext::finalizeCmd: called.");
	MStatus stat;

	// Don't put the command into the undo queue if the change is small
	bool changed = this->isChangeLarge(&stat); MAssertStat(stat);
	// MTrace("BaseContext::finalizeCmd: changed = " + changed); 
	if(changed)
	{    
		// Undo the command model here...
		stat = this->model->undoIt(); MAssertStat(stat);

		// Finalize the command
		MoveToolCmd* tempCmd = (MoveToolCmd*)this->newToolCommand();
		MAssert(tempCmd != NULL);
		*tempCmd->model = *this->model;
		stat = tempCmd->redoIt(); MAssertStat(stat);
		stat = tempCmd->finalize(); MAssertStat(stat);
	}
	
	stat = this->model->reset(); MAssertStat(stat);

	return(MS::kSuccess);
}

MStatus BaseContext::calcPlaneLineIntersection(const MPoint& rayStart, const MVector& rayDirection,
						 const MPoint& pos, const MVector& normal,
						 MPoint& retVal)
{
    // Determine the point of intersection between the line and plane
    // Solution from http://astronomy.swin.edu.au/~pbourke/geometry/planeline/
    double denom = normal * rayDirection;
    // Check if denom is really small
    if((denom*denom) < 0.00001)
    {
		return(MS::kFailure);
    }

    double numer = normal * (pos - rayStart);
    // MTrace(MString("calcPlaneLineIntersection: denom = ") + denom);
    // MTrace(MString("calcPlaneLineIntersection: numer = ") + numer);
    double u = numer / denom;
    
    retVal = rayStart + (rayDirection * u);
    return(MS::kSuccess);
}

MStatus BaseContext::doSelectionChanged()
{ 
	// MTrace("BaseContext::doSelectionChanged: called.");
	// Delete any current manipulator
	MStatus stat = this->deleteManipulators(); MAssertStat(stat);

	// Validate the current selection
	// Check that there is only one(1) selected object.
	MSelectionList sList;
	stat = MGlobal::getActiveSelectionList(sList); MAssertStat(stat);
	// Check that there is only one object selected
	unsigned length = sList.length(&stat); MAssertStat(stat);
	if(length != 1)
	{
		MError("BaseContext::doSelectionChanged: Only one(1) object can be selected to move at a time.");
		return(MS::kFailure);
	} 
	// Make sure it's a correct type
	MObject nodeObj;
	stat = sList.getDependNode(0, nodeObj); MAssertStat(stat);
	if(!nodeObj.hasFn(MFn::kDagNode))
	{
		MError("BaseContext::doSelectionChanged: a Dag node must be selected.");
		return(MS::kFailure);
	}

	// Check that component(s) are selected
	MDagPath dagPath;
	MObject component; 
	stat = sList.getDagPath(0, dagPath, component); MAssertStat(stat);
	if(component == MObject::kNullObj)
	{
		MError("BaseContext::doSelectionChanged: Components must be selected, not the transform node.");
		return(MS::kFailure);
	}
	
	// NOTE:: maybe extend the dagpath??
	
	// Set the shape node
	stat = this->model->setContextGeom(dagPath, component); MAssertStat(stat); 
	// Rebuild the model
	stat = this->model->reset(); MAssertStat(stat);

	// Add the new manip
	MObject depNode = dagPath.node(&stat); MAssertStat(stat);
	stat = this->addManipulators(depNode); MAssertStat(stat);  

	return(MS::kSuccess);
}
MStatus BaseContext::doViewDirty()
{
	// MTrace("BaseContext::doViewDirty: called.");
	MStatus stat = this->model->setViewChanged(); MAssertStat(stat);
	return(MS::kSuccess);
}

MoveToolCmdModel* BaseContext::getModel()
{
	return(this->model);
}

MStatus BaseContext::getCenter(MPoint& center)
{
	MStatus stat;
	center = this->model->getCenterPoint(&stat); MAssertStat(stat);
	// MTrace("BaseContext::getCenter: center = " + Format::toString(center));
	return(MS::kSuccess);
}

MStatus BaseContext::doManipPress()
{ 
	return(MS::kSuccess);
}
MStatus BaseContext::doManipRelease()
{
	// MTrace("MoveToolContext::doManipRelease: called"); 
	// Finalize the command
	MStatus stat = this->finalizeCmd(); MAssertStat(stat);

	// notify that the center is changed to the screen reflects the final state.
	this->Notify(MSG_CENTER);

	return(MS::kSuccess);
}

MStatus BaseContext::setDisplayFalloffManip(const bool flag)
{
	this->displayFalloffManip = flag;
	// Update the selection and change the manipulators
	MStatus stat = this->doSelectionChanged(); MAssertStat(stat);
	return(MS::kSuccess);
}
bool BaseContext::getDisplayFalloffManip() const
{
	return(this->displayFalloffManip);
}

MStatus BaseContext::setDisplayMirrorManip(const bool flag)
{
	this->displayMirrorManip = flag;
	// Update the selection and change the manipulators
	MStatus stat = this->doSelectionChanged(); MAssertStat(stat);
	return(MS::kSuccess);
}
bool BaseContext::getDisplayMirrorManip() const
{
	return(this->displayMirrorManip);
}
