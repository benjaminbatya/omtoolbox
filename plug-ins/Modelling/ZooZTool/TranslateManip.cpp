/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "TranslateManip.h"
#include "TranslateContext.h"

#include <Log/MayaLog.h>
#include <Format/Format.h>
#include <BaseManip/FreeTriadPointManip.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnFreePointTriadManip.h>
#include <maya/MManipData.h>

MTypeId TranslateManip::typeId(0x8001d);
MString TranslateManip::typeName("TranslateManip");

void* TranslateManip::creator()
{
	return(new TranslateManip);
}

/**
 * Used to connect the manip to a depNode.
 * @param depNode   a dependency node for the manip to attach to. It can be anything because
 * NONE OF IT'S ATTRIBUTES ARE MODIFIED. 
 * @return an MStatus<br><b>kSuccess</b> if the connection succeed<br><b>kFailure</b> otherwise
 */
MStatus TranslateManip::customConnectToDependNode(const MObject& node)
{
	MStatus stat;
 
	MFnDependencyNode nodeFn(node, &stat); MAssertStat(stat);
	MPlug nodeStatePlug = nodeFn.findPlug("nodeState", &stat); MAssertStat(stat);

	// Create a Move manip
	this->moveManip = FreeTriadPointManip::create(this, &stat); MAssertStat(stat);

	this->addPlugToManipConversionCallback(this->moveManip->pointIndex(), 
		(manipToPlugConversionCallback)this->centerPtoMCB);
	this->addManipToPlugConversionCallback(nodeStatePlug, 
		(plugToManipConversionCallback)this->centerMtoPCB);

	return(stat);
}

void TranslateManip::Update(const NotificationMsg& msg)
{
	// MTrace("TranslateManip::Update: called with msg " + msg);
	switch(msg)
	{
	case(MSG_DRAW_ARROW_HEADS):
		// Sanity
		MAssertNoReturn(this->context != NULL);
		this->moveManip->setDrawArrowHead(((TranslateContext*)this->context)->getDrawArrowHead());
		break;
	default:
		// Ignore the message
		break;
	}
}

/**
 * This function fetches the current center value from the context and returns it as a MVector
 */
MManipData TranslateManip::centerPtoMCB(unsigned index)
{
	MStatus stat;
 
	// Pass through the center position.
	MPoint center;
	// Sanity
	MAssertReturnObj(this->context != NULL, 0);
	stat = this->context->getCenter(center); MAssertStatReturnObj(stat, 0);
	// MTrace("TranslateManip::centerPtoMCB: center = " + Format::toString(center));

	MVector vec = static_cast<TranslateContext*>(this->context)->getTranslation(&stat); MAssertStatReturnObj(stat, 0);
	center += vec; 
	// MTrace("TranslateManip::centerPtoMCB: transVec = " + Format::toString(vec));
	
	// Always sent the center to the freepointManip.
	MFnNumericData numData;
	MObject numDataObj = numData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, 0);
	stat = numData.setData(center.x, center.y, center.z); MAssertStatReturnObj(stat, 0);
	return(numDataObj);
}

/**
 * This tells the context the current position of the manip.
 * It has to get the plug value of the node and return it so
 * it is unaffected by the updating. I consider this a bug with Maya's manipulator framework
 * because this is a hack. I don't want to have to rewrite the manipulator framework again.
 */
MManipData TranslateManip::centerMtoPCB(unsigned index)
{
	// MInform("TranslateManip::centerMtoPCB: called.");

	// Get the plug value and return it...
	int plugValue;
	MStatus stat = this->getConvertorPlugValue(index, plugValue); MAssertStatReturnObj(stat, 0);
	// MInform("TranslateManip::centerMtoPCB: plugValue = " + plugValue);

	// Get the manip's new position
	MPoint center, oldPos;
	stat = this->getConvertorManipValue(this->moveManip->pointIndex(), center); MAssertStatReturnObj(stat, 0);

	// Sanity check
	MAssert(this->context != NULL);
	// Calculate the delta
	stat = this->context->getCenter(oldPos); MAssertStatReturnObj(stat, 0);
	// MTrace("TranslateManip::centerMtoPCB: centerMoved = " + Format::toString(center));

	MVector vec = center - oldPos;
	//MTrace("TranslateManip::centerMtoPCB: called, translation = " + Format::toString(vec));
	// Call the setCenterPt method with the 
	stat = static_cast<TranslateContext*>(this->context)->setTranslation(vec); MAssertStatReturnObj(stat, 0);

	return(plugValue);
}

void TranslateManip::onPress(const CustomEvent& e)
{
	// MTrace("TranslateManip::onPress: called");
	CustomManipContainer::onPress(e);

	// If no manip was selected, just return
	if(this->selectedManip != NULL)
	{ 
		// Sanity check	
		MAssertNoReturn(this->context != NULL);
		MStatus stat = this->context->doManipPress(); MAssertStatNoReturn(stat);
	}
}

void TranslateManip::onRelease(const CustomEvent& e)
{	
	// MTrace("TranslateManip::onRelease: called");
	if(this->selectedManip != NULL)
	{
		// Sanity check
		MAssertNoReturn(this->context != NULL);
		MStatus stat = this->context->doManipRelease(); MAssertStatNoReturn(stat); 
	}
	// Finally run the parent's release code to clear selectedManip in the end
	CustomManipContainer::onRelease(e);
}

