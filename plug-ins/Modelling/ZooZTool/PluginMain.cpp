/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

/*******************************************************
 * PluginMain.cpp - Sets up and tear downs the ZooZ Tool 
 */

#include <maya/MFnPlugin.h>

// Dpsi maya logging functions
#include <Log/MayaLog.h>

#include <BaseManip/CustomPlugin.h>

#include "ToolCmd.h"
#include "ToolNode.h"
#include "ToolNodeManip.h"

#include "TranslateContextCmd.h"
#include "TranslateManip.h"
#include "RotateContextCmd.h"
#include "RotateManip.h"
#include "ScaleContextCmd.h"
#include "ScaleManip.h"
#include "FalloffManip.h"
#include "MirrorManip.h"

MStatus initializePlugin( MObject obj )
{
	MStatus stat;
	MString errStr;
	MFnPlugin plugin(obj, "OMToolbox", "1.0", "Any", &stat); MAssertStat(stat);
 
	stat = CustomPlugin::initialize(plugin); MAssertStat(stat);

	stat = plugin.registerNode(
		MoveToolNode::typeName, MoveToolNode::typeId,
		MoveToolNode::creator, MoveToolNode::initialize,
		MPxNode::kDeformerNode); MAssertStat(stat);

	// Register the node manipulator
	stat = plugin.registerNode(
		ToolNodeManip::typeName, ToolNodeManip::typeId,
		ToolNodeManip::creator, ToolNodeManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);

	// Register the translation context
	stat = plugin.registerContextCommand(
		TranslateContextCmd::toolName, TranslateContextCmd::creator,
		"TranslateCmd", MoveToolCmd::creator,
		MoveToolCmd::newSyntax); MAssertStat(stat);  

	// Register the translation manipulator
   	stat = plugin.registerNode(
		TranslateManip::typeName, TranslateManip::typeId,
		TranslateManip::creator, TranslateManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);

	// Register the rotate context
	stat = plugin.registerContextCommand(
		RotateContextCmd::toolName, RotateContextCmd::creator,
		"RotateCmd", MoveToolCmd::creator,
		MoveToolCmd::newSyntax); MAssertStat(stat);

	// Register the Rotate manipulator
   	stat = plugin.registerNode(
		RotateManip::typeName, RotateManip::typeId,
		RotateManip::creator, RotateManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);

	// Register the rotate context
	stat = plugin.registerContextCommand(
		ScaleContextCmd::toolName, ScaleContextCmd::creator,
		"ScaleCmd", MoveToolCmd::creator,
		MoveToolCmd::newSyntax); MAssertStat(stat);

	// Register the Scale manipulator
   	stat = plugin.registerNode(
		ScaleManip::typeName, ScaleManip::typeId,
		ScaleManip::creator, ScaleManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);

	// Register the Falloff manip
	stat = plugin.registerNode(
		FalloffManip::typeName, FalloffManip::typeId,
		FalloffManip::creator, FalloffManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);
   
	stat = plugin.registerNode(
		MirrorManip::typeName, MirrorManip::typeId,
		MirrorManip::creator, MirrorManip::initialize,
		MPxNode::kManipContainer); MAssertStat(stat);

	// Begin to setup all of the plugin's UIs during the loadPlugin call
	// NOTE: THIS IS VERY IMPORTANT TO Do to maintain the stability of maya while loading the 
	// plugin. Because MEL initialization is SO fucked up, we need to presource the Properties/Values
	// beforehand.
	// NOTE: Change this to plugin.registerUI 	
	// stat = plugin.registerUI("MoveToolSetupUI", "MoveToolRemoveUI"); MAssertStat(stat);
	stat = MGlobal::executeCommand("source \"OMT_ZT_SetupUI.mel\"", true); MAssertStat(stat);

	return(MS::kSuccess);
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus stat;
	MString errStr;
	MFnPlugin plugin(obj);

	// Remove the tool's UI.
	stat = MGlobal::executeCommand("OMT_ZT_RemoveUI", true); MAssertStat(stat);

	stat = plugin.deregisterNode(MirrorManip::typeId); MAssertStat(stat);
	stat = plugin.deregisterNode(FalloffManip::typeId); MAssertStat(stat);

	stat = plugin.deregisterNode(ScaleManip::typeId); MAssertStat(stat);
	stat = plugin.deregisterNode(RotateManip::typeId); MAssertStat(stat);
	stat = plugin.deregisterNode(TranslateManip::typeId); MAssertStat(stat);
	stat = plugin.deregisterContextCommand(ScaleContextCmd::toolName, "ScaleCmd"); MAssertStat(stat);
	stat = plugin.deregisterContextCommand(RotateContextCmd::toolName, "RotateCmd"); MAssertStat(stat);
	stat = plugin.deregisterContextCommand(TranslateContextCmd::toolName, "TranslateCmd"); MAssertStat(stat);

	stat = plugin.deregisterNode(ToolNodeManip::typeId); MAssertStat(stat);
	stat = plugin.deregisterNode(MoveToolNode::typeId); MAssertStat(stat);
	stat = CustomPlugin::deinitialize(plugin); MAssertStat(stat);

	return stat;
}

