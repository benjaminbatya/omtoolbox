/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "TranslateContext.h"
#include "TranslateManip.h"

// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MDagPath.h>

#define HELPSTR	    "Drag manipulator to translate the selected vertices."
#define TITLESTR	"ZooZ Translation Tool"
#define CONTEXTNAME	    "TranslateTool"
#define CONTEXTIMAGE	"move.xpm"

TranslateContext::TranslateContext() : BaseContext(CONTEXTNAME, CONTEXTIMAGE, TITLESTR, HELPSTR, TranslateManip::typeName)
{
	MStatus stat;
	// MTrace("TranslateContext::TranslateContext: New context created."); 
	this->_drawArrowHead = true;
	this->customDragDelta = MVector::zero;
}

bool TranslateContext::isChangeLarge(MStatus* stat)
{
	MValidateStatPtr(stat);
	double size = this->getModel()->getTranslation(stat).length(); MAssertStatReturnObj(*stat, false);

	if(size >= 1e-4)
	{
		return(true);
	} else
	{
		return(false);
	}
}

MStatus TranslateContext::doCustomPress(MEvent& event)
{
	MStatus stat;

	// MTrace("TranslateContext::doCustomPress: called");	
	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// Get the start position of the current drag
	short eX, eY;
	stat = event.getPosition(eX, eY); MAssertStat(stat);
	M3dView view = M3dView::active3dView(&stat); MAssertStat(stat);
	MPoint temp, rayStart, pos, startPos; 
	MVector rayDirection, normal;
	stat = view.viewToWorld(eX, eY, rayStart, rayDirection); MAssertStat(stat);
	stat = this->getCenter(pos); MAssertStat(stat);
	stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal); MAssertStat(stat);
	normal *= -1;
	stat = this->calcPlaneLineIntersection(rayStart, rayDirection, pos, normal, startPos); MAssertStat(stat);

	this->customDragDelta = startPos - this->getModel()->getCenterPoint(&stat); MAssertStat(stat);

	// Set the drawArrow flag to false...
	this->setDrawArrowHead(false);
	// Notify the observers
	this->Notify(MSG_DRAW_ARROW_HEADS); 

	return(MS::kSuccess);
}
MStatus TranslateContext::doCustomDrag(MEvent& event)
{
	// MTrace("TranslateContext::doCustomDrag: called");
	MStatus stat;

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }   

	// Get the start position of the current drag  
	short eX, eY;
	stat = event.getPosition(eX, eY); MAssertStat(stat);
	M3dView view = M3dView::active3dView(&stat); MAssertStat(stat);
	MPoint temp, endPt, rayStart, pos; 
	MVector rayDirection, normal;
	stat = view.viewToWorld(eX, eY, rayStart, rayDirection); MAssertStat(stat);
	stat = this->getCenter(pos); MAssertStat(stat);
	stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, normal); MAssertStat(stat);
	normal *= -1;
	stat = this->calcPlaneLineIntersection(rayStart, rayDirection, pos, normal, endPt); MAssertStat(stat);

	MVector delta = endPt - this->customDragDelta - this->getModel()->getCenterPoint(&stat); MAssertStat(stat);

	stat = this->setTranslation(delta); MAssertStat(stat);

	// Has to be manually notified that a drag is occurring
	this->Notify(MSG_CENTER);

	return(MS::kSuccess);
}
MStatus TranslateContext::doCustomRelease(MEvent& event)
{
	// MTrace("TranslateContext::doCustomRelease: called");

	// Don't do anything is an object IS NOT selected
	if(this->isSelecting()) { return(MS::kSuccess); }
	
	// Finalize the command
	MStatus stat = this->finalizeCmd(); MAssertStat(stat);
 
	// Set the drawArrow flag to true...
	this->setDrawArrowHead(true);
	// Notify the observers
	this->Notify(MSG_DRAW_ARROW_HEADS);

	return(MS::kSuccess);
}

MVector TranslateContext::getTranslation(MStatus* stat)
{
	MValidateStatPtr(stat);
	MVector vec = this->getModel()->getTranslation(stat); MAssertStatReturnObj(*stat, vec);
	// MTrace("TranslateContext::getTranslation: translation = " + Format::toString(vec));
	return(vec);
}

MStatus TranslateContext::setTranslation(const MVector& delta)
{
	// MTrace("TranslateContext::updateCmd: called.");
	MStatus stat;
	
	// Update the command
	stat = this->getModel()->undoIt(); MAssertStat(stat);
	stat = this->getModel()->setTranslation(delta); MAssertStat(stat);
	stat = this->getModel()->redoIt(); MAssertStat(stat);

	return(stat);
}

bool TranslateContext::getDrawArrowHead() const
{
	return(this->_drawArrowHead);
}
void TranslateContext::setDrawArrowHead(const bool flag)
{
	this->_drawArrowHead = flag;
}

