/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "BaseContextCmd.h"
#include "CmdFlags.h"
#include "BaseContext.h"

// Custom Maya logging functions
#include <Log/MayaLog.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MArgParser.h>
#include <maya/MDistance.h>

BaseContextCmd::BaseContextCmd()
{ 
	this->contextInstance = NULL; 
};

// Flags for displaying/not displaying modifier manips
const charPtr kDisplayFalloff = "-df", kDisplayFalloffLong = "-displayFalloff";
const charPtr kDisplayMirror = "-dm", kDisplayMirrorLong = "-displayMirror";
// Flag for resetting the falloff curve
const charPtr kFalloffCurveReset = "-fcr", kFalloffCurveResetLong = "-falloffCurveReset";

MStatus BaseContextCmd::appendSyntax()
{
    MStatus stat;
    // MTrace("BaseContextCmd::appendSyntax: called.");

    MSyntax syntax = this->syntax(&stat); MAssertStat(stat);

	// Move space flag
	// NOTE: add later...

	// Display flags
 	stat = syntax.addFlag(kDisplayFalloff, kDisplayFalloffLong, MSyntax::kBoolean); MAssertStat(stat);
	stat = syntax.addFlag(kDisplayMirror, kDisplayMirrorLong, MSyntax::kBoolean); MAssertStat(stat);

    // Falloff flags 
	stat = syntax.addFlag(kFalloffType, kFalloffTypeLong, MSyntax::kUnsigned); MAssertStat(stat);
	stat = syntax.addFlag(kFalloffInnerRadius, kFalloffInnerRadiusLong, MSyntax::kDistance); MAssertStat(stat);
	stat = syntax.addFlag(kFalloffOuterRadius, kFalloffOuterRadiusLong, MSyntax::kDistance); MAssertStat(stat);
	stat = syntax.addFlag(kFalloffCurveReset, kFalloffCurveResetLong); MAssertStat(stat);
	stat = syntax.addFlag(kFalloffAxis, kFalloffAxisLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStat(stat);
	
	// Mirror flags
	stat = syntax.addFlag(kMirrorType, kMirrorTypeLong, MSyntax::kUnsigned); MAssertStat(stat);
	stat = syntax.addFlag(kMirrorAxis, kMirrorAxisLong, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble); MAssertStat(stat);

    return(stat);
}

MStatus BaseContextCmd::doQueryFlags()
{
	MStatus stat;
	// MTrace("BaseContextCmd::doQueryFlags: called");

	MAssert(this->contextInstance != NULL);

	MArgParser parser = this->parser(&stat); MAssertStat(stat);

	// Check if each flag is set and return the result, else signal an error
	bool flagSet = parser.isFlagSet(kDisplayFalloff, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// MTrace("BaseContextCmd::doQueryFlags: handling display falloff flag.");
		stat = this->setResult(this->contextInstance->getDisplayFalloffManip()); MAssertStat(stat);
		return(MS::kSuccess);
	}
	flagSet = parser.isFlagSet(kDisplayMirror, &stat); MAssertStat(stat);
	if(flagSet)
	{
		stat = this->setResult(this->contextInstance->getDisplayMirrorManip()); MAssertStat(stat);
		return(MS::kSuccess);
	}

	flagSet = parser.isFlagSet(kFalloffType, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// MTrace("BaseContextCmd::doQueryFlags: handling falloff type flag.");
		MoveToolCmdModel::FalloffType type = this->contextInstance->getModel()->getFalloffType();
		stat = this->setResult(type); MAssertStat(stat);
		return(MS::kSuccess);
	}
 
	flagSet = parser.isFlagSet(kFalloffInnerRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MDistance rad = this->contextInstance->getModel()->getFalloffInnerRadius();
		double ret = rad.as(MDistance::uiUnit(), &stat); MAssertStat(stat);
		stat = this->setResult(ret); MAssertStat(stat);
		return(MS::kSuccess);
	}

	flagSet = parser.isFlagSet(kFalloffOuterRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// MTrace("BaseContextCmd::doQueryFlags: handling falloff radius flag.");
		MDistance rad = this->contextInstance->getModel()->getFalloffOuterRadius();
		double ret = rad.as(MDistance::uiUnit(), &stat); MAssertStat(stat);
		stat = this->setResult(ret); MAssertStat(stat);
		return(MS::kSuccess);
	}

	flagSet = parser.isFlagSet(kFalloffAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector axis = this->contextInstance->getModel()->getFalloffAxis();
		MString str = MString() + axis.x + " " + axis.y + " " + axis.z;
		stat = this->setResult(str); MAssertStat(stat);
		return(MS::kSuccess);
	}

	flagSet = parser.isFlagSet(kMirrorType, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// MTrace("BaseContextCmd::doQueryFlags: handling falloff type flag.");
		MoveToolCmdModel::MirrorType type = this->contextInstance->getModel()->getMirrorType();
		stat = this->setResult(type); MAssertStat(stat);
		return(MS::kSuccess);
	}

	flagSet = parser.isFlagSet(kMirrorAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector axis = this->contextInstance->getModel()->getMirrorAxis(&stat); MAssertStat(stat);
		MString str = MString() + axis.x + " " + axis.y + " " + axis.z;
		stat = this->setResult(str); MAssertStat(stat);
		return(MS::kSuccess);
	}

	return(MS::kInvalidParameter);
}

MStatus BaseContextCmd::doEditFlags()
{
	MStatus stat;

	MTrace("BaseContextCmd::doEditFlags: called");
	MAssert(this->contextInstance != NULL);

	MArgParser parser = this->parser(&stat); MAssertStat(stat);

	bool flagSet = parser.isFlagSet(kDisplayFalloff, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// Get the value and set it in the context
		bool val;
		stat = parser.getFlagArgument(kDisplayFalloff, 0, val); MAssertStat(stat);
		stat = this->contextInstance->setDisplayFalloffManip(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kDisplayMirror, &stat); MAssertStat(stat);
	if(flagSet)
	{
		bool val;
		stat = parser.getFlagArgument(kDisplayMirror, 0, val); MAssertStat(stat);
		stat = this->contextInstance->setDisplayMirrorManip(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kFalloffType, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MoveToolCmdModel::FalloffType type;
		stat = parser.getFlagArgument(kFalloffType, 0, (unsigned&)type); MAssertStat(stat);
		stat = this->contextInstance->getModel()->setFalloffType(type); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kFalloffInnerRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MDistance val;
		stat = parser.getFlagArgument(kFalloffInnerRadius, 0, val); MAssertStat(stat);
		stat = this->contextInstance->getModel()->setFalloffInnerRadius(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kFalloffOuterRadius, &stat); MAssertStat(stat);
	if(flagSet)
	{
		// Get the value and set it in the context
		MDistance val;
		stat = parser.getFlagArgument(kFalloffOuterRadius, 0, val); MAssertStat(stat);  
		stat = this->contextInstance->getModel()->setFalloffOuterRadius(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kFalloffCurveReset, &stat); MAssertStat(stat);
	if(flagSet)
	{
		stat = this->contextInstance->getModel()->resetCurve(); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kFalloffAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector val;
		stat = parser.getFlagArgument(kFalloffAxis, 0, val[0]); MAssertStat(stat);
		stat = parser.getFlagArgument(kFalloffAxis, 1, val[1]); MAssertStat(stat);
		stat = parser.getFlagArgument(kFalloffAxis, 2, val[2]); MAssertStat(stat);
		stat = this->contextInstance->getModel()->setFalloffAxis(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kMirrorType, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MoveToolCmdModel::MirrorType val;
		stat = parser.getFlagArgument(kMirrorType, 0, (unsigned&)val); MAssertStat(stat);
		stat = this->contextInstance->getModel()->setMirrorType(val); MAssertStat(stat);
	}

	flagSet = parser.isFlagSet(kMirrorAxis, &stat); MAssertStat(stat);
	if(flagSet)
	{
		MVector val;
		stat = parser.getFlagArgument(kMirrorAxis, 0, val[0]); MAssertStat(stat);
		stat = parser.getFlagArgument(kMirrorAxis, 1, val[1]); MAssertStat(stat);
		stat = parser.getFlagArgument(kMirrorAxis, 2, val[2]); MAssertStat(stat);
		stat = this->contextInstance->getModel()->setMirrorAxis(val); MAssertStat(stat);
	}

	return(MS::kSuccess);
}
