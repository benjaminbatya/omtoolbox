/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "MirrorManip.h"
#include "BaseContext.h"

#include <Log/MayaLog.h>

#include <maya/MSelectionList.h>
#include <maya/MCommandResult.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItGeometry.h>
#include <maya/MManipData.h>

#include <maya/MFnDirectionManip.h>

MTypeId MirrorManip::typeId(0x80021);
MString MirrorManip::typeName("MirrorManip");

const char* mirrorColorName = "userDefined2";

void* MirrorManip::creator()
{
	return(new MirrorManip);
}

MStatus MirrorManip::customConnectToDependNode(const MObject& node)
{
	MStatus stat;

	this->directionManip = this->addDirectionManip("mirrorAxis", "ma");
	MFnDirectionManip directionFn(this->directionManip, &stat); MAssertStat(stat);
	stat = directionFn.setNormalizeDirection(false); MAssertStat(stat);

	// Hook up the start point to a center callback
	unsigned slot = directionFn.startPointIndex(&stat); MAssertStat(stat);
	this->addPlugToManipConversionCallback(slot, 
		(plugToManipConversionCallback)this->centerPToMCB); 
	
	// Hook up the direction plug
	slot = directionFn.directionIndex(&stat); MAssertStat(stat);
	this->addPlugToManipConversionCallback(slot,
		(plugToManipConversionCallback)this->directionPToMCB);

	MFnDependencyNode nodeFn(node, &stat); MAssertStat(stat);
	MPlug nodeStatePlug = nodeFn.findPlug("nodeState", &stat); MAssertStat(stat); 
	// This callback is shared by all of the CustomManips for the ManipToPlug callback
	this->addManipToPlugConversionCallback(nodeStatePlug,
		(manipToPlugConversionCallback)this->generalMToPCB);

	return(MS::kSuccess);
}

void MirrorManip::drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{ 
	MSelectionList sList;
	MStatus stat = MGlobal::getActiveSelectionList(sList); MAssertStatNoReturn(stat);
	// Check the selection list that we're got a selected object	
	unsigned length = sList.length(&stat); MAssertStatNoReturn(stat);
	MAssertNoReturn(length == 1);

	MDagPath dagPath;
	stat = sList.getDagPath(0, dagPath); MAssertStatNoReturn(stat); 

	// Get the weights
	MDoubleArray weights = this->context->getModel()->getMirrorWeights(&stat); MAssertStatNoReturn(stat);
 
	// Get the correct color for the falloff
	MString cmd = MString("displayRGBColor -q ") + mirrorColorName;
	MCommandResult res;
	stat = MGlobal::executeCommand(cmd, res); MAssertStatNoReturn(stat);
	MDoubleArray color;
	stat = res.getResult(color); MAssertStatNoReturn(stat); 

	// Try using a MeshPoly iterator first so we can draw the falloff as a mesh
	MItMeshPolygon polyIt(dagPath, MObject::kNullObj, &stat);  
	if(stat == MS::kSuccess)
	{
		// Draw
		stat = view.beginGL(); MAssertStatNoReturn(stat);
		{
			glPushAttrib(GL_CURRENT_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
			{
				// Disable the lighting model.
				glDisable(GL_LIGHTING);
				// Emable the smooth shade model
				glShadeModel(GL_SMOOTH);
				// Setup culling so the backfaces aren't drawn
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);   

				// Setup the blended fill  
				glPolygonMode(GL_FRONT, GL_FILL);
				// Setup blending...
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				// Set the polygon offset
				glEnable(GL_POLYGON_OFFSET_FILL);
				glPolygonOffset(0, 0);
// #define ABS(x) ((x)<0? -(x): (x))
				// Draw each of the polygons
				MPointArray pts;
				MIntArray verts;
				for(polyIt.reset(); !polyIt.isDone(&stat); polyIt.next())
	   			{
					MAssertStatNoReturn(stat)
					polyIt.getPoints(pts, MSpace::kWorld, &stat); MAssertStatNoReturn(stat);  
					stat = polyIt.getVertices(verts); MAssertStatNoReturn(stat);

					glBegin(GL_POLYGON);
					{
						for(unsigned i=0; i<pts.length(); i++)
						{
							// MTrace("Weight for vertex " + verts[i] + " = " + falloffWeights[verts[i]]);
							double w = weights[verts[i]];      
							glColor4d(color[0], color[1], color[2], w);
							glVertex3d(pts[i].x, pts[i].y, pts[i].z);
						}
					}
					glEnd();
				}   
			}
			glPopAttrib();
		}
		stat = view.endGL(); MAssertStatNoReturn(stat);
	} else // use the geometry iterator
	{ 
		MItGeometry vertIt(dagPath, &stat); MAssertStatNoReturn(stat); 
		// Draw
		stat = view.beginGL(); MAssertStatNoReturn(stat);
		{ 
			glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT | GL_COLOR_BUFFER_BIT);
			{
				// Setup blending
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				// Setup point settings
				glEnable(GL_POINT_SMOOTH);
				glPointSize(5.0f);

				glBegin(GL_POINTS);
				{
					for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
					{
						MAssertStatNoReturn(stat);
						double w = weights[vertIt.index(&stat)]; MAssertStatNoReturn(stat);
						if(w > 0.0)
						{
							glColor4d(color[0], color[1], color[2], w);
							MPoint& pt = vertIt.position(MSpace::kWorld, &stat); MAssertStatNoReturn(stat);
							glVertex3d(pt.x, pt.y, pt.z);
						}
					}
				}
				glEnd();
			}
			glPopAttrib();
		}
		stat = view.endGL(); MAssertStatNoReturn(stat);
	}
}

MManipData MirrorManip::centerPToMCB(unsigned index)
{
	// MTrace("FalloffManip::centerPToMCB: called");  
	MStatus stat;
	MAssertReturnObj(this->context != NULL, 0);
	MPoint center;
	stat = this->context->getCenter(center); MAssertStatReturnObj(stat, 0);
	// Add in the translation
	center += this->context->getModel()->getTranslation(&stat); MAssertStatReturnObj(stat, 0);
	
	MFnNumericData numData;
	MObject numDataObj = numData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, 0);
	stat = numData.setData(center.x, center.y, center.z); MAssertStatReturnObj(stat, 0);

	return(numDataObj);
}

MManipData MirrorManip::directionPToMCB(unsigned index)
{
	// MTrace("FalloffManip::directionPToMCB: called");  
	MStatus stat;
	MAssertReturnObj(this->context != NULL, 0);
	MVector direction = this->context->getModel()->getMirrorAxis(&stat); MAssertStatReturnObj(stat, 0);
 
	MFnNumericData numData;
	MObject numDataObj = numData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, 0);
	stat = numData.setData(direction.x, direction.y, direction.z); MAssertStatReturnObj(stat, 0);

	return(numDataObj);
}
MManipData MirrorManip::generalMToPCB(unsigned index)
{
	// MTrace("MirrorManip::generalMToPCB: plug index = " + index);
	MStatus stat;
	// Get the plug value so we can return it at the end...
	int plugValue = 0;
	stat = this->getConvertorPlugValue(index, plugValue); MAssertStatReturnObj(stat, plugValue);

	// Get the directionManip's direction value
	MFnDirectionManip directionFn(this->directionManip, &stat); MAssertStatReturnObj(stat, plugValue);
	unsigned slot = directionFn.directionIndex(&stat); MAssertStatReturnObj(stat, plugValue);
	MVector direction;
	stat = this->getConverterManipValue(slot, direction); MAssertStatReturnObj(stat, plugValue);
	 
	// Sanity check
	MAssertReturnObj(this->context != NULL, plugValue);
	stat = this->context->getModel()->setMirrorAxis(direction); MAssertStatReturnObj(stat, plugValue);

	return(plugValue);
}
