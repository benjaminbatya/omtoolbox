/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "FalloffManip.h"
#include "BaseContext.h"

#include <Log/MayaLog.h>

#include <BaseManip/ViewportDistanceManip.h>
#include <BaseManip/CatmullRomCurveManip.h>

#include <maya/MFnStateManip.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MCommandResult.h>
#include <maya/MItGeometry.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MSelectionList.h>

MTypeId FalloffManip::typeId(0x8001e);
MString FalloffManip::typeName("FalloffManip");

const char* falloffColorName = "userDefined1";

void* FalloffManip::creator()
{
	return(new FalloffManip);
}

MStatus FalloffManip::customConnectToDependNode(const MObject& node)
{
	MStatus stat;

	MFnDependencyNode nodeFn(node, &stat); MAssertStat(stat);
	MPlug nodeStatePlug = nodeFn.findPlug("nodeState", &stat); MAssertStat(stat);
	
	// This callback is shared by all of the CustomManips for the ManipToPlug callback
	this->addManipToPlugConversionCallback(nodeStatePlug,
		(manipToPlugConversionCallback)this->generalMToPCB);

	// Create the inner radius manip
	this->innerRadiusManip = ViewportDistanceManip::create(this, &stat); MAssertStat(stat);
	this->innerRadiusManip->setScalingFactor(12.0);
	this->innerRadiusManip->setLabel("Inner Radius");
	this->innerRadiusManip->setLabelPos(ViewportDistanceManip::kTop);
	this->innerRadiusManip->setInfluencedByInteraction(false);
	// Callbacks
	this->addPlugToManipConversionCallback(this->innerRadiusManip->centerIndex(),
		(plugToManipConversionCallback)this->centerPToMCB);
	this->addPlugToManipConversionCallback(this->innerRadiusManip->radiusIndex(),
		(plugToManipConversionCallback)this->innerRadiusPToMCB);

	// Create the outer distance manip
	this->outerDistManip = ViewportDistanceManip::create(this, &stat); MAssertStat(stat);
	this->outerDistManip->setScalingFactor(12.0);
	this->outerDistManip->setLabel("Outer Distance");
	this->outerDistManip->setLabelPos(ViewportDistanceManip::kBottom);
	this->outerDistManip->setInfluencedByInteraction(false);  
	// Callbacks
	this->addPlugToManipConversionCallback(this->outerDistManip->centerIndex(),
		(plugToManipConversionCallback)this->centerPToMCB);
	this->addPlugToManipConversionCallback(this->outerDistManip->radiusIndex(),
		(plugToManipConversionCallback)this->outerRadiusPToMCB);

	// Create the falloff curve manip.
	this->curveManip = CatmullRomCurveManip::create(this, &stat); MAssertStat(stat);
	this->curveManip->setLabel("Falloff Curve");
	this->curveManip->setSize(MVector(150, 100));
	// callbacks
	this->addPlugToManipConversionCallback(this->curveManip->controlPtsIndex(),
		(plugToManipConversionCallback)this->curvePToMCB);

	return(MS::kSuccess);
}

void FalloffManip::drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{ 
	MSelectionList sList;
	MStatus stat = MGlobal::getActiveSelectionList(sList); MAssertStatNoReturn(stat);
	// Check the selection list that we're got a selected object	
	unsigned length = sList.length(&stat); MAssertStatNoReturn(stat);
	MAssertNoReturn(length == 1);

	MDagPath dagPath;
	stat = sList.getDagPath(0, dagPath); MAssertStatNoReturn(stat); 

	// Get the weights
	MDoubleArray weights = this->context->getModel()->getFalloffWeights(&stat); MAssertStatNoReturn(stat);
 
	// Get the correct color for the falloff
	MString cmd = MString("displayRGBColor -q ") + falloffColorName;
	MCommandResult res;
	stat = MGlobal::executeCommand(cmd, res); MAssertStatNoReturn(stat);
	MDoubleArray color;
	stat = res.getResult(color); MAssertStatNoReturn(stat); 

	// Try using a MeshPoly iterator first so we can draw the falloff as a mesh
	MItMeshPolygon polyIt(dagPath, MObject::kNullObj, &stat); 
	if(stat == MS::kSuccess)
	{
		// Draw
		stat = view.beginGL(); MAssertStatNoReturn(stat);
		{
			glPushAttrib(GL_CURRENT_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
			{
				// Disable the lighting model.
				glDisable(GL_LIGHTING);
				// Emable the smooth shade model
				glShadeModel(GL_SMOOTH);
				// Setup culling so the backfaces aren't drawn
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);   

				// Setup the blended fill  
				glPolygonMode(GL_FRONT, GL_FILL);
				// Setup blending...
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				// Set the polygon offset
				glEnable(GL_POLYGON_OFFSET_FILL);
				glPolygonOffset(0, 0);
// #define ABS(x) ((x)<0? -(x): (x))
				// Draw each of the polygons
				MPointArray pts;
				MIntArray verts;
				for(polyIt.reset(); !polyIt.isDone(&stat); polyIt.next())
	   			{
					MAssertStatNoReturn(stat)
					polyIt.getPoints(pts, MSpace::kWorld, &stat); MAssertStatNoReturn(stat);  
					stat = polyIt.getVertices(verts); MAssertStatNoReturn(stat);

					glBegin(GL_POLYGON);
					{
						for(unsigned i=0; i<pts.length(); i++)
						{
							// MTrace("Weight for vertex " + verts[i] + " = " + falloffWeights[verts[i]]);
							double w = weights[verts[i]];      
							glColor4d(color[0], color[1], color[2], w);
							glVertex3d(pts[i].x, pts[i].y, pts[i].z);
						}
					}
					glEnd();
				}   
			}
			glPopAttrib();
		}
		stat = view.endGL(); MAssertStatNoReturn(stat);
	} else // use the geometry iterator
	{ 
		MItGeometry vertIt(dagPath, &stat); MAssertStatNoReturn(stat); 
		// Draw
		stat = view.beginGL(); MAssertStatNoReturn(stat);
		{ 
			glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT | GL_COLOR_BUFFER_BIT);
			{
				// Setup blending
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				// Setup point settings
				glEnable(GL_POINT_SMOOTH);
				glPointSize(5.0f);

				glBegin(GL_POINTS);
				{
					for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
					{
						MAssertStatNoReturn(stat);
						double w = weights[vertIt.index(&stat)]; MAssertStatNoReturn(stat);
						if(w > 0.0)
						{
							glColor4d(color[0], color[1], color[2], w);
							MPoint& pt = vertIt.position(MSpace::kWorld, &stat); MAssertStatNoReturn(stat);
							glVertex3d(pt.x, pt.y, pt.z);
						}
					}
				}
				glEnd();
			}
			glPopAttrib();
		}
		stat = view.endGL(); MAssertStatNoReturn(stat);
	}
}

MManipData FalloffManip::centerPToMCB(unsigned index)
{
	MObject numDataObj;
	// MTrace("FalloffManip::centerPToMCB: called"); 
	MPoint center;
	MAssertReturnObj(this->context != NULL, 0);
	MStatus stat = this->context->getCenter(center); MAssertStatReturnObj(stat, numDataObj);
	// Add in the translation
	center += this->context->getModel()->getTranslation(&stat); MAssertStatReturnObj(stat, numDataObj);
	MFnNumericData numData;
	numDataObj = numData.create(MFnNumericData::k3Double, &stat); MAssertStatReturnObj(stat, numDataObj);
	stat = numData.setData(center.x, center.y, center.z); MAssertStatReturnObj(stat, numDataObj);

	return(numDataObj);
}

MManipData FalloffManip::innerRadiusPToMCB(unsigned index)
{
	MStatus stat;
	MAssertReturnObj(this->context != NULL, 0);
	MDistance rad = this->context->getModel()->getFalloffInnerRadius();
	double dist = rad.as(MDistance::uiUnit(), &stat); MAssertStatReturnObj(stat, dist);

	return(dist);
}
MManipData FalloffManip::outerRadiusPToMCB(unsigned index)
{
	MStatus stat;
	MAssertReturnObj(this->context != NULL, 0);
	MDistance rad = this->context->getModel()->getFalloffOuterRadius();
	double dist = rad.as(MDistance::uiUnit(), &stat); MAssertStatReturnObj(stat, dist);

	return(dist);
}
MManipData FalloffManip::generalMToPCB(unsigned index)
{
	// MTrace("FalloffManip::generalMToPCB: called.");

	MStatus stat;
	// Get the plug value so we can return it at the end...
	int plugValue = 0;
	stat = this->getConvertorPlugValue(index, plugValue); MAssertStatReturnObj(stat, plugValue);

	// Sanity check
	MAssertReturnObj(this->context != NULL, plugValue);

	// Run update for the inner radius manip
	double dist;
	stat = this->getConvertorManipValue(this->innerRadiusManip->radiusIndex(), dist); MAssertStatReturnObj(stat, plugValue);
	MDistance radius(dist, MDistance::uiUnit());
	stat = this->context->getModel()->setFalloffInnerRadius(radius); MAssertStatReturnObj(stat, plugValue);

	// Run the update for the falloff outer radius manip 
	stat = this->getConvertorManipValue(this->outerDistManip->radiusIndex(), dist); MAssertStatReturnObj(stat, plugValue);  
	radius = MDistance(dist, MDistance::uiUnit());
	stat = this->context->getModel()->setFalloffOuterRadius(radius); MAssertStatReturnObj(stat, plugValue); 

	// Run the update for the falloff curve manip
	MDoubleArray curve;
	stat = this->getConvertorManipValue(this->curveManip->controlPtsIndex(), curve); MAssertStatReturnObj(stat, plugValue);
	stat = this->context->getModel()->setFalloffCurve(curve); MAssertStatReturnObj(stat, plugValue);

	return(plugValue);
}

MManipData FalloffManip::curvePToMCB(unsigned index)
{
	// MTrace("FalloffManip::curvePToMCB: called");
	MStatus stat;
	// Get the falloff curve from the context
	MAssertReturnObj(this->context != NULL, 0);
	MDoubleArray value = this->context->getModel()->getFalloffCurve();
    // Convert the value into a DoubleArray Object
    MFnDoubleArrayData arrayData;
    MObject obj = arrayData.create(value, &stat); MAssertStatReturnObj(stat, MObject::kNullObj);
    return(obj);
}
