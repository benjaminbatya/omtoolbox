/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ToolNodeManip.h"

#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MFnDependencyNode.h>

const MTypeId ToolNodeManip::typeId(0x00451);
const MString ToolNodeManip::typeName("ZooZToolNodeManip");

void* ToolNodeManip::creator()
{
	return(new ToolNodeManip);
}

MStatus ToolNodeManip::customConnectToDependNode(const MObject& node)
{
	MTrace("ToolNodeManip::customConnectToDependNode: called");
	MStatus stat;

	// Set the target object
	this->targetObj = node;

	// Connect the locator to the various node attributes
	// Later...


	return(MS::kSuccess);
}


/**
 * Do custom drawing here. Draw the influence of the node falloff weights.
 */
void ToolNodeManip::drawManip(M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{
	MStatus stat;
	MFnDependencyNode nodeFn(this->targetObj, &stat); MAssertStatNoReturn(stat);

	stat = view.drawText("N", MPoint::origin); MAssertStatNoReturn(stat);
}
