/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ToolModel.h"

// Maya logging functions
#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>
#include <maya/M3dView.h>
#include <maya/MItGeometry.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MMatrix.h>
#include <maya/MQuaternion.h>

#include <math.h>
#include <queue>

#include "CatmullRomCurve.h"

MoveToolCmdModel::MoveToolCmdModel(const IterationType type) : updateType(type)
{
	this->clear();
	

	// Test Code to see MTransformMatrix's (WRONG!) pivot behavior
	/*
	MStatus stat;
	MTransformationMatrix first, second;
	double rot[3] = { 0.0, 1.23, 0.0};
	MPoint pivot(12.3, 3.4, -2.3);
	stat = first.setRotation(rot, MTransformationMatrix::kXYZ); MAssertStatNoReturn(stat);
	stat = first.setRotatePivot(pivot, MSpace::kPostTransform, true); MAssertStatNoReturn(stat);
	// stat = first.setRotation(rot, MTransformationMatrix::kXYZ); MAssertStatNoReturn(stat);
	stat = second.setRotatePivot(pivot, MSpace::kPostTransform, true); MAssertStatNoReturn(stat);
	stat = second.setRotation(rot, MTransformationMatrix::kXYZ); MAssertStatNoReturn(stat);
	MTrace("First = " + Format::toString(first.asMatrix()));
	MTrace("Second = " + Format::toString(second.asMatrix()));
	*/
}
MoveToolCmdModel::~MoveToolCmdModel()
{
	// Nothing for now...
}

/************************************
 Reset methods
 ************************************/
MStatus MoveToolCmdModel::clear()
{
	// Initialize the default arguments
	MStatus stat = this->resetTransform(); MAssertStat(stat);
	this->centerPointDirtyFlag = true;
	this->centerPoint = MPoint::origin;
	
	// Defaults for falloff
	this->falloffType = this->kNone;
	this->falloffInnerRadius = MDistance(0, MDistance::uiUnit());
	this->falloffOuterRadius = MDistance(10, MDistance::uiUnit()); 
	stat = this->resetCurve(); MAssertStat(stat);
	this->falloffAxis = MVector::xAxis;
	this->falloffWeights.clear();
	this->falloffWeightsDirtyFlag = true;

	// Defaults for mirror
	this->mirrorType = this->kMirrorNone;
	this->mirrorAxis = MVector::xAxis;
	this->mirrorWeights.clear();
	this->mirrorWeightsDirty = true;

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::resetCurve()
{ 
	const double dArr[4] = {1.0, 0.66, 0.33, 0.0};
	MDoubleArray arr(dArr, 4);
	MStatus stat = this->setFalloffCurve(arr); MAssertStat(stat);
	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::resetTransform()
{
	MStatus stat;
	this->transform = MTransformationMatrix::identity;
	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::reset()
{
	// reset should only be called from the context, NOT the node
	MAssert(this->updateType == this->kContextSet);

	// Make the output data dirty
	this->centerPointDirtyFlag = true;
	this->falloffWeightsDirtyFlag = true;
	this->mirrorWeightsDirty = true;

	// Reset the transform
	MStatus stat = this->resetTransform(); MAssertStat(stat);

	// Save the original point positions so they can be restored during undo
	// This is because the inverse transformation matrix doesn't restore the points exactly
	// so they have to be restored with saved positions.
	MItGeometry geomIt(this->dagPath, &stat); MAssertStat(stat);
	stat = this->undoPositions.setLength(geomIt.count()); MAssertStat(stat);
	for(geomIt.reset(); !geomIt.isDone(&stat); geomIt.next())
	{
		MAssertStat(stat);
		unsigned idx = geomIt.index(&stat); MAssertStat(stat);
		MPoint pt = geomIt.position(MSpace::kWorld, &stat); MAssertStat(stat);
		stat = this->undoPositions.set(pt, idx); MAssertStat(stat);
	} 

	return(MS::kSuccess);
}

/*************************************
 Copy method
 *************************************/
MoveToolCmdModel& MoveToolCmdModel::operator=(const MoveToolCmdModel& that)
{
	// Copy over the attributes 
	this->transform = that.transform;
	this->centerPointDirtyFlag = true;

	this->falloffType = that.falloffType;
	this->falloffInnerRadius = that.falloffInnerRadius;
	this->falloffOuterRadius = that.falloffOuterRadius;
	this->falloffCurve = that.falloffCurve;
	this->falloffAxis = that.falloffAxis; 
	this->falloffWeightsDirtyFlag = true;
 	
	this->mirrorType = that.mirrorType;
	this->mirrorAxis = that.mirrorAxis;
	this->mirrorWeightsDirty = true;

	return(*this);
}

/**********************************************
 Command methods
 **********************************************/
MStatus MoveToolCmdModel::redoIt()
{
	// Move everything by delta
	MStatus stat = this->processGeom(); MAssertStat(stat);
	return(MS::kSuccess);
}
MStatus MoveToolCmdModel::undoIt()
{
	MStatus stat;
	// UndoIt should only be called from the context, NOT the node
	MAssert(this->updateType == this->kContextSet);

	// Make sure both mirror and falloff weights are up to date.
	stat = this->calcFalloffWeights(); MAssertStat(stat);
	stat = this->calcMirrorWeights(); MAssertStat(stat);

	// Restore the saved positions. See reset()
	MItGeometry geomIt(this->dagPath, &stat); MAssertStat(stat);
	MAssert(this->undoPositions.length() == geomIt.count());
	MAssert(this->falloffWeights.length() == geomIt.count());
	MAssert(this->mirrorWeights.length() == geomIt.count());
	for(geomIt.reset(); !geomIt.isDone(&stat); geomIt.next())
	{
		MAssertStat(stat);
		unsigned idx = geomIt.index(&stat); MAssertStat(stat);  
		// Quick optimization. If both falloffWeight and mirrorWeight == 0.0, no need to
		// restore the position.
		// NOTE: this optimization maybe unneeded.
		if(this->falloffWeights[idx] == 0.0 && this->mirrorWeights[idx] == 0.0)
		{
			continue;
		}
		stat = geomIt.setPosition(this->undoPositions[idx], MSpace::kWorld); MAssertStat(stat);
	}

	return(MS::kSuccess);
}

/****************************************
 Getter/Setter methods
 ****************************************/
MStatus MoveToolCmdModel::setContextGeom(MDagPath& dagPath, MObject& component)
{ 
	// Sanity
	MAssert(this->updateType == this->kContextSet);

	// MTrace("MoveToolCmdModel::setContextGeom: called.");
	this->dagPath = dagPath;
	this->component = component;
	// Mark the weights as dirty so they'll be recalculated
	this->falloffWeightsDirtyFlag = true;
	this->centerPointDirtyFlag = true;

	// Make sure center point is up to date
	MStatus stat = this->calcCenterPoint(); MAssertStat(stat);

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::setNodeGeom(MDataHandle& dataHandle, unsigned groupId)
{ 
	// Sanity
	MAssert(this->updateType == this->kNodeSet);
	
	// MTrace("MoveToolCmdModel::setNodeGeom: called.");
	// Make sure the data handle is a correct type
	MAssert(dataHandle.isNumeric() == false &&
		(dataHandle.type() == MFnData::kMesh ||
		 // Skip checking for pluginData for now...
		 // dataHandle.type() == MFnData::kPluginGeometry ||
		 dataHandle.type() == MFnData::kNurbsCurve ||
		 dataHandle.type() == MFnData::kNurbsSurface ||
		 dataHandle.type() == MFnData::kSubdSurface));
	this->dataHandle = dataHandle;  
	this->groupId = groupId;
	// Mark the weights as dirty so they'll be recalculated	
	this->falloffWeightsDirtyFlag = true; 
	this->centerPointDirtyFlag = true;

	// Make sure center point is up to date
	MStatus stat = this->calcCenterPoint(); MAssertStat(stat);

	return(MS::kSuccess);
}

MVector MoveToolCmdModel::getTranslation(MStatus* stat) const
{
	MValidateStatPtr(stat);
	MVector vec = this->transform.translation(MSpace::kTransform, stat); MAssertStatReturnObj(*stat, MVector::zero);
	return(vec);
}
MStatus MoveToolCmdModel::setTranslation(const MVector& vec)
{
	MStatus stat = this->transform.setTranslation(vec, MSpace::kTransform); MAssertStat(stat);
	return(MS::kSuccess);
}
MVector MoveToolCmdModel::getScale(MStatus* stat) const
{
	MValidateStatPtr(stat);
	double val[3];
	*stat = this->transform.getScale(val, MSpace::kTransform); MAssertStatReturnObj(*stat, val);
	return(val);
}
MStatus MoveToolCmdModel::setScale(const MVector& vec)
{
	MStatus stat;
	double val[3];
	stat = vec.get(val); MAssertStat(stat);
	stat = this->transform.setScale(val, MSpace::kTransform); MAssertStat(stat);
	return(MS::kSuccess);
}
MVector MoveToolCmdModel::getRotation(MStatus* stat) const
{
	MValidateStatPtr(stat);
	double val[3];
	MTransformationMatrix::RotationOrder order = MTransformationMatrix::kXYZ;
	*stat = this->transform.getRotation(val, order); MAssertStatReturnObj(*stat, val);
	return(val);
}
MStatus MoveToolCmdModel::setRotation(const MVector& vec)
{
	// MTrace("MoveToolCmdModel::setRotation: vec = " + Format::toString(vec));
	MStatus stat;
	double val[3];
	stat = vec.get(val); MAssertStat(stat);
	stat = this->transform.setRotation(val, MTransformationMatrix::kXYZ); MAssertStat(stat);
   
	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::setFalloffType(const FalloffType& type)
{
	MStatus stat;

	if(type != this->falloffType)
	{ 
		// Sanity
		MAssert(type < this->kLastFalloffType);
		this->falloffType = type;
		
		// Both falloff and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;
		  
		// Inform all of the views that the falloff type changed
		this->Notify(MSG_FALLOFF_TYPE);  
	}
	return(MS::kSuccess);
}
MoveToolCmdModel::FalloffType MoveToolCmdModel::getFalloffType() const
{
	return(this->falloffType);
}
MStatus MoveToolCmdModel::setFalloffInnerRadius(const MDistance& radius)
{
	if(radius.asCentimeters() != this->falloffInnerRadius.asCentimeters())
	{ 
		// Correct for the outer radius...
		if(radius.asCentimeters() > this->falloffOuterRadius.asCentimeters())
		{
			this->falloffInnerRadius = this->falloffOuterRadius;
		} else
		{ 
			this->falloffInnerRadius = radius;
		}
		
		// Both falloff and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;

		// Inform all of the views that the outer radius changed
		this->Notify(MSG_FALLOFF_INNERRADIUS);
	}
	
	return(MS::kSuccess);
}
MDistance MoveToolCmdModel::getFalloffInnerRadius() const
{
	return(this->falloffInnerRadius);
}
MStatus MoveToolCmdModel::setFalloffOuterRadius(const MDistance& radius)
{
	if(radius.asCentimeters() != this->falloffOuterRadius.asCentimeters())
	{ 
		// Correct for the inner radius...
		if(radius.asCentimeters() < this->falloffInnerRadius.asCentimeters())
	   	{
			this->falloffOuterRadius = this->falloffInnerRadius;
		} else
		{ 
			this->falloffOuterRadius = radius;
		}
		
		// Both falloff and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;

		// Inform all of the views that the outer radius changed
		this->Notify(MSG_FALLOFF_OUTERRADIUS);  
	}
	return(MS::kSuccess);
}
MDistance MoveToolCmdModel::getFalloffOuterRadius() const
{
	return(this->falloffOuterRadius);
}
MStatus MoveToolCmdModel::setFalloffCurve(const MDoubleArray& curve)
{
	// Check if they're equalivant
	bool equal = true;
	if(curve.length() != this->falloffCurve.length())
	{
		equal = false;
	} else
	{
		for(unsigned i=0; i<curve.length(); i++)
		{
			double val = curve[i] - this->falloffCurve[i];
			if(val*val >= MVector_kTol)
			{
				equal = false;
				break;
			}
		}
	}

	if(equal == false)
	{ 
		// Sanity
		MAssert(curve.length() == 4);
		MStatus stat = this->falloffCurve.copy(curve); MAssertStat(stat);
		
		// Both falloff and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;

		// Inform all of the views that the outer radius changed
		this->Notify(MSG_FALLOFF_CURVE);  
	}
	
	return(MS::kSuccess);
}
MDoubleArray MoveToolCmdModel::getFalloffCurve() const
{
	return(this->falloffCurve);
}
MStatus MoveToolCmdModel::setFalloffAxis(const MVector& axis)
{  
	MStatus stat;
	if(!axis.isEquivalent(this->falloffAxis) ||
	   this->updateType == this->kNodeSet)
	{ 
		this->falloffAxis = axis;  

		// If the geometry type is set by a node and the type is XXXView
		// then the axis should be the current side vector. See calcWeights for usage.
		if(this->updateType == this->kNodeSet)
		{  
			// Check if the type is kXXXView or kXXXView
			// If so, override the axis with the current side vector
			if(this->falloffType == this->kLinearView ||
			   this->falloffType == this->kCylinderView)
			{   
				this->falloffAxis = this->calcSideVector(&stat); MAssertStat(stat);
			}
		}

		// Both falloff and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;

		// Inform all of the views that the outer radius changed
		this->Notify(MSG_FALLOFF_AXIS);  
	}
	return(MS::kSuccess);
}
MVector MoveToolCmdModel::getFalloffAxis(MStatus* stat) const
{
	MValidateStatPtr(stat);
	*stat = MS::kSuccess;
	return(this->falloffAxis);
}

MDoubleArray MoveToolCmdModel::getFalloffWeights(MStatus* stat)
{
	MValidateStatPtr(stat);

	// This should recalculate the falloff weights
	*stat = this->calcFalloffWeights(); MAssertStatReturnObj(*stat, this->falloffWeights);
	return(this->falloffWeights);
}

// Methods for mirroring functionality
MStatus MoveToolCmdModel::setMirrorType(const MoveToolCmdModel::MirrorType& type)
{
	MStatus stat;

	if(type != this->mirrorType)
	{
		MAssert(type < this->kMirrorLast);
		// MTrace("MoveToolCmdModel::setMirrorType: setting mirror type to " + type);
		this->mirrorType = type;
		this->mirrorWeightsDirty = true;
  
		// Inform all of the views that the mirror type changed
		this->Notify(MSG_MIRROR_TYPE);
	}
	return(MS::kSuccess);
}
MoveToolCmdModel::MirrorType MoveToolCmdModel::getMirrorType() const
{
	return(this->mirrorType);
}
MStatus MoveToolCmdModel::setMirrorAxis(const MVector& axis)
{
	MStatus stat;

	if(!axis.isEquivalent(this->mirrorAxis) ||
	   this->updateType == this->kNodeSet)
	{
		// MTrace("MoveToolCmdModel::setMirrorAxis: setting mirrorAxis to " + Format::toString(axis));
		this->mirrorAxis = axis;
		// If the geometry type is set by a node and the type is XXXView
		// then the axis should be the current side vector. See getMirrorMatrix().
		if(this->updateType == this->kNodeSet)
		{  
			// Check if the mirror type is kMirrorView
			// If so, override the mirror axis with the current side vector
			if(this->mirrorType == this->kMirrorView)
			{   
				this->mirrorAxis = this->calcSideVector(&stat); MAssertStat(stat);
			} 
		}

		this->mirrorWeightsDirty = true;

		this->Notify(MSG_MIRROR_AXIS);
	}
	return(MS::kSuccess);
}
MVector MoveToolCmdModel::getMirrorAxis(MStatus* stat) const
{
	MValidateStatPtr(stat);
	*stat = MS::kSuccess;
	return(this->mirrorAxis);
}
MDoubleArray MoveToolCmdModel::getMirrorWeights(MStatus* stat)
{
	MValidateStatPtr(stat);
	// Make sure the mirror weights are up to date
	*stat = this->calcMirrorWeights(); MAssertStatReturnObj(*stat, this->mirrorWeights);
 
	return(this->mirrorWeights);
}

MPoint MoveToolCmdModel::getCenterPoint(MStatus* stat)
{
	// MTrace("MoveToolCmdModel::getCenterPoint: called.");
	MValidateStatPtr(stat);

	*stat = this->calcCenterPoint(); MAssertStatReturnObj(*stat, MPoint::origin);

	return(this->centerPoint);
}

MStatus MoveToolCmdModel::setViewChanged()
{
	if(this->falloffType == this->kCylinderView ||
	   this->falloffType == this->kLinearView)
   	{ 
		// Both falloffWeights and mirror weights are dirtied
		this->falloffWeightsDirtyFlag = true;
		this->mirrorWeightsDirty = true;
	}
	if(this->mirrorType == this->kMirrorView)
	{
		this->mirrorWeightsDirty = true;
	}

	return(MS::kSuccess);
}

/****************************************
 Calculating methods
 ****************************************/
MStatus MoveToolCmdModel::calcCenterPoint()
{
	// MTrace("MoveToolCmdModel::calcCenterPoint: called.");
	// Only run calcCenterPoint if the dirty flag is set
	if(this->centerPointDirtyFlag == false)
	{
		return(MS::kSuccess);
	}
	this->centerPointDirtyFlag = false;
	// MTrace("MoveToolCmdModel::calcCenterPoint: calcing.");

	MStatus stat;

	MItGeometry* selectedItPtr;
	if(this->updateType == MoveToolCmdModel::kContextSet)
	{ 
		selectedItPtr = new MItGeometry(this->dagPath, this->component, &stat); MAssertStat(stat);
	} else if(this->updateType == MoveToolCmdModel::kNodeSet)
	{
		selectedItPtr = new MItGeometry(this->dataHandle, this->groupId, true, &stat); MAssertStat(stat);  
	} else
	{
		MCheck(false, "The data type is not specified!");
	}

	// Move everything in worldspace for now
	// NOTE: get rid of this... DONT need it anymore
	MSpace::Space space = MSpace::kWorld;  

	// Get the reference to the geometry iterator
	MItGeometry& selIt = *selectedItPtr;
	// Sanity
	MAssert(selIt.count(&stat) > 0) MAssertStat(stat);

	this->centerPoint = MPoint::origin;
	for(; !selIt.isDone(&stat); selIt.next())
	{
		MAssertStat(stat);
		this->centerPoint += selIt.position(space, &stat); MAssertStat(stat);
	}
	this->centerPoint = this->centerPoint / selIt.count(&stat); MAssertStat(stat);

	delete selectedItPtr;
	
	// MTrace("MoveToolCmdModel::calcCenterPoint: center = " + Format::toString(this->centerPoint));

	// NOTE: In order to work around a MTransformationMatrix bug, we need to clear the transformation matrix and 
	// reset it each time we set the pivot
	MTransformationMatrix::RotationOrder order = MTransformationMatrix::kXYZ;
	MVector translate = this->transform.translation(MSpace::kTransform, &stat); MAssertStat(stat);
	double rotate[3], scale[3];
	stat = this->transform.getRotation(rotate, order); MAssertStat(stat); 
	stat = this->transform.getScale(scale, MSpace::kTransform); MAssertStat(stat);

	this->transform = MTransformationMatrix::identity;
	stat = this->transform.setRotatePivot(this->centerPoint, MSpace::kTransform, false); MAssertStat(stat); 
	stat = this->transform.setScalePivot(this->centerPoint, MSpace::kTransform, false); MAssertStat(stat);

	stat = this->transform.setTranslation(translate, MSpace::kTransform); MAssertStat(stat);
	stat = this->transform.setRotation(rotate, order); MAssertStat(stat);
	stat = this->transform.setScale(scale, MSpace::kTransform); MAssertStat(stat);

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::calcFalloffWeights()
{ 
	// Only run the calcFalloffWeights if the dirty flag is set
	if(this->falloffWeightsDirtyFlag == false)
	{
		return(MS::kSuccess);
	}
	this->falloffWeightsDirtyFlag = false;

	// MTrace("MoveToolCmd::calcFalloffWeights: called");
	MStatus stat;   

	// Get the objects we need for the update
	MObject shapeObj;
	MMatrix worldMatrix;
	MItGeometry* shapeItPtr = NULL,* selectedItPtr = NULL;
	stat = this->getGeometryInfo(shapeObj, worldMatrix, shapeItPtr, selectedItPtr); MAssertStat(stat);  

	// Get the reference to the geometry iterator
	MItGeometry& selIt = *selectedItPtr;
	MItGeometry& vertIt = *shapeItPtr;

	int numVerts = vertIt.count(&stat); MAssertStat(stat);
	// Set the length of the falloff weights
	stat = this->falloffWeights.setLength(numVerts); MAssertStat(stat);

	// If there are no selected CVs, set all of the weights to 1, it's the object..
	if(selIt.count() == 0)
	{
		for(int i=0; i<numVerts; i++)
		{
			this->falloffWeights[i] = 1.0;
		}
	} else // Set each of the indices to zero and run the falloff calculations for the Type
	{  
		for(int i=0; i<numVerts; i++)
		{
			this->falloffWeights[i] = 0.0;
		} 
	
		// Get the selected points into an array and their indices
		MPointArray selectedPts; 
		for(selIt.reset(); !selIt.isDone(&stat); selIt.next())
		{
			MAssertStat(stat);  
			MPoint pt = selIt.position(MSpace::kWorld, &stat); MAssertStat(stat);
			stat = selectedPts.append(pt); MAssertStat(stat);
		}

		stat = this->calcWeights(this->falloffWeights, selectedPts, vertIt, shapeObj, worldMatrix); MAssertStat(stat);
	}

	delete shapeItPtr;
	delete selectedItPtr;

	// MTrace("MoveToolCmd::calcFalloffWeights: finished calcing falloff weights.");
	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::calcWeights(MDoubleArray& weights, const MPointArray& selectedPts, 
	MItGeometry& vertIt, MObject& shapeObj, const MMatrix& worldMatrix)
{
	MStatus stat;
	// MTrace("MoveToolCmd::calcWeights: falloff type = " + this->falloffType);
	switch(this->falloffType)
	{
	case(MoveToolCmdModel::kNone): 
		stat = this->calcNoneFalloff(weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kSpherical):
		// Don't need direction here
		stat = this->calcFalloff(calcSphericalDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kLinearX):
		this->falloffDirection = MVector::xAxis;
		stat = this->calcFalloff(calcLinearDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kLinearY):
		this->falloffDirection = MVector::yAxis;
		stat = this->calcFalloff(calcLinearDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kLinearZ):
		this->falloffDirection = MVector::zAxis;
		stat = this->calcFalloff(calcLinearDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kLinearView):
		// special case if the geometry type is set by a node. See setFalloffAxis()
		if(this->updateType == this->kNodeSet)
		{ 
			// Use the falloff axis
			this->falloffDirection = this->falloffAxis;
		} else
		{  
			// Use the side vector as the falloff direction
			this->falloffDirection = this->calcSideVector(&stat); MAssertStat(stat);
		}
		stat = this->calcFalloff(calcLinearDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break; 
	case(MoveToolCmdModel::kLinearCustom):
		this->falloffDirection = this->falloffAxis.normal();
		MAssert((this->falloffDirection.length() - 1.0) < MVector_kTol);
		stat = this->calcFalloff(calcLinearDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;  
	case(MoveToolCmdModel::kCylinderX):
		this->falloffDirection = MVector::xAxis;
		stat = this->calcFalloff(calcCylinderDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kCylinderY):
		this->falloffDirection = MVector::yAxis;
		stat = this->calcFalloff(calcCylinderDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kCylinderZ):
		this->falloffDirection = MVector::zAxis;
		stat = this->calcFalloff(calcCylinderDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kCylinderView):
		// special case if the geometry type is set by a node. See setFalloffAxis()
		if(this->updateType == this->kNodeSet)
		{ 
			// Use the falloff axis
			this->falloffDirection = this->falloffAxis;
		} else
		{  
			// Use the side vector as the falloff direction
			this->falloffDirection = this->calcSideVector(&stat); MAssertStat(stat);
		}
		stat = this->calcFalloff(calcCylinderDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break; 
	case(MoveToolCmdModel::kCylinderCustom):
		this->falloffDirection = this->falloffAxis.normal();
		MAssert((this->falloffDirection.length() - 1.0) < MVector_kTol);
		stat = this->calcFalloff(calcCylinderDist, weights, selectedPts, vertIt); MAssertStat(stat);
		break;
	case(MoveToolCmdModel::kTopology):     
		stat = this->calcTopologyFalloff(weights, selectedPts, vertIt, shapeObj, worldMatrix); MAssertStat(stat);
		break;  
	default:
		MError("Unimplemented falloff type " + this->falloffType);
		break;
	}  

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::calcNoneFalloff(MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt) const
{
	MStatus stat;
	
	// Find the closest point to each of the elements in selectedPts
	for(unsigned i=0; i<selectedPts.length(); i++)
	{
		unsigned closestIdx;
		double distance = 10e38;
		for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
		{
			MAssertStat(stat);
			MPoint pt = vertIt.position(MSpace::kWorld, &stat); MAssertStat(stat);
			double d = pt.distanceTo(selectedPts[i]);
			if(d < distance)
			{
				distance = d;
				closestIdx = vertIt.index(&stat); MAssertStat(stat);
			}
		}
		MAssertStat(stat);

		// Set the closest index to weight 1
		weights[closestIdx] = 1.0;
	}

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::calcFalloff(FalloffDistFunc distFunc, MDoubleArray& weights, 
	const MPointArray& selectedPts, MItGeometry& vertIt) const
{
	MStatus stat;

	MDoubleArray deltaArray(vertIt.count(), 10e38);

	for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
	{
		MAssertStat(stat);
		MPoint vertPt = vertIt.position(MSpace::kWorld, &stat); MAssertStat(stat);
		unsigned vertIdx = vertIt.index(&stat); MAssertStat(stat);

		double& delta = deltaArray[vertIdx];

		// Find the smallest delta to the current vertex
		for(unsigned i=0; i<selectedPts.length(); i++)
   		{
			// Calculate the delta
			MPoint selPt = selectedPts[i];
			double dist = (this->*(distFunc))(selPt, vertPt);   
			if(delta > dist)
			{
				
				delta = dist;
			}
		}
	}
 
	stat = this->final_calcFalloffWeights(weights, deltaArray); MAssertStat(stat);

	return(MS::kSuccess);
}

double MoveToolCmdModel::calcSphericalDist(const MPoint& a, const MPoint& b) const
{
	// Calc the regular distance...
	return(a.distanceTo(b));
}
double MoveToolCmdModel::calcLinearDist(const MPoint& a, const MPoint& b) const
{
	MVector deltaV = b - a;
	// Calculate the dot be delta and direction
	double dist = this->falloffDirection * deltaV;
	return(fabs(dist));
}
double MoveToolCmdModel::calcCylinderDist(const MPoint& a, const MPoint& b) const
{
	MVector deltaV = b - a;
	// Calculate the projected vector from deltaV onto direction
	MVector bVec = this->falloffDirection * (this->falloffDirection * deltaV);
	// Get the difference
	deltaV =  deltaV - bVec;
	return(deltaV.length());
}

// declare a structure for the priority queue
// so we don;'t have to use distancePair
class DistancePair
{
public:
	unsigned index;
	double distance;
	DistancePair(unsigned index, double distance) : index(index), distance(distance) { }
	// Make it a min queue, fuck the weird STL messages
	bool operator<(const DistancePair& that) const { return(this->distance > that.distance); }
};

MStatus MoveToolCmdModel::calcTopologyFalloff(MDoubleArray& weights, const MPointArray& selectedPts, MItGeometry& vertIt, 
	MObject& shapeObj, const MMatrix& worldMatrix) const
{
	MStatus stat;
	// MTrace("MoveToolCmdModel::calcTopologyFalloff: called.");
 
	// The shapeObj must be a kMesh(if updateType is kContextType) or 
	// kMeshGeom(if updateType if kNodeType)

	if(!shapeObj.hasFn(MFn::kMesh)) 
	{
		MError("MoveToolCmdModel::calcTopologyFalloff: A mesh must be selected.");
		return(MS::kInvalidParameter);
	}
	MFnMesh meshFn(shapeObj, &stat); MAssertStat(stat);
	// Get the world space coords
	MPointArray vertexPositions;
	stat = meshFn.getPoints(vertexPositions); MAssertStat(stat);
	// NOTE: For optimization, pre-multiply the vertex positions by the world space matrix.
	// Only if performance is an issue...

	// Run dijkstra's algorithm modified for multiple sources and using STL's priority queue
	std::priority_queue<DistancePair> Q;
	// The current distance for each vertex
	// Initialize the initial distances to infinity
	MDoubleArray deltaArray(weights.length(), 10e38);
	
	// First need to find the closest points to the selectedPts
	MIntArray selectedIdxs;
	for(unsigned i=0; i<selectedPts.length(); i++)
	{
		unsigned closestIdx;
		double distance = 10e38;
		for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
		{
			MAssertStat(stat);
			MPoint pt = vertIt.position(MSpace::kWorld, &stat); MAssertStat(stat);
			double d = pt.distanceTo(selectedPts[i]);
			if(d < distance)
			{
				distance = d;
				closestIdx = vertIt.index(&stat); MAssertStat(stat);
			}
		}
		// MTrace("selected vert = " + idx);
		deltaArray[closestIdx] = 0;
		// Push the idx into Q
		Q.push(DistancePair(closestIdx, deltaArray[closestIdx]));
	}

	MFnSingleIndexedComponent compFn;
	MItMeshEdge edgeIt(shapeObj, &stat); MAssertStat(stat);
	// MItMeshVertex vertIt(shapeObj, &stat); MAssertStat(stat);
	double outerDistance = this->falloffOuterRadius.as(MDistance::uiUnit(), &stat); MAssertStat(stat);
	while(!Q.empty())
	{
		// Extract the minimum -> Get and pop the minimum index
		int U = Q.top().index; Q.pop();  
		// MTrace("Getting vertex " + U + " from queue.");
		
		// Now relax the neighbors of u
		MObject newComp = compFn.create(MFn::kMeshVertComponent, &stat); MAssertStat(stat);
		stat = compFn.addElement(U); MAssertStat(stat);

		stat = edgeIt.reset(shapeObj, newComp); MAssertStat(stat);
		for(; !edgeIt.isDone(&stat); edgeIt.next())
		{
			MAssertStat(stat);
			unsigned edgeIdx = edgeIt.index(&stat); MAssertStat(stat);
			// MTrace("Looking at edge # " + edgeIdx);
			int2 vertIdxs; 
			stat = meshFn.getEdgeVertices(edgeIdx, vertIdxs); MAssertStat(stat); 

			// Get v
			int V = (vertIdxs[0]==U? vertIdxs[1]: vertIdxs[0]);	
			// MTrace("Looking at edge between pt " + U + " and pt " + V);
			
			// Now get the distance between U and V
			double edgeLength = (vertexPositions[U]*worldMatrix).distanceTo(vertexPositions[V]*worldMatrix);

			// Shortcuircet the relaxation calculations because we don't care about vertices
			// beyond the outer radius. 
			if(deltaArray[U] + edgeLength > outerDistance)
			{
				continue;
			}  

			// Run the relaxation algorithm
			if(deltaArray[V] > deltaArray[U] + edgeLength)
			{
				deltaArray[V] = deltaArray[U] + edgeLength;
				// MTrace("adding vertex #" + V + " to queue, distance = " + deltaArray[V]);
				// Add v into the priority control
				Q.push(DistancePair(V, deltaArray[V]));
			}
		}
	}

	stat = this->final_calcFalloffWeights(weights, deltaArray); MAssertStat(stat);

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::final_calcFalloffWeights(MDoubleArray& weights, const MDoubleArray& deltas) const
{
	// MTrace("MoveToolCmdModel::final_calcFalloffWeights: called.");
	MStatus stat;
	
	double outerDistance = this->falloffOuterRadius.as(MDistance::uiUnit(), &stat); MAssertStat(stat);
	double innerDistance = this->falloffInnerRadius.as(MDistance::uiUnit(), &stat); MAssertStat(stat);
	double deltaDistance = outerDistance - innerDistance;

	CatmullRomCurve curve(this->falloffCurve);
	// Sanity
	MAssert(deltas.length() == weights.length());

	// Print out the minimum distance lengths for each vertex
	for(unsigned vertIdx=0; vertIdx<deltas.length(); vertIdx++)
	{
		double delta = deltas[vertIdx];
		// MTrace("Vertex " + vertIdx + ", minimum distance = " + delta);
	
		// Always make the falloff weight of points inside innerDistance = ptArray[0]
		if(delta <= innerDistance)
		{
			weights[vertIdx] = this->falloffCurve[0];
		} else if(delta > outerDistance) // These stay zero
		{
			continue;
		} else
		{   
			// Normalize the delta from innerDistance->outerDistance to 0->1 
			double t = (delta - innerDistance) / deltaDistance;
			// Calc the bezier curve
			// NOTE: This should be either Catmull-Rom, bezier, or NURBS
			/*
			double minT = 1.0 - t;
			this->falloffWeights[vertIdx] = 
				minT * minT * minT * this->falloffCurve[0] +
				3.0 * minT * minT * t * this->falloffCurve[1] +
				3.0 * minT * t * t * this->falloffCurve[2] +
				t * t * t * this->falloffCurve[3];
			*/

			weights[vertIdx] = curve.calcT(t);
		}
	} // End for

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::calcMirrorWeights()
{   
	// Only run the calcFalloffWeights if the dirty flag is set
	if(this->mirrorWeightsDirty == false)
	{
		return(MS::kSuccess);
	}
	this->mirrorWeightsDirty = false;

	// MTrace("MoveToolCmd::calcMirrorWeights: called");
	MStatus stat;

	// Get the objects we need for the update
	MObject shapeObj;
	MMatrix worldMatrix;
	MItGeometry* shapeItPtr = NULL,* selectedItPtr = NULL;
	stat = this->getGeometryInfo(shapeObj, worldMatrix, shapeItPtr, selectedItPtr); MAssertStat(stat);

	// Get the reference to the geometry iterator
	MItGeometry& selIt = *selectedItPtr;
	MItGeometry& vertIt = *shapeItPtr;

	int numVerts = vertIt.count(&stat); MAssertStat(stat);
	// Set the length of the mirror weights and clear the values
	stat = this->mirrorWeights.setLength(numVerts); MAssertStat(stat);
	for(unsigned i = 0; i<this->mirrorWeights.length(); i++)
	{
		this->mirrorWeights[i] = 0.0;
	}

	// There must at least one vertex selected, otherwise, the object is selected and
	// mirroring should not apply. Also don't do mirroring if the mirrorType is kNone
	unsigned count = selIt.count(&stat); MAssertStat(stat);
	if(count > 0 && this->mirrorType != this->kMirrorNone)
	{
		// For now, reflect across the YZ plane at the local origin
		// Later, use the mirror type to determine the reflection matrix
		MMatrix reflectMat = this->getMirrorMatrix(&stat); MAssertStat(stat);
  
		// after the reflection is applied
		// Convert into world space
		reflectMat *= worldMatrix;

		// Calc the reflected points
		MPointArray selectedPts;
		for(selIt.reset(); !selIt.isDone(&stat); selIt.next())
		{
			MAssertStat(stat);  
			MPoint pt = selIt.position(MSpace::kObject, &stat); MAssertStat(stat);
			pt *= reflectMat;
			stat = selectedPts.append(pt); MAssertStat(stat);
			// MTrace("MoveToolCmd::calcMirrorWeights: point #" + selIt.index() + " = " + Format::toString(pt));
		}

		stat = this->calcWeights(this->mirrorWeights, selectedPts, vertIt, shapeObj, worldMatrix); MAssertStat(stat);
	} 

	// Clean up
	delete shapeItPtr;
	delete selectedItPtr;
    
	return(MS::kSuccess);
}

/******************************************************
 Generic Helper methods
 ******************************************************/
MMatrix MoveToolCmdModel::getRegularMatrix(MStatus* stat)
{
	MValidateStatPtr(stat);
	// Make sure the matrix is uptodate
	*stat = this->calcCenterPoint(); MAssertStatReturnObj(*stat, MMatrix::identity); 
	return(this->transform.asMatrix());
}
MMatrix MoveToolCmdModel::getMirrorMatrix(MStatus* stat)
{
	MValidateStatPtr(stat);

	// Calculate the correct mirror transformation in local space
	MMatrix mirror;
	switch(this->mirrorType)
	{
	case(this->kMirrorNone):
		break;
	case(this->kMirrorX):
		mirror[0][0] = -1;
		break;
	case(this->kMirrorY):
		mirror[1][1] = -1;
		break;
	case(this->kMirrorZ):
		mirror[2][2] = -1;
		break;
	case(this->kMirrorView):
		{
			MVector sideVector;
			// Special case. See setMirrorAxis().
			if(this->updateType == this->kNodeSet)
		   	{ 
				// Use the mirror axis
				sideVector = this->mirrorAxis;
			} else
			{  
				// Calc the current side vector
				sideVector = this->calcSideVector(stat); MAssertStatReturnObj(*stat, mirror);   
			}
			// Calc the rotation matrix from the side to the X axis
			MQuaternion quat(sideVector, MVector::xAxis);
			MMatrix rotMat = quat.asMatrix();
		
			// calc the mirror matrix
			mirror[0][0] = -1;

			// calc the final matrix
			mirror = rotMat * mirror * rotMat.inverse();
			break;
		}
	case(this->kMirrorCustom):
		{
			// Calc the rotation matrix from the side to the X axis
			MQuaternion quat(this->mirrorAxis, MVector::xAxis);
			MMatrix rotMat = quat.asMatrix();
		
			// calc the mirror matrix
			mirror[0][0] = -1;
		
			// calc the final matrix
			mirror = rotMat * mirror * rotMat.inverse();
			break;
		}
	default:
		MCheckReturnObj(false, "MoveToolCmdModel::getMirrorMatrix: invalid mirror type " + this->mirrorType, mirror);
		break;
	}

	return(mirror);
}

MStatus MoveToolCmdModel::getGeometryInfo(MObject& shapeObj, MMatrix& worldMatrix, 
	MItGeometry*& shapeItPtr, MItGeometry*& selectedItPtr)
{
	MStatus stat;

	if(this->updateType == MoveToolCmdModel::kContextSet)
	{ 
		// MTrace("MoveToolCmdModel::calcFalloffWeights: dag path = " + this->dagPath.fullPathName());
		shapeObj = this->dagPath.node(&stat); MAssertStat(stat);
		worldMatrix = this->dagPath.inclusiveMatrix(&stat); MAssertStat(stat);
		shapeItPtr = new MItGeometry(this->dagPath, &stat); MAssertStat(stat);
		selectedItPtr = new MItGeometry(this->dagPath, this->component, &stat); MAssertStat(stat);
	} else if(this->updateType == MoveToolCmdModel::kNodeSet)
	{
		switch(this->dataHandle.type())
		{
		case(MFnData::kMesh): shapeObj = this->dataHandle.asMesh(); break;
		case(MFnData::kNurbsCurve): shapeObj = this->dataHandle.asNurbsCurve(); break;
		case(MFnData::kNurbsSurface): shapeObj = this->dataHandle.asNurbsSurface(); break;
		case(MFnData::kSubdSurface): shapeObj = this->dataHandle.asSubdSurface(); break;
		default: MCheck(false, "Invalid surface type.");
		}
		worldMatrix = this->dataHandle.geometryTransformMatrix();
		shapeItPtr = new MItGeometry(this->dataHandle, true, &stat); MAssertStat(stat); 
		selectedItPtr = new MItGeometry(this->dataHandle, this->groupId, true, &stat); MAssertStat(stat);  
	} else
	{
		MCheck(false, "The data type is not specified!");
	}

	return(MS::kSuccess);
}

MStatus MoveToolCmdModel::processGeom()
{
	MStatus stat;
 
	// We have to use this hack because there is no copy method in the MItGeometry class
	// Notice we delete the pointer at the end of the method
	MItGeometry* shapeItPtr;
	MMatrix worldMatrix;
	if(this->updateType == MoveToolCmdModel::kContextSet)
	{ 
		shapeItPtr = new MItGeometry(this->dagPath, &stat); MAssertStat(stat);
		worldMatrix = this->dagPath.inclusiveMatrix(&stat); MAssertStat(stat);
	} else if(this->updateType == MoveToolCmdModel::kNodeSet)
	{
		shapeItPtr = new MItGeometry(this->dataHandle, false, &stat); MAssertStat(stat);
		worldMatrix = this->dataHandle.geometryTransformMatrix();
	} else
	{
		MCheck(false, "The data type is not specified!");
	}
	MItGeometry& vertIt = *shapeItPtr; 

	// Make sure the falloff weights are up-to-date
	stat = this->calcFalloffWeights(); MAssertStat(stat);

	// Make sure the mirror weights are up to date
	stat = this->calcMirrorWeights(); MAssertStat(stat);

	// Calc the transformation matrix
	MMatrix matrix = this->getRegularMatrix(&stat); MAssertStat(stat);
	matrix = worldMatrix * matrix * worldMatrix.inverse();
	// Calc the mirrored transformation matrix
	MMatrix mirrorMatrix = this->getMirrorMatrix(&stat); MAssertStat(stat);
	mirrorMatrix *= matrix * mirrorMatrix;
	
	// Sanity check
	MAssert(vertIt.count() == this->falloffWeights.length());
	MAssert(vertIt.count() == this->mirrorWeights.length());
	// Iterate over each of the vertices 
	for(vertIt.reset(); !vertIt.isDone(&stat); vertIt.next())
	{
		MAssertStat(stat);
		unsigned idx = vertIt.index(&stat); MAssertStat(stat);
		// First move the points depending on the falloff
		double normalWeight = this->falloffWeights[idx];
		double mirrorWeight = this->mirrorWeights[idx];
		// Quick optimization: Only do the big calculations if the weights are not zero
		if(normalWeight == 0.0 && mirrorWeight == 0.0)
		{
			continue;
		}  
		MPoint pt = vertIt.position(MSpace::kObject, &stat); MAssertStat(stat);  
		// MTrace("Pt " + idx + ": prev pos=" + Format::toString(pt));

		// Get the interpolated position for the normal point
		MPoint normalPt = pt * matrix;
		normalPt = pt*(1.0-normalWeight) + normalPt*normalWeight;
		
		// Get the interpolated position for the mirrored point
		MPoint mirrorPt = pt * mirrorMatrix;
		mirrorPt = pt*(1.0-mirrorWeight) + mirrorPt*mirrorWeight;
		
		// get the final interpolated position between the mirrored and normal points
		MPoint finalPt = (normalPt*normalWeight + mirrorPt*mirrorWeight) / (normalWeight + mirrorWeight);

		stat = vertIt.setPosition(finalPt, MSpace::kObject); MAssertStat(stat);  
		// MTrace("Pt " + idx + ": new pos=" + Format::toString(newPt));     
	}

	delete shapeItPtr;

	return(MS::kSuccess);
}

// Helper function for calculating the current side vector for the view
MVector MoveToolCmdModel::calcSideVector(MStatus* stat) const
{
	MValidateStatPtr(stat);
	MPoint temp;
	MVector viewDirection, upDirection, sideDirection;
	M3dView view = M3dView::active3dView(stat); MAssertStatReturnObj(*stat, sideDirection);
	
	*stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2, temp, viewDirection); MAssertStatReturnObj(*stat, sideDirection);
	*stat = view.viewToWorld(view.portWidth()/2, view.portHeight()/2 + 10, temp, upDirection); MAssertStatReturnObj(*stat, sideDirection);
	sideDirection = viewDirection ^ upDirection;
	*stat = sideDirection.normalize(); MAssertStatReturnObj(*stat, sideDirection);
	return(sideDirection);
}

