/**
	This file is part of ZooZTools.

    ZooZTools is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZooZTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A copy of the GNU license is obtainable at:
	http://www.gnu.org/licenses/gpl.html
	
	or write to:
	Free Software Foundation, Inc., 
	51 Franklin St, Fifth Floor, 
	Boston, MA  02110-1301  USA
*/

#include "ToolNode.h"

#include <Log/MayaLog.h>
#include <Format/Format.h>

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MItGeometry.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPxManipContainer.h>

const MTypeId MoveToolNode::typeId(0x00450);
const MString MoveToolNode::typeName("ZooZToolNode");

// Define the MoveToolNode static members
MObject MoveToolNode::translation;
MObject MoveToolNode::scale;
MObject MoveToolNode::rotation;
MObject MoveToolNode::falloffType;
MObject MoveToolNode::falloffInnerRadius;
MObject MoveToolNode::falloffOuterRadius;
MObject MoveToolNode::falloffCurve;
MObject MoveToolNode::falloffInvert;
MObject MoveToolNode::falloffAxis;
MObject MoveToolNode::falloffWeights;
MObject MoveToolNode::mirrorType;
MObject MoveToolNode::mirrorAxis;
MObject MoveToolNode::mirrorWeights;

MoveToolNode::MoveToolNode()
{
	this->model = new MoveToolCmdModel(MoveToolCmdModel::kNodeSet);
	MAssertNoReturn(this->model != NULL);
}
MoveToolNode::~MoveToolNode()
{
	delete(this->model);
}

MStatus MoveToolNode::initialize()
{
	MTrace("MoveToolNode::initialize: called.");
	MStatus stat;

	MFnTypedAttribute typedAttr;
	MFnUnitAttribute unitAttr;
	MFnNumericAttribute numAttrFn;
	MFnEnumAttribute enumAttrFn;

	MoveToolNode::translation = numAttrFn.create("translation", "t", MFnNumericData::k3Double, 0, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::translation); MAssertStat(stat);

	MoveToolNode::scale = numAttrFn.create("scale", "s", MFnNumericData::k3Double, 0, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::scale); MAssertStat(stat);

	MoveToolNode::rotation = numAttrFn.create("rotation", "r", MFnNumericData::k3Double, 0, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::rotation); MAssertStat(stat);
	
	// Construct the falloff attributes
	MoveToolNode::falloffType = enumAttrFn.create("falloffType", "ft", MoveToolCmdModel::kNone, &stat); MAssertStat(stat);
	stat = enumAttrFn.addField("kNone", MoveToolCmdModel::kNone); MAssertStat(stat);
	stat = enumAttrFn.addField("kSpherical", MoveToolCmdModel::kSpherical); MAssertStat(stat);
	stat = enumAttrFn.addField("kLinearX", MoveToolCmdModel::kLinearX); MAssertStat(stat);
	stat = enumAttrFn.addField("kLinearY", MoveToolCmdModel::kLinearY); MAssertStat(stat);
	stat = enumAttrFn.addField("kLinearZ", MoveToolCmdModel::kLinearZ); MAssertStat(stat);
	stat = enumAttrFn.addField("kLinearView", MoveToolCmdModel::kLinearView); MAssertStat(stat);
	stat = enumAttrFn.addField("kLinearCustom", MoveToolCmdModel::kLinearCustom); MAssertStat(stat);
	stat = enumAttrFn.addField("kCylinderX", MoveToolCmdModel::kCylinderX); MAssertStat(stat);
	stat = enumAttrFn.addField("kCylinderY", MoveToolCmdModel::kCylinderY); MAssertStat(stat);
	stat = enumAttrFn.addField("kCylinderZ", MoveToolCmdModel::kCylinderZ); MAssertStat(stat);
	stat = enumAttrFn.addField("kCylinderView", MoveToolCmdModel::kCylinderView); MAssertStat(stat);
	stat = enumAttrFn.addField("kCylinderCustom", MoveToolCmdModel::kCylinderCustom); MAssertStat(stat);
	stat = enumAttrFn.addField("kTopology", MoveToolCmdModel::kTopology); MAssertStat(stat);
	stat = enumAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffType); MAssertStat(stat);

	MoveToolNode::falloffInnerRadius = unitAttr.create("falloffInnerRadius", "fir", MFnUnitAttribute::kDistance, 0.0, &stat); MAssertStat(stat);
	stat = unitAttr.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffInnerRadius); MAssertStat(stat);

	MoveToolNode::falloffOuterRadius = unitAttr.create("falloffOuterRadius", "for", MFnUnitAttribute::kDistance, 0.0, &stat); MAssertStat(stat);
	stat = unitAttr.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffOuterRadius); MAssertStat(stat);

	MoveToolNode::falloffCurve = typedAttr.create("falloffCurve", "fc", MFnData::kDoubleArray, MObject::kNullObj, &stat); MAssertStat(stat);
	stat = typedAttr.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffCurve); MAssertStat(stat);

	MoveToolNode::falloffInvert = numAttrFn.create("falloffInvert", "fi", MFnNumericData::kBoolean, false, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffInvert); MAssertStat(stat);

	MoveToolNode::falloffAxis = numAttrFn.create("falloffAxis", "fa", MFnNumericData::k3Double, 0, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffAxis); MAssertStat(stat);

	MoveToolNode::falloffWeights = typedAttr.create("falloffWeights", "fw", MFnData::kDoubleArray, MObject::kNullObj, &stat); MAssertStat(stat);
	stat = typedAttr.setWritable(false); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::falloffWeights); MAssertStat(stat);

	// Construct the mirror attributes
	MoveToolNode::mirrorType = enumAttrFn.create("mirrorType", "mt", MoveToolCmdModel::kMirrorNone, &stat); MAssertStat(stat);
	stat = enumAttrFn.addField("kNone", MoveToolCmdModel::kMirrorNone); MAssertStat(stat);
	stat = enumAttrFn.addField("kMirrorX", MoveToolCmdModel::kMirrorX); MAssertStat(stat);
	stat = enumAttrFn.addField("kMirrorY", MoveToolCmdModel::kMirrorY); MAssertStat(stat);
	stat = enumAttrFn.addField("kMirrorZ", MoveToolCmdModel::kMirrorZ); MAssertStat(stat);
	stat = enumAttrFn.addField("kMirrorView", MoveToolCmdModel::kMirrorView); MAssertStat(stat);
	stat = enumAttrFn.addField("kMirrorCustom", MoveToolCmdModel::kMirrorCustom); MAssertStat(stat);
	stat = enumAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::mirrorType); MAssertStat(stat);

	MoveToolNode::mirrorAxis = numAttrFn.create("mirrorAxis", "ma", MFnNumericData::k3Double, 0, &stat); MAssertStat(stat);
	stat = numAttrFn.setInternal(true); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::mirrorAxis); MAssertStat(stat);

	MoveToolNode::mirrorWeights = typedAttr.create("mirrorWeights", "mw", MFnData::kDoubleArray, MObject::kNullObj, &stat); MAssertStat(stat);
	stat = typedAttr.setWritable(false); MAssertStat(stat);
	stat = MoveToolNode::addAttribute(MoveToolNode::mirrorWeights); MAssertStat(stat);

	// Add other attributes...

	// Setup each of the depedencies
	stat = MoveToolNode::attributeAffects(MoveToolNode::translation, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::scale, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::rotation, MoveToolNode::outputGeom); MAssertStat(stat);

	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffType, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInnerRadius, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffOuterRadius, MoveToolNode::outputGeom); MAssertStat(stat); 
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffCurve, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInvert, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffAxis, MoveToolNode::outputGeom); MAssertStat(stat);
	// The falloff types affect the falloff weights
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffType, MoveToolNode::falloffWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInnerRadius, MoveToolNode::falloffWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffOuterRadius, MoveToolNode::falloffWeights); MAssertStat(stat); 
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffCurve, MoveToolNode::falloffWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInvert, MoveToolNode::falloffWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffAxis, MoveToolNode::falloffWeights); MAssertStat(stat); 

	stat = MoveToolNode::attributeAffects(MoveToolNode::mirrorType, MoveToolNode::outputGeom); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::mirrorAxis, MoveToolNode::outputGeom); MAssertStat(stat);
	// All of the input attributes affect the mirrorWeights
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffType, MoveToolNode::mirrorWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInnerRadius, MoveToolNode::mirrorWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffOuterRadius, MoveToolNode::mirrorWeights); MAssertStat(stat); 
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffCurve, MoveToolNode::mirrorWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffInvert, MoveToolNode::mirrorWeights); MAssertStat(stat);
	stat = MoveToolNode::attributeAffects(MoveToolNode::falloffAxis, MoveToolNode::mirrorWeights); MAssertStat(stat); 
	stat = MoveToolNode::attributeAffects(MoveToolNode::mirrorType, MoveToolNode::mirrorWeights); MAssertStat(stat); 
	stat = MoveToolNode::attributeAffects(MoveToolNode::mirrorType, MoveToolNode::mirrorWeights); MAssertStat(stat); 

	// NOTE: outputGeom isn't cached automatically.
	MFnAttribute outputGeom(MoveToolNode::outputGeom, &stat); MAssertStat(stat);
	outputGeom.setCached(true); MAssertStat(stat);

	// Add this to the manipulator connection table 
	stat = MPxManipContainer::addToManipConnectTable(const_cast<MTypeId&>(MoveToolNode::typeId)); MAssertStat(stat);

	return(MS::kSuccess);
}

/* Maya doesn't seem to call these.. using regular get/setIntervalValue instead
bool MoveToolNode::setInternalValueInContext(const MPlug& plug, const MDataHandle& handle, MDGContext& ctx)
{
	MTrace("MoveToolNode::setInternalValueInContext: called");
	MStatus stat;
	MObject attr = plug.attribute(&stat); MAssertStatReturnObj(stat, false);
	if(attr == this->transform)
	{
		stat = this->model->setVector(handle.asVector()); MAssertStatReturnObj(stat, false);
		return(true);
	} 

	return(false); 
}
bool MoveToolNode::getInternalValueInContext(const MPlug& plug, MDataHandle& handle, MDGContext& ctx)
{
	MTrace("MoveToolNode::getInternalValueInContext: called");
	MStatus stat;
	MObject attr = plug.attribute(&stat); MAssertStatReturnObj(stat, false);
	if(attr == this->transform)
	{
		MVector vec;
		stat = this->model->getVector(vec); MAssertStatReturnObj(stat, false);
		handle.set(vec);
		return(true);
	} 

	return(false); 
}
*/

bool MoveToolNode::setVectorPlug(const MObject& pAttr, const MDataHandle& handle, const MObject& attr, modelGetterFunc getter, modelSetterFunc setter, MStatus* stat)
{
	MValidateStatPtr(stat);	
	if(pAttr == attr)
	{
		MVector vec = handle.asVector();
		// MTrace("MoveToolNode::setVectorPlug: Getting value " + Format::toString(vec));  
		*stat = (this->model->*(setter))(vec); MAssertStatReturnObj(*stat, false);
		return(true);
	}
	// Check attr against each of the children of transform
	MFnNumericAttribute attrFn(attr, stat); MAssertStatReturnObj(*stat, false); 
	for(unsigned i=0; i<3; i++)
	{
		MObject cAttr = attrFn.child(i, stat); MAssertStatReturnObj(*stat, false);
		if(pAttr == cAttr)
		{   
			MVector vec = (this->model->*(getter))(stat); MAssertStatReturnObj(*stat, false);
			// MTrace("MoveToolNode::setVectorPlug: Matched " + i + "th child of attribute. val = " + Format::toString(vec));  
			vec[i] = handle.asDouble();
			*stat = (this->model->*(setter))(vec); MAssertStatReturnObj(*stat, false);
			return(true);
		}
	}

	return(false);
}
bool MoveToolNode::getVectorPlug(const MObject& pAttr, MDataHandle& handle, const MObject& attr, modelGetterFunc getter, MStatus* stat)
{
	MValidateStatPtr(stat);
	if(pAttr == attr)
	{
		MVector vec = (this->model->*(getter))(stat); MAssertStatReturnObj(*stat, false);
		// MTrace("MoveToolNode::getVectorPlug: Getting value " + Format::toString(vec));  
		handle.set(vec);
		return(true);
	} 
	// Check attr against each of the children of transform
	MFnNumericAttribute attrFn(attr, stat); MAssertStatReturnObj(*stat, false);
	for(unsigned i=0; i<3; i++)
	{
		MObject cAttr = attrFn.child(i, stat); MAssertStatReturnObj(*stat, false);
		if(pAttr == cAttr)
		{
			MVector vec = (this->model->*(getter))(stat); MAssertStatReturnObj(*stat, false);
			// MTrace("MoveToolNode::getVectorPlug: Matched " + i + "th child of attribute. val = " + Format::toString(vec));  
			handle.set(vec[i]);
			return(true);
		}
	}
	return(false);
}

bool MoveToolNode::getInternalValue(const MPlug& plug, MDataHandle& handle)
{
	// MTrace("MoveToolNode::getInternalValue: called, plug = " + plug.partialName(true, true, true, true, true, true));
	MStatus stat = MS::kSuccess; 
	// MTrace("handle type = " + handle.type() + ", numerType = " + handle.numericType());
	MObject attr = plug.attribute(&stat); MAssertStatReturnObj(stat, false);
	 
	// Handle the transformation values
	bool handled = this->getVectorPlug(attr, handle, this->translation, this->model->getTranslation, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{ 
		return(true); 
	} 
	handled = this->getVectorPlug(attr, handle, this->scale, this->model->getScale, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{ 
		return(true); 
	}
	handled = this->getVectorPlug(attr, handle, this->rotation, this->model->getRotation, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{ 
		return(true);
	}

	// Handle the falloff attributes
	if(attr == this->falloffType)
	{
		handle.set(this->model->getFalloffType());
		return(true);
	}
	if(attr == this->falloffInnerRadius)
	{
		handle.set(this->model->getFalloffInnerRadius());
		return(true);
	}
	if(attr == this->falloffOuterRadius)
	{
		handle.set(this->model->getFalloffOuterRadius());
		return(true);
	}
	if(attr == this->falloffCurve)
	{
		MFnDoubleArrayData dataFn;
		MObject curveObj = dataFn.create(this->model->getFalloffCurve(), &stat); MAssertStatReturnObj(stat, false);
		stat = handle.set(curveObj); MAssertStat(stat);
		return(true);
	} 	
	handled = this->getVectorPlug(attr, handle, this->falloffAxis, this->model->getFalloffAxis, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{
		 return(true); 
	}

	// Handle the mirror attributes
	if(attr == this->mirrorType)
	{
		handle.set(this->model->getMirrorType());
		return(true);
	}	
	handled = this->getVectorPlug(attr, handle, this->mirrorAxis, this->model->getMirrorAxis, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{ 
		return(true); 
	}

	return(false); 
}

bool MoveToolNode::setInternalValue(const MPlug& plug, const MDataHandle& handle)
{
	// MTrace("MoveToolNode::setInternalValue: called, plug = " + plug.partialName(true, true, true, true, true, true));
	MStatus stat;
	// MTrace("handle type = " + handle.type() + ", numerType = " + handle.numericType());
	MObject attr = plug.attribute(&stat); MAssertStatReturnObj(stat, false);
	
	// Handle the transformation attributes
	bool handled = this->setVectorPlug(attr, handle, this->translation, this->model->getTranslation, this->model->setTranslation, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{
		return(true); 
	}
	handled = this->setVectorPlug(attr, handle, this->scale, this->model->getScale, this->model->setScale, &stat); MAssertStatReturnObj(stat, false);
	if(handled)
	{
		return(true);
	}
	handled = this->setVectorPlug(attr, handle, this->rotation, this->model->getRotation, this->model->setRotation, &stat); MAssertStatReturnObj(stat, false);
	if(handled)
	{
		return(true);
	}
	
	// Handle the falloff attributes
	if(attr == this->falloffType)
	{
		MoveToolCmdModel::FalloffType type = (MoveToolCmdModel::FalloffType)handle.asShort();
		stat = this->model->setFalloffType(type); MAssertStatReturnObj(stat, false);
		return(true);
	}
	if(attr == this->falloffInnerRadius)
	{
		MDistance rad = handle.asDistance();
		stat = this->model->setFalloffInnerRadius(rad); MAssertStatReturnObj(stat, false);
		return(true);
	}
	if(attr == this->falloffOuterRadius)
	{
		MDistance rad = handle.asDistance();
		stat = this->model->setFalloffOuterRadius(rad); MAssertStatReturnObj(stat, false);
		return(true);
	}
	if(attr == this->falloffCurve)
	{
		// NOTE: We copy the data handle to overcome it's annoying const status. THIS IS a HACK!
		// Alternatively, we could get the MObject using one of the .asXXX functions but
		// that is risky
		MDataHandle tempHandle = handle;
		MObject curveObj = tempHandle.data();
		MFnDoubleArrayData dataFn(curveObj, &stat); MAssertStatReturnObj(stat, false);
		MDoubleArray curve = dataFn.array(&stat); MAssertStatReturnObj(stat, false);
		stat = this->model->setFalloffCurve(curve); MAssertStatReturnObj(stat, false);
		return(true);
	}
	handled = this->setVectorPlug(attr, handle, this->falloffAxis, this->model->getFalloffAxis, this->model->setFalloffAxis, &stat); 	MAssertStatReturnObj(stat, false);
	if(handled)
	{
		return(true);
	}

	// Handle the mirror attributes
	if(attr == this->mirrorType)
	{
		MoveToolCmdModel::MirrorType type = (MoveToolCmdModel::MirrorType)handle.asShort();
		stat = this->model->setMirrorType(type); MAssertStatReturnObj(stat, false);
		return(true);
	}
	handled = this->setVectorPlug(attr, handle, this->mirrorAxis, this->model->getMirrorAxis, this->model->setMirrorAxis, &stat); MAssertStatReturnObj(stat, false);
	if(handled) 
	{ 
		return(true); 
	}

	return(false); 
}

void MoveToolNode::copyInternalData( MPxNode* thatNode)
{
	// Copy over the model.
	MoveToolNode* that = (MoveToolNode*)thatNode;
	this->model = that->model;
}

MStatus MoveToolNode::compute(const MPlug& plug, MDataBlock& dataBlock)
{
	MStatus stat;
	MTrace("MoveToolNode::compute: called, plug = " + plug.partialName(true, true, true, true, true, true));

	// Get the input geometry
	MArrayDataHandle hInputArray = dataBlock.inputArrayValue(this->input, &stat); MAssertStat(stat);
	// Make sure only one object is selected here
	if(hInputArray.elementCount() != 1)
	{
		MError("MoveToolNode::compute: there is no inputGeometry. Returning.");
		return(MS::kInvalidParameter);
	}

	stat = hInputArray.jumpToElement(0); MAssertStat(stat);
	MDataHandle hInputData = hInputArray.inputValue(&stat); MAssertStat(stat); 
	MDataHandle hInputGeom = hInputData.child(this->inputGeom);

	// Get the groupID of this deformer
	MDataHandle hGroupId = hInputData.child(this->groupId);
	unsigned groupId = hGroupId.asInt();

	MObject attr = plug.attribute(&stat); MAssertStat(stat);

	// Compute based on the plug type provided
	if(attr == this->outputGeom)
	{ 
		// Check the status
		MDataHandle stateData = dataBlock.outputValue(this->state, &stat); MAssertStat(stat);
		short nodeStatus = stateData.asShort();
		// handle the state...
        // 0=Normal, 
		// 1=PassThrough, 
		// 2=Blocking,
		// 3=Internally disabled. Will return to Normal state when enabled. 
		// 4=Internally disabled. Will return to PassThrough state when enabled. 
		// 5=Internally disabled. Will return to Blocking state when enabled.  
		if(nodeStatus == 0)
		{
			// Do normal calculation.
			// NOTE: for now just copy the data...
			MDataHandle hOutputGeom = dataBlock.outputValue(plug, &stat); MAssertStat(stat);
			// Copy the inputGeom to the outputGeom
			stat = hOutputGeom.copy(hInputGeom); MAssertStat(stat);   
			// Run the geometry through the model
			stat = this->model->setNodeGeom(hOutputGeom, groupId); MAssertStat(stat);
			stat = this->model->redoIt(); MAssertStat(stat);
			// MTrace("MoveToolNode::compute: cemter point = " + Format::toString(this->model->getCenterPoint()));
		} else if(nodeStatus == 1)
		{
			// Just copy inMesh to outMesh
			// NOTE: for now just copy the data...
			MDataHandle hOutputGeom = dataBlock.outputValue(plug, &stat); MAssertStat(stat);
			// Copy the inputGeom to the outputGeom
			stat = hOutputGeom.copy(hInputGeom); MAssertStat(stat);
		} else
		{
			// Do nothing... Is blocked in weird ways...
		}
	
		// Alwasy set the plug as clean...
		stat = dataBlock.setClean(plug); MAssertStat(stat);
	} else if(attr == this->falloffWeights)
	{
		MTrace("MoveToolNode::compute: Getting the falloff weights.");
	} else if(attr == this->mirrorWeights)
	{
		MTrace("MoveToolNode::compute: getting the mirror weights");
	}

	return(MS::kSuccess);
}
