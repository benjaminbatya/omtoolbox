TODO:
1) Modify BaseContext and implementing contexts to rename their tool commands different depending on the context.
Also, they need to display all of the transformations, not just movement.

2) Make locator + manip for node so the user can adjust the node parameters later.

3) Make topology be another parameter in the falloff modifier instead of a falloff type.
Then every falloff type can be limited topologically instead of it being a falloff type.

4) Sketch UI for constraint modifier.

5) Implement constraint modifier code (this probably only affects the translation context.)
Constraint types include:
  a) Snap to grid points
  b) snap to vertices
  c) Constrain movement to edges
  d) Constrain movement to faces

6) Add particle transformation.

7) Make the manips transform objects (no components). (Add object manipulation for the manips.)

8) Make nurbs and subdiv manipulation work properly 

9) Implement discrete intervals for each of the manips
  a) Translate in discrete intervals (settable by the user)
  c) Rotate in discrete intervals (settable by the user)
  d) scale in discrete intervals (setable by the user)

10) Make the space in the model adjustable by the user. Let the user change the Action Center 
   and Action Axis of the manips. (These are just an additional transformation space to apply.)
  a) Action center - the pivot offset.
  b) Action Axis - additional rotation/scaling to do on the transform to put it into a different
  transformation space.
	
11) Make front facing/back facing a falloff parameter so users don't accidently move
unseen vertices.

12) Add different spaces for manipulation: (like Maya's standard transformation tools)
  a) World space,
  b) Local space, 
  c) normal space.
  d) Object space - The space of the parent.

13) Allow the user to choose the type of falloff curve: Bezier, NURBS, Catmull-Rom

